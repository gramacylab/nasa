import nlopt
import numpy as np
import pandas as pd
from pyDOE import lhs
from scipy.optimize import minimize

import os
import sys
fileDir = os.path.dirname(os.path.abspath(__file__))
sourcePath = os.path.join(fileDir, '../../../../CLoVER/source')
sys.path.append(sourcePath)
import functionLibrary
from meanFunction import *
from covarianceKernel import *
from MISGP import *
from CLoVER import *


def adaptive_design_with_clover(x0, y0, hf_model, n_select, threshold, bounds, 
                                n_int=1000, n_cand=1000, new_cand=False,
                                ecl_opt=False):
    import numpy as np
    import os
    import sys

    def hf_model_shifted(x):
        if x.ndim == 1:
            X = np.copy(x.reshape(1,-1))
        else:
            X = np.copy(x.T)
            
        return hf_model.predict(X) - threshold, 1

    g = [hf_model_shifted]
    cost = [1.]
    noiseVariance = [0.]
    
    # GP priors
    dim = x0.shape[1]
    m0 = meanZero(dim)
    k0 = kernelSquaredExponential(dim, [1.]+dim*[2./11.],
                                      pmin=[0.1]+dim*[1e-4], 
                                      pmax=[10.]+dim*[1e4])    
    
    # Sample points (for optimization step)
    samplePoints = lhs(x0.shape[1], n_cand).T
    for i in range(x0.shape[1]):
        samplePoints[i,:] = \
            samplePoints[i,:]*(bounds[i][1]-bounds[i][0])+bounds[i][0]     
    
    # Integration points (for computation of contour Entropy)
    xInt = lhs(x0.shape[1], n_int).T
    for i in range(x0.shape[1]):
        xInt[i,:] = xInt[i,:]*(bounds[i][1]-bounds[i][0])+bounds[i][0]

    ## Weights for integration points (sum to 1)
    wInt = np.ones((xInt.shape[1],))/xInt.shape[1]


    nIter = n_select
    # Minimum distance between samples
    tolX = dim*[1.e-3]
    # Contour Entropy stopping criterion
    tolH = 1.e-20;

    # Initialization
    source = len(x0)*[0]

    x = np.copy(x0.T)
    y = np.copy(y0)

    # Update prior to reflect variance of initial samples
    p0 = k0.getHyperparameter()
    p0min = k0.getHyperparameterLowerLimit()
    p0max = k0.getHyperparameterUpperLimit()
    p0 = k0.getHyperparameter()
    p0[0] = np.var(y)
    p0min[0] = 0.5*p0[0]
    p0max[0] = 1.5*p0[0]
    k0.setHyperparameter(p0)
    k0.setHyperparameterLowerLimit(p0min)
    k0.setHyperparameterUpperLimit(p0max)

    f = MISGP([m0], [k0], tolr=1.e-14)
    f.train(source, x, y)

    #Execute CLoVER
    f, history = select_with_clover(g, f, cost, noiseVariance, samplePoints, bounds,
                        nIter, tolH=tolH, tolX=tolX,integrationPoints=xInt, 
                        integrationWeights=wInt, log=True, 
                        ecl_opt=ecl_opt, new_cand=new_cand)
    return f.x.T, f.y



def select_with_clover(g, f, cost, noiseVariance, samplePoints, bounds, nIter, tolH=1.e-4,
           tolX=None, epsilon=2., integrationPoints=2500, 
           integrationWeights=None, log=False,
           ecl_opt=False, new_cand=False):
    print('Start of CLoVER')

    tcost = 0.
    
    nIS = f.nIS
    d = f.dimension()
    tolX = tolX
 
    if type(integrationPoints) is int:
        nInt = integrationPoints
        if d == 1:
            xMin = np.amin(samplePoints)
            xMax = np.amax(samplePoints)
        else:
            xMin = np.amin(samplePoints, axis=1)
            xMax = np.amax(samplePoints, axis=1)
            
        xInt, wInt = entropyImportance(f, epsilon, nInt, xMin, xMax)
    else:
        xInt = integrationPoints
        wInt = integrationWeights/np.sum(integrationWeights)
        nInt = int(xInt.size/d)
    
    KitPrior = f.evaluateCovariancePrior([0]*nInt, xInt, f.source, f.x)
    vInt = evaluateVariance(f, [0]*nInt, xInt, KitPrior)
    mInt = evaluateMean(f, [0]*nInt, xInt, KitPrior)
        
    H = contourEntropy(f, epsilon, xInt, wInt, mInt, vInt, KitPrior)        
    print('Iteration {:d}, contour entropy = {:.3e}, cost = {:.3e}'.format(0, H, tcost))
    
    nx = len(f.source)
    history = []
    for i in range(nx-1):
        if d == 1:
            history = updateHistory(history, -nx+i+1, [f.source[i]], 
                                    f.x[i], f.y[i], -1., -1., f)
        else:
            history = updateHistory(history, -nx+i+1, [f.source[i]], 
                                    f.x[:, i], f.y[i], -1., -1., f)
    if d == 1:
        history = updateHistory(history, 0, [f.source[nx-1]], f.x[nx-1], 
                                f.y[nx-1], H, 0., f)
    else:
        history = updateHistory(history, 0, [f.source[nx-1]], f.x[:, nx-1], 
                                f.y[nx-1], H, 0., f)
    if log:
        np.savetxt('CLoVER_history.txt', history, fmt='%1.6e')
    
    for iter in range(nIter):        
        if new_cand:           
            samplePoints = lhs(d, samplePoints.shape[0]).T
            for i in range(d):
                samplePoints[i,:] = \
                    samplePoints[i,:]*(bounds[i][1]-bounds[i][0])+bounds[i][0]   
        if ecl_opt:
            samplePoints = lhs(d, 10*d).T
            for i in range(d):
                samplePoints[i,:] = \
                    samplePoints[i,:]*(bounds[i][1]-bounds[i][0])+bounds[i][0]   
   
        source, x = choose_next_sample(f, cost, noiseVariance, epsilon, samplePoints, 
                                 xInt, wInt, mInt, vInt, KitPrior, tolX)
        if ecl_opt:
            i = argWhereSameXtol(samplePoints, x)[0]
            iNoise = noiseVariance[0]

            opt_x = minimize(calc_opt_lookAheadEntropy, x, method='L-BFGS-B',
                              bounds = bounds,
                              args=(f, source, iNoise, epsilon, xInt,
                                    mInt, vInt, KitPrior))
            x = opt_x.x           
        
        iCost = 0.
        if source == 0:
            if not ecl_opt:
                i = argWhereSameXtol(samplePoints, x)[0]
                samplePoints = np.delete(samplePoints, i, axis=1)

            y = []
            source = []
            ct = 0
            for s in range(nIS):
                if not ismemberTol(f.source, f.x, s, x, tolX):
                    ct += 1
                    ys, iCosts = g[s](x)
                    source += [s]
                    y += [ys]
                    iCost += iCosts
            
            y = np.array(y)
            x = np.tile(np.reshape(x, (d, 1)), ct)
        else:
            y, iCost = g[source](x)
            source = [source]

        f.update(source, x, y)
        
        if type(integrationPoints) is int:
            xInt, wInt = entropyImportance(f, epsilon, nInt, xMin, xMax)

        KitPrior = f.evaluateCovariancePrior([0]*nInt, xInt, f.source, f.x)
        vInt = evaluateVariance(f, [0]*nInt, xInt, KitPrior)
        mInt = evaluateMean(f, [0]*nInt, xInt, KitPrior)
        vInt[vInt <=0] = 1e-30 ## added

        H = contourEntropy(f, epsilon, xInt, wInt, mInt, vInt, KitPrior)
        tcost += iCost
        
        print('Iteration {:d}, contour entropy = {:.3e}, cost = {:.3e}'.format(iter+1, H, tcost))

        history = updateHistory(history, iter+1, source, x, y, H, tcost, f)
        
        if log:
            np.savetxt('CLoVER_history.txt', history, fmt='%1.6e')
        
        if H < tolH:
            print('CLoVER stopped because contour entropy is smaller than specified tolerance')            
            break

    if iter == nIter-1:
        print('CLoVER stopped because it reached the specified maximum number of iterations')
    
    return f, history


def choose_next_sample(f, cost, noiseVariance, epsilon, samplePoints, xInt,
                       wInt, mInt, vInt, KitPrior, tolX):
    d = f.dimension()
    nSample = int(samplePoints.size/d)
    HWx = entropy(f, epsilon, xInt, mInt, vInt, KitPrior)
            
    umax = -1.e10
    for s in range(f.nIS):
        for i in range(nSample):
            if d == 1:
                x = samplePoints[i]
            else:
                x = samplePoints[:, i]
            
            if not ismemberTol(f.source, f.x, s, x, tolX):
                if type(cost[s]) is float:
                    iCost = cost[s]
                else:
                    iCost = cost[s](x)
            
                if type(noiseVariance[s]) is float:
                    iNoise = noiseVariance[s]
                else:
                    iNoise = noiseVariance[s](x)
            
                EHWx = calc_lookAheadEntropy(f, s, x, iNoise, epsilon, xInt, mInt, vInt, KitPrior)


                u = np.sum(wInt*np.maximum(HWx.copy() - EHWx.copy(), 0.))/iCost

                if u > umax:
                    umax = u
                    xSample = x
                    sourceSample = s
 
        return sourceSample, xSample
    

def calc_lookAheadEntropy(f, source, x, noise, epsilon, xInt, mInt, vInt,
                          KitPrior):
    d = f.dimension()
    nInt = int(xInt.size/d)
  
    shift = st.norm.ppf(np.exp(-1))
    c = np.exp(-1.)

    tolr = np.min(f.getRankTolerance())
    
    KtsPrior = f.evaluateCovariancePrior(f.source, f.x, source, x)
    aux = f.evaluateCovariancePrior([0]*nInt, xInt, source, x)
    Kis = f.evaluateCovariancePrior([0]*nInt, xInt, source, x) - np.dot(KitPrior, f.applyKinverse(KtsPrior))
    Kss = noise + evaluateVariance(f, source, x, KxtPrior=KtsPrior.T)
    Kss = np.maximum(Kss, tolr)

    mean = mInt.copy()
    meanVariance = np.power(Kis.flatten(), 2)/Kss
    lookAheadVariance = vInt - meanVariance
    lookAheadVariance = np.maximum(lookAheadVariance, tolr)
    vInt[vInt <= 0] = 1e-30 ## added
    sigmaHat = np.sqrt(vInt)
  
    lookAheadStd = np.sqrt(lookAheadVariance)

    if type(epsilon) is int or type(epsilon) is float:
        eps = epsilon*lookAheadStd
    else:
        eps = epsilon(f, xInt)
        
    EHWx = np.zeros(nInt)
    for i in range(2):
        for j in range(2):
            num = -0.5*np.power(mean + ((-1.)**i)*eps + ((-1.)**j)*shift*lookAheadStd, 2)
            EHWx += np.exp(np.divide(num, vInt))
    EHWx *= c*np.divide(lookAheadStd, sigmaHat)

    return EHWx


def calc_opt_lookAheadEntropy(x, f, source, noise, epsilon, xInt, mInt, vInt, 
                              KitPrior):
    d = f.dimension()
    nInt = int(xInt.size/d)
  
    shift = st.norm.ppf(np.exp(-1))
    c = np.exp(-1.)

    tolr = np.min(f.getRankTolerance())
    
    KtsPrior = f.evaluateCovariancePrior(f.source, f.x, source, x)
    aux = f.evaluateCovariancePrior([0]*nInt, xInt, source, x)
    Kis = f.evaluateCovariancePrior([0]*nInt, xInt, source, x) - np.dot(KitPrior, f.applyKinverse(KtsPrior))
    Kss = noise + evaluateVariance(f, source, x, KxtPrior=KtsPrior.T)
    Kss = np.maximum(Kss, tolr)

    mean = mInt.copy()
    meanVariance = np.power(Kis.flatten(), 2)/Kss
    lookAheadVariance = vInt - meanVariance
    lookAheadVariance = np.maximum(lookAheadVariance, tolr)
    vInt[vInt <= 0] = 1e-30
    sigmaHat = np.sqrt(vInt)
  
    lookAheadStd = np.sqrt(lookAheadVariance)

    if type(epsilon) is int or type(epsilon) is float:
        eps = epsilon*lookAheadStd
    else:
        eps = epsilon(f, xInt)
        
    EHWx = np.zeros(nInt)
    for i in range(2):
        for j in range(2):
            num = -0.5*np.power(mean + ((-1.)**i)*eps + ((-1.)**j)*shift*lookAheadStd, 2)
            EHWx += np.exp(np.divide(num, vInt))
    EHWx *= c*np.divide(lookAheadStd, sigmaHat)

    return np.sum(EHWx)
