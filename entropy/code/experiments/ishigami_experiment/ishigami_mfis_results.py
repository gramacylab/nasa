"""
Calculates MFIS estimates for failure probability using ECL
adaptive designs from the Ishigami experiment and space-filling designs.

@author: D. Austin Cole  austin.cole8@vt.edu
"""
from datetime import date
import numpy as np
import pandas as pd
from pyDOE import lhs
import scipy.stats as ss
from sklearn.gaussian_process import GaussianProcessRegressor as GPR
from sklearn.gaussian_process.kernels import RBF
import warnings

import os
import sys
fileDir = os.path.dirname(os.path.abspath(__file__))
sourcePath = os.path.join(fileDir, '../../../../../MFISPy')
sys.path.append(sourcePath)
from mfis import BiasingDistribution
from mfis import MultiFidelityIS
from mfis.mv_independent_distribution import MVIndependentDistribution

sourcePath = os.path.join(fileDir, '../../')
sys.path.append(sourcePath)
from eclGP import IshigamiModel


ishigami_hf = IshigamiModel()
bounds = ((-np.pi, np.pi), (-np.pi, np.pi), (-np.pi, np.pi))

failure_threshold =  -10.244 #prob 1e-4 if uniform input distribution
def ishigami_limit_state_function(y):
    return -y + failure_threshold

## Defining input distribution
unif_dist = ss.uniform(-np.pi, 2*np.pi)
m1 = -1
s1 = 1
tnorm_d1_dist = ss.truncnorm(a=(-np.pi - m1)/s1, b=(np.pi-m1)/s1,
                             loc=m1, scale=s1)
m2 = 1.5
s2 = 1.5
tnorm_d2_dist = ss.truncnorm(a=(-np.pi-m2)/s2, b=(np.pi-m2)/s2,
                             loc=m2, scale=s2)
input_distribution = MVIndependentDistribution(
    distributions=[tnorm_d1_dist,tnorm_d2_dist,unif_dist])

## Estimate failure probability (alpha)
'''
num_failures = np.zeros((100,))
for i in range(len(num_failures)):
    new_X = input_distribution.draw_samples(1000000)
    new_Y = ishigami_hf.predict(new_X)
    num_failures[i] = np.sum(ishigami_limit_state_function(new_Y)>0)
    print(num_failures[i])
    print(i)

alpha = np.mean(num_failures)/len(new_X)
'''
alpha = 0.00019
dim = 3
MC_REPS = 30
mfis_probs = np.ones((MC_REPS, 5))
today = date.today()

ish_gauss_kernel = 1.0 * RBF(length_scale= np.repeat(.5, dim),
                     length_scale_bounds=(1e-4, 1e4))
N_HIGH_FIDELITY_INPUTS = 1000

n_init = 10*dim
n_select = 170
file_date = "041621"

ecl_designs_df = \
    pd.read_csv(f"data/Ish_ecl_designs_{file_date}.csv", header=None)
ecl_designs = np.array(ecl_designs_df)

designs_list = [ecl_designs]

for j in range(MC_REPS):
    ecl_gp = None
       
    for kk in range(len(designs_list)):
            design_X = designs_list[kk][0:100, ((dim+1)*j):((dim+1)*(j+1)-1)]
            design_Y = designs_list[kk][0:100, (dim+1)*(j+1)-1]
           
            ecl_gp =  GPR(kernel=ish_gauss_kernel, alpha=1e-6)
            ecl_gp.fit(design_X, design_Y)
            
    ## N-sized Space-filling GP
    X_lhs = 2*np.pi*lhs(3, n_init + n_select) - np.pi
    Y_lhs = ishigami_hf.predict(X_lhs)
    lhs_gp = GPR(kernel=ish_gauss_kernel, alpha=1e-6)
    lhs_gp.fit(X_lhs, Y_lhs)
    

    # Initialize Biasing Distributions
    ecl_bd =  BiasingDistribution(trained_surrogate=ecl_gp,
                            limit_state=ishigami_limit_state_function,
                            input_distribution=input_distribution)
    ecl_bd_ucb =  BiasingDistribution(trained_surrogate=ecl_gp,
                            limit_state=ishigami_limit_state_function,
                            input_distribution=input_distribution)
    lhs_bd = BiasingDistribution(trained_surrogate=lhs_gp,
                        limit_state=ishigami_limit_state_function,
                        input_distribution=input_distribution)
    lhs_bd_ucb = BiasingDistribution(trained_surrogate=lhs_gp,
                    limit_state=ishigami_limit_state_function,
                    input_distribution=input_distribution)
    
    ## Fit Biasing Distributions
    ecl_failed_inputs = np.empty((0, dim))   
    lhs_failed_inputs = np.empty((0, dim))
    ecl_failed_inputs_ucb = np.empty((0, dim))   
    lhs_failed_inputs_ucb = np.empty((0, dim))

    ## Generate sample outputs from GPs    
    for k in range(50):
        sample_inputs = input_distribution.draw_samples(100000)
        with warnings.catch_warnings():
            warnings.simplefilter("ignore")
            ecl_sample_outputs, ecl_sample_std = \
                ecl_gp.predict(sample_inputs, return_std=True)
            ecl_failed_inputs_new = sample_inputs[
                ishigami_limit_state_function(ecl_sample_outputs.flatten()) > 0,:]
            ecl_failed_inputs = \
                np.vstack((ecl_failed_inputs, ecl_failed_inputs_new))
    
            ecl_failed_inputs_ucb_new = sample_inputs[
                ishigami_limit_state_function(
                    ecl_sample_outputs.flatten()) + 1.645*ecl_sample_std > 0,:]
            ecl_failed_inputs_ucb = np.vstack((
                ecl_failed_inputs_ucb, ecl_failed_inputs_ucb_new))
            
        
            lhs_sample_outputs, lhs_sample_std = lhs_gp.predict(sample_inputs,
                                                              return_std=True)
            lhs_failed_inputs = sample_inputs[
                ishigami_limit_state_function(lhs_sample_outputs.flatten()) > 0,:]
            lhs_failed_inputs = np.vstack((lhs_failed_inputs, lhs_failed_inputs))
            
            lhs_failed_inputs_ucb_new = sample_inputs[
                ishigami_limit_state_function(
                    lhs_sample_outputs.flatten()) + 1.645*lhs_sample_std > 0,:]
            lhs_failed_inputs_ucb = np.vstack((
                lhs_failed_inputs_ucb, lhs_failed_inputs_ucb_new))
       
    ## Fit bias distributions
    ecl_bd.fit_from_failed_inputs(ecl_failed_inputs,
                               max_clusters=10, covariance_type='diag')
    ecl_bd_ucb.fit_from_failed_inputs(ecl_failed_inputs_ucb,
                           max_clusters=10, covariance_type='diag')
    

    
    ## Failure probability estimates
    XX_ecl = ecl_bd.draw_samples(N_HIGH_FIDELITY_INPUTS - n_init - n_select)
    hf_ecl_outputs = ishigami_hf.predict(XX_ecl)
    multi_IS_ecl = \
        MultiFidelityIS(limit_state=ishigami_limit_state_function,
                        biasing_distribution=ecl_bd,
                        input_distribution=input_distribution,
                        bounds=bounds)
    ecl_mfis_stats = \
        multi_IS_ecl.get_failure_prob_estimate(XX_ecl, hf_ecl_outputs)
    mfis_probs[j, 0] = ecl_mfis_stats[0]
    
    
    XX_ecl_ucb = \
        ecl_bd_ucb.draw_samples(N_HIGH_FIDELITY_INPUTS - n_init - n_select)
 
    hf_ecl_ucb_outputs = ishigami_hf.predict(XX_ecl_ucb)
    multi_IS_ecl_ucb = \
        MultiFidelityIS(limit_state=ishigami_limit_state_function,
                        biasing_distribution=ecl_bd_ucb,
                        input_distribution=input_distribution,
                        bounds=bounds)
    ecl_ucb_mfis_stats = \
        multi_IS_ecl_ucb.get_failure_prob_estimate(XX_ecl_ucb,
                                                   hf_ecl_ucb_outputs)
    mfis_probs[j, 1] = ecl_ucb_mfis_stats[0]

    ## MFIS estimate with lhs
    if len(lhs_failed_inputs) < 10:
        mfis_probs[j, 2] = 0
    else:
        lhs_bd.fit_from_failed_inputs(lhs_failed_inputs,
                            max_clusters=10, 
                            covariance_type='diag')
    
        XX_lhs = lhs_bd.draw_samples(N_HIGH_FIDELITY_INPUTS - n_init - n_select)
        hf_lhs_outputs = ishigami_hf.predict(XX_lhs)
        multi_IS_lhs = \
            MultiFidelityIS(limit_state=ishigami_limit_state_function,
                            biasing_distribution=lhs_bd,
                            input_distribution=input_distribution,
                            bounds=bounds)
        lhs_mfis_stats = \
            multi_IS_lhs.get_failure_prob_estimate(XX_lhs, hf_lhs_outputs)
        mfis_probs[j, 2] = lhs_mfis_stats[0]

    
    if len(lhs_failed_inputs_ucb) < 10:
        mfis_probs[j, 3] = 0
    else:
        lhs_bd_ucb.fit_from_failed_inputs(lhs_failed_inputs_ucb,
                            max_clusters=10, 
                            covariance_type='diag')
    
        XX_lhs_ucb = \
            lhs_bd_ucb.draw_samples(N_HIGH_FIDELITY_INPUTS - n_init - n_select)
        hf_lhs_ucb_outputs = ishigami_hf.predict(XX_lhs_ucb)
        multi_IS_lhs_ucb = \
            MultiFidelityIS(limit_state=ishigami_limit_state_function,
                            biasing_distribution=lhs_bd_ucb,
                            input_distribution=input_distribution,
                            bounds=bounds)
        lhs_ucb_mfis_stats = \
            multi_IS_lhs_ucb.get_failure_prob_estimate(XX_lhs_ucb,
                                                       hf_lhs_ucb_outputs)
        mfis_probs[j, 3] = lhs_ucb_mfis_stats[0]


    XX_mc = input_distribution.draw_samples(N_HIGH_FIDELITY_INPUTS)

    mc_outputs = ishigami_hf.predict(XX_mc)
    mc_failures = XX_mc[ishigami_limit_state_function(mc_outputs) > 0,:]
    mfis_probs[j, 4] = len(mc_failures) / N_HIGH_FIDELITY_INPUTS

    print(j)


    mfis_probs_df = pd.DataFrame(mfis_probs)
    mfis_probs_df = mfis_probs_df.rename(columns={0:'ECL',1:'ECL(UCB)',2:'LHS',
                                          3:'LHS(UCB)',4:'MC'})
    mfis_probs_df.to_csv(f'data/Ish_mfis_estimates_{today.strftime("%m%d%y")}.csv',
                         index=False)
