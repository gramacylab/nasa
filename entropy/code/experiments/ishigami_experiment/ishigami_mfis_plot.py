"""
Creates boxplots comparing failure probability estimates using ECL
adaptive designs from the Ishigami experiment and space-filling designs.

@author: D. Austin Cole  austin.cole8@vt.edu
"""
from datetime import date
import matplotlib.pyplot as plt
import matplotlib.ticker as mtick 
import pandas as pd
import seaborn as sns


today = date.today()
MC_REPS=30
alpha = 0.00019
filedate='041621'

## Upload and reshape data and add extra categories
mfis_probs_df = pd.read_csv(f"data/Ish_mfis_estimates_{filedate}.csv")

results_df = mfis_probs_df[['ECL','ECL(UCB)','LHS','LHS(UCB)']].melt()
results_df= results_df.rename(columns={'value': "Failure Probability",
                                       'variable':"Method"})
results_df['GP Design'] = 2*MC_REPS*['ECL'] +  2*MC_REPS*['LHS'] 
results_df['Classification'] = MC_REPS*['Mean'] + MC_REPS*['UCB'] +\
    MC_REPS*['Mean'] + MC_REPS*['UCB']


## Boxplot comparing failure probability estimates for ECL and LHS designs
fig, ax = plt.subplots(1, 1)
fig.set_size_inches(5, 4)   
plt.subplots_adjust(left=0.25, bottom=0.15, right=.98, top=.93) 
mfis_boxes =sns.boxplot(x='GP Design', y='Failure Probability', 
                        data=results_df, hue='Classification',
            palette=['white'])
mfis_boxes.axhline(alpha, c='r', linestyle='dashed')
mfis_boxes.set(ylabel='Failure probability')
mfis_boxes.set_title("Ishigami MFIS estimates with 1000 function evals")

ax.yaxis.set_major_formatter(mtick.FormatStrFormatter('%0.1e'))

## Change box outline colors manually
box_line_col = ['k','b', 'k','b']

for i,box_col in enumerate(box_line_col):
    mybox = mfis_boxes.artists[i]
    mybox.set_edgecolor(box_col)
    mybox.set_facecolor('white') 
    
    for j in range(i*6,i*6+6):
        line = mfis_boxes.lines[j]
        line.set_color(box_col)
        line.set_mfc(box_col)
        line.set_mec(box_col)

# ax.get_legend().remove()
## Add manual legend
import matplotlib.patches as mpatches
ucb_patch = mpatches.Patch(color='b',
                            label='UCB')
mean_patch = mpatches.Patch(color='k', label='Mean')
plt.legend(handles=[mean_patch, ucb_patch], loc='lower left')
plt.savefig(f'Ish_mfis_estimates_{today.strftime("%m%d%y")}.pdf',dpi=200)
#plt.savefig(f'Ish_mfis_estimates_{today.strftime("%m%d%y")}.png',dpi=200)
