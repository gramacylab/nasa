## Ishigami Experiment performed in the paper, "Entropy-based adaptive design for contour finding and estimating reliability"
### D. Austin Cole, Robert B. Gramacy, James E. Warner, Geoffrey F. Bomarito, Patrick E. Leser, William P. Leser

## Includes:	
- **get_ishigami_kriginv_adaptive_designs.R**: script to perform adaptive designs (EGRA, Ranjan, SUR, tIMSE, tMSE) using the KrigInv package
- **ishigami_contour_plots.py**: script to generate sensitivity and contour volume error plots shown in Section 4.2.
- **ishigami_contour_results.py**: script to take all adaptive designs to fit GPs and use a reference set to calculate sensitivity and area of the predicted failure region.
- **ishigami_ecl_clover_designs**: script to perform ECL and CLoVER adaptive designs. Uses the CLoVER package from Marques et al. (2018).
- **ishigami_generate_initial_designs.py**: script to generate Latin hypercube sample (LHS) designs for initial GP fits.
- **ishigami_mfis_plot.py**: script to generate box plots comparing failure probability estimates from multifidelity importance sampling results as in Section 5.1.
- **ishigami_mfis_results.py**: script to conduct multifidelity importance sampling based on ECL and LHS designs.