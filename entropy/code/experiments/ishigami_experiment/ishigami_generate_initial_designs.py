"""
Generates initial designs for training GPs before adaptive design. Used in
the Ishigami experiment.

@author: D. Austin Cole  austin.cole8@vt.edu
"""

from datetime import date
import numpy as np
from pyDOE import lhs

import os
import sys
fileDir = os.path.dirname(os.path.abspath(__file__))
sourcePath = os.path.join(fileDir, '../../')
sys.path.append(sourcePath)
from eclGP import IshigamiModel

today = date.today()
ishigami_model = IshigamiModel()

dim = 3
n_init = 10*dim
n_cand = 10*dim
MC_REPS = 30
stat_results = np.zeros((MC_REPS, 4))
initial_designs = np.zeros((n_init, 4*MC_REPS))


for i in range(MC_REPS):
    X0 = 2*np.pi*lhs(dim, n_init) - np.pi
    Y0 = ishigami_model.predict(X0)

    initial_designs[:, (4*i):(4*(i+1))] = np.hstack((X0, Y0.reshape((-1, 1))))


np.savetxt(f'data/Ish_initial_designs_{today.strftime("%m%d%y")}.csv',
           initial_designs, delimiter = ",")
