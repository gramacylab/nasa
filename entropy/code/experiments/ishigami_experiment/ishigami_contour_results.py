"""
Calculates sensitivity and contour volume based on adaptive designs for the
Ishigami experiment.

@author: D. Austin Cole  austin.cole8@vt.edu
"""
from datetime import date
import numpy as np
import pandas as pd
from sklearn.gaussian_process import GaussianProcessRegressor as GPR
from sklearn.gaussian_process.kernels import RBF

import os
import sys
fileDir = os.path.dirname(os.path.abspath(__file__))
sourcePath = os.path.join(fileDir, '../../')
sys.path.append(sourcePath)
from eclGP import IshigamiModel
from eclGP import calc_classification_stats


today = date.today()
today_string = today.strftime("%m%d%y")
ishigami_model = IshigamiModel(a_coef=5, b_coef=0.1)
bounds = ((-np.pi, np.pi), (-np.pi, np.pi), (-np.pi, np.pi))

threshold =  -10.244 #prob 1e-4
def ishigami_limit_state_function(y):
    return -y+threshold

## Get alpha estimate
'''
fail_count = np.zeros((100,))
for i in range(100):
    xx = 2*np.pi * lhs(3, samples=10000000) - np.pi
    yy = ishigami_model.predict(xx)
    fail_count[i] = np.sum(1*(ishigami_limit_state_function(yy) > 0))
    print(i)
alpha = np.sum(fail_count)/(100*len(fail_count))
'''

## Generate reference set
'''
dense_xx = 2*np.pi * lhs(3, samples=10000000) - np.pi
dense_yy = ishigami_model.predict(dense_xx)
XX = dense_xx[dense_yy < -1,:]
np.savetxt(f'ishigami_data/Ish_XX_10mil.csv', XX, delimiter = ",")
'''

## Upload reference set and find failures
XX_df = pd.read_csv(f"data/Ish_XX_10mil.csv", header=None)
XX = np.array(XX_df)
yy = ishigami_model.predict(XX)
true_failures = XX[ishigami_limit_state_function(yy) > 0,:]
true_classifications = 1*(ishigami_limit_state_function(yy) > 0)


dim = 3
ish_gauss_kernel = 1.0 * RBF(length_scale= np.repeat(.5, dim),
                     length_scale_bounds=(1e-4, 1e4))
n_init = 10*dim
n_cand = 10*dim


n_select = 170
MC_REPS = 2#30
dim = 3
n_init = 10*dim
file_date = "041621"
filedate = '060421'

clover_designs_df = \
    pd.read_csv(f"data/Ish_clover_designs_{file_date}.csv", header=None)
clover_designs = np.array(clover_designs_df)
ecl_designs_df = \
    pd.read_csv(f"data/Ish_ecl_designs_{file_date}.csv", header=None)
ecl_designs = np.array(ecl_designs_df)

ecl_batch_designs_df = \
    pd.read_csv(f"data/Ish_ecl_batch10_designs_{file_date}.csv", header=None)
ecl_batch_designs = np.array(ecl_batch_designs_df)
'''
egra_designs_df = pd.read_csv(f"data/Ish_egra_designs_{file_date}.csv")
egra_designs = np.array(egra_designs_df)
ranjan_designs_df = pd.read_csv(f"data/Ish_ranjan_designs_{file_date}.csv")
ranjan_designs = np.array(ranjan_designs_df)
sur_designs_df = pd.read_csv(f"data/Ish_sur_designs_{file_date}.csv")
sur_designs = np.array(sur_designs_df)
tmse_designs_df = pd.read_csv(f"data/Ish_tmse_designs_{file_date}.csv")
tmse_designs = np.array(tmse_designs_df)
timse_designs_df = pd.read_csv(f"data/Ish_timse_designs_{file_date}.csv")
timse_designs = np.array(timse_designs_df)
'''

n_gps = np.int(n_select/5)

ecl_area = np.zeros((MC_REPS, n_gps+1))
ecl_batch_area = np.zeros((MC_REPS, n_gps+1))
#egra_area = np.zeros((MC_REPS, n_gps+1))
clover_area = np.zeros((MC_REPS, n_gps+1))
#ranjan_area = np.zeros((MC_REPS, n_gps+1))
#sur_area = np.zeros((MC_REPS, n_gps+1))
#tmse_area = np.zeros((MC_REPS, n_gps+1))
#timse_area = np.zeros((MC_REPS, n_gps+1))


ecl_sens = np.zeros((MC_REPS, n_gps+1))
ecl_batch_sens = np.zeros((MC_REPS, n_gps+1))
#egra_sens = np.zeros((MC_REPS, n_gps+1))
clover_sens = np.zeros((MC_REPS, n_gps+1))
#ranjan_sens = np.zeros((MC_REPS, n_gps+1))
#sur_sens = np.zeros((MC_REPS, n_gps+1))
#tmse_sens = np.zeros((MC_REPS, n_gps+1))
#timse_sens = np.zeros((MC_REPS, n_gps+1))


designs_list = [ecl_designs, ecl_batch_designs, clover_designs]#, egra_designs,
      #          tmse_designs, timse_designs, ranjan_designs, sur_designs]
area_list = [ecl_area, ecl_batch_area, clover_area]#,egra_area,
       #         tmse_area, timse_area, ranjan_area, sur_area]
sens_list = [ecl_sens, ecl_batch_sens, clover_sens]#,egra_sens, 
        #        tmse_sens, timse_sens, ranjan_sens, sur_sens]

for j in range(MC_REPS):
    for k in range(0, n_select+1, 5):
        k_ind = np.int(k/5)

        for kk in range(len(designs_list)):
            design_X = designs_list[kk][0:(n_init+k), 
                                        ((dim+1)*j):((dim+1)*(j+1)-1)]
            design_Y = designs_list[kk][0:(n_init+k), (dim+1)*(j+1)-1]
           
            fit_gp =  GPR(kernel=ish_gauss_kernel, alpha=1e-6)
            fit_gp.fit(design_X, design_Y)
            gp_preds = fit_gp.predict(XX)
            new_failures = XX[ishigami_limit_state_function(gp_preds) > 0,:]
            gp_classifications = 1*(ishigami_limit_state_function(gp_preds) > 0)
            
            
            sens_list[kk][j,k_ind] = calc_classification_stats(
                    true_classifications, gp_classifications)[0]
            area_list[kk][j,k_ind] = np.sum(gp_classifications)
    print(j)  
    
    np.savetxt(f'data/Ish_clover_area_{today_string}.csv',
               clover_area, delimiter = ",")
    np.savetxt(f'data/Ish_clover_sens_{today_string}.csv', 
               clover_sens, delimiter = ",")
    
    np.savetxt(f'data/Ish_ecl_sens_{today_string}.csv', 
               ecl_sens, delimiter = ",")
    np.savetxt(f'data/Ish_ecl_area_{today_string}.csv', 
               ecl_area, delimiter = ",")
    
    np.savetxt(f'data/Ish_ecl_batch_sens_{today_string}.csv', 
               ecl_batch_sens, delimiter = ",")
    np.savetxt(f'data/Ish_ecl_batch_area_{today_string}.csv', 
               ecl_batch_area, delimiter = ",")
''' 
    np.savetxt(f'data/Ish_egra_sens_{today_string}.csv', 
               egra_sens, delimiter = ",")
    np.savetxt(f'data/Ish_egra_area_{today_string}.csv', 
               egra_area, delimiter = ",")
    
    np.savetxt(f'data/Ish_tmse_area_{today_string}.csv', 
               tmse_area, delimiter = ",")
    np.savetxt(f'data/Ish_tmse_sens_{today_string}.csv', 
               tmse_sens, delimiter = ",")
    
    np.savetxt(f'data/Ish_timse_area_{today_string}.csv', 
               timse_area, delimiter = ",")
    np.savetxt(f'data/Ish_timse_sens_{today_string}.csv', 
               timse_sens, delimiter = ",")
    
    np.savetxt(f'data/Ish_sur_sens_{today_string}.csv', 
               sur_sens, delimiter = ",")
    np.savetxt(f'data/Ish_sur_area_{today_string}.csv', 
               sur_area, delimiter = ",")
    
    np.savetxt(f'data/Ish_ranjan_sens_{today_string}.csv', 
               ranjan_sens, delimiter = ",")
    np.savetxt(f'data/Ish_ranjan_area_{today_string}.csv', 
               ranjan_area, delimiter = ",")
'''

