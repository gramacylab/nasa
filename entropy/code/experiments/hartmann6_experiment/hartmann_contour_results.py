"""
Calculates sensitivity and contour volume based on adaptive designs for the
Hartmann6 experiment.

@author: D. Austin Cole  austin.cole8@vt.edu
"""
from datetime import date
import numpy as np
import pandas as pd
from sklearn.gaussian_process import GaussianProcessRegressor as GPR
from sklearn.gaussian_process.kernels import RBF

import os
import sys
fileDir = os.path.dirname(os.path.abspath(__file__))
sourcePath = os.path.join(fileDir, '../../')
sys.path.append(sourcePath)
from eclGP import HartmannModel
from eclGP import calc_classification_stats

today = date.today()
today_string = today.strftime("%m%d%y")
hartmann_model = HartmannModel()

threshold = 2.63
def hartmann_limit_state_function(y):
    return y-threshold
'''
## Get alpha/area estimate
fail_count = np.zeros((100,))
for i in range(100):
    xx = lhs(6, samples=10000000)
    yy = hartmann_model.predict(xx)
    fail_count[i] = np.sum(1*(hartmann_limit_state_function(yy) > 0))
    print(i)
alpha = np.sum(fail_count)/(100*len(fail_count))
'''

# XX = dense_xx[dense_yy > 1.75,:]
# np.savetxt(f'data/Hart_XX_greater_1pt75.csv', XX, delimiter = ",")
    
XX_df = pd.read_csv(f"data/Hart_XX_greater_1pt75.csv", header=None)
XX = np.array(XX_df)
yy = hartmann_model.predict(XX)
true_failures = XX[yy > threshold,:]
true_classifications = 1*(yy > threshold)

dim = 6
hartmann_gauss_kernel = 1.0 * RBF(length_scale= np.repeat(.5, dim),
                     length_scale_bounds=(1e-4, 1e4))

n_init = 10*dim
n_cand = 10*dim
n_select = 500 - n_init
MC_REPS = 30

file_date = "041621"

clover_designs_df = pd.read_csv(f"data/Hart_clover_designs_{file_date}.csv",
                                header=None)
clover_designs = np.array(clover_designs_df)
ecl_designs_df = pd.read_csv(f"data/Hart_ecl_designs_{file_date}.csv",
                             header=None)
ecl_designs = np.array(ecl_designs_df)
ecl_batch_designs_df = pd.read_csv(f"data/Hart_ecl_batch10_designs_{file_date}.csv",
                                   header=None)
ecl_batch_designs = np.array(ecl_batch_designs_df)
'''
egra_designs_df = pd.read_csv(f"data/Hart_egra_designs_{file_date}.csv")
egra_designs = np.array(egra_designs_df)
ranjan_designs_df = pd.read_csv(f"data/Hart_ranjan_designs_{file_date}.csv")
ranjan_designs = np.array(ranjan_designs_df)
sur_designs_df = pd.read_csv(f"data/Hart_sur_designs_{file_date}.csv")
sur_designs = np.array(sur_designs_df)
timse_designs_df = pd.read_csv(f"data/Hart_timse_designs_{file_date}.csv")
timse_designs = np.array(timse_designs_df)
tmse_designs_df = pd.read_csv(f"data/Hart_tmse_designs_{file_date}.csv")
tmse_designs = np.array(tmse_designs_df)
'''

n_gps = np.int(n_select/20)

ecl_area = np.zeros((MC_REPS, n_gps+1))
ecl_batch_area = np.zeros((MC_REPS, n_gps+1))
clover_area = np.zeros((MC_REPS, n_gps+1))
#egra_area = np.zeros((MC_REPS, n_gps+1))
#ranjan_area = np.zeros((MC_REPS, n_gps+1))
#sur_area = np.zeros((MC_REPS, n_gps+1))
#tmse_area = np.zeros((MC_REPS, n_gps+1))
#timse_area = np.zeros((MC_REPS, n_gps+1))


ecl_sens = np.zeros((MC_REPS, n_gps+1))
ecl_batch_sens = np.zeros((MC_REPS, n_gps+1))
clover_sens = np.zeros((MC_REPS, n_gps+1))
'''
egra_sens = np.zeros((MC_REPS, n_gps+1))
tmse_sens = np.zeros((MC_REPS, n_gps+1))
timse_sens = np.zeros((MC_REPS, n_gps+1))
ranjan_sens = np.zeros((MC_REPS, n_gps+1))
sur_sens = np.zeros((MC_REPS, n_gps+1))
'''

designs_list = [ecl_designs, ecl_batch_designs, clover_designs]#, egra_designs,
        #        ranjan_designs, sur_designs, timse_designs, tmse_designs]
area_list = [ecl_area, ecl_batch_area, clover_area]#, egra_area,
         #       ranjan_area, sur_area, timse_area, tmse_area]
sens_list = [ecl_sens, ecl_batch_sens, clover_sens]#, egra_sens,
         #       ranjan_sens, sur_sens, timse_sens, tmse_sens]

for j in range(MC_REPS):
    for k in range(0, n_select+1, 20):
        k_ind = np.int(k/20)

        for kk in range(len(designs_list)):
            design_X = designs_list[kk][0:(n_init+k), 
                                        ((dim+1)*j):((dim+1)*(j+1)-1)]
            design_Y = designs_list[kk][0:(n_init+k), (dim+1)*(j+1)-1]
           
            fit_gp =  GPR(kernel=hartmann_gauss_kernel, alpha=1e-6)
            fit_gp.fit(design_X, design_Y)
            gp_preds = fit_gp.predict(XX)
            new_failures = XX[hartmann_limit_state_function(gp_preds) > 0,:]
            gp_classifications = 1*(hartmann_limit_state_function(gp_preds) > 0)
            
            sens_list[kk][j,k_ind] = calc_classification_stats(
                true_classifications, gp_classifications)[0]
            area_list[kk][j,k_ind] = np.sum(gp_classifications)
    print(j)   


    np.savetxt(f'data/Hart_ecl_sens_{today_string}.csv', 
                ecl_sens, delimiter = ",")
    np.savetxt(f'data/Hart_ecl_area_{today_string}.csv', 
                ecl_area, delimiter = ",")

    np.savetxt(f'data/Hart_ecl_batch10_sens_{today_string}.csv', 
                ecl_batch_sens, delimiter = ",")
    np.savetxt(f'data/Hart_ecl_batch10_area_{today_string}.csv', 
                ecl_batch_area, delimiter = ",")

    np.savetxt(f'data/Hart_clover_sens_{today_string}.csv', 
                clover_sens, delimiter = ",")
    np.savetxt(f'data/Hart_clover_area_{today_string}.csv',
                clover_area, delimiter = ",")
'''
    np.savetxt(f'data/Hart_egra_sens_{today_string}.csv', 
                egra_sens, delimiter = ",")
    np.savetxt(f'data/Hart_egra_area_{today_string}.csv', 
                egra_area, delimiter = ",")
    
    np.savetxt(f'data/Hart_ranjan_sens_{today_string}.csv', 
                ranjan_sens, delimiter = ",")
    np.savetxt(f'data/Hart_ranjan_area_{today_string}.csv', 
                ranjan_area, delimiter = ",")
    
    np.savetxt(f'data/Hart_sur_sens_{today_string}.csv', 
                sur_sens, delimiter = ",")
    np.savetxt(f'data/Hart_sur_area_{today_string}.csv', 
                sur_area, delimiter = ",")
    
    np.savetxt(f'data/Hart_timse_sens_{today_string}.csv', 
                timse_sens, delimiter = ",")
    np.savetxt(f'data/Hart_timse_area_{today_string}.csv', 
                timse_area, delimiter = ",")
    
    np.savetxt(f'data/Hart_tmse_sens_{today_string}.csv', 
                tmse_sens, delimiter = ",")
    np.savetxt(f'data/Hart_tmse_area_{today_string}.csv', 
                tmse_area, delimiter = ",")
'''


