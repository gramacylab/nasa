"""
Creates sensitivity and contour volume error plots for Hartmann6 experiment.

@author: D. Austin Cole  austin.cole8@vt.edu
"""

from datetime import date
from matplotlib import gridspec
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns

import os
import sys
fileDir = os.path.dirname(os.path.abspath(__file__))
sourcePath = os.path.join(fileDir, '../../')
sys.path.append(sourcePath)
from eclGP import HartmannModel

today = date.today()
hartmann_model = HartmannModel()


threshold = 2.63
def hartmann_limit_state_function(y):
    return y-threshold

XX_df = pd.read_csv(f"data/Hart_XX_greater_1pt75.csv", header=None)
XX = np.array(XX_df)
yy = hartmann_model.predict(XX)
true_failures = XX[hartmann_limit_state_function(yy) > 0,:]


dim = 6
n_init = 10*dim
n_select = 500 - n_init
MC_REPS = 30
file_date = "041621"

n_markers = np.int(n_select/20) + 1

ecl_sens = pd.read_csv(f'data/Hart_ecl_sens_{file_date}.csv', header=None)
ecl_area = pd.read_csv(f'data/Hart_ecl_area_{file_date}.csv', header=None)
ecl_batch_sens = pd.read_csv(f'data/Hart_ecl_batch_sens_{file_date}.csv',
                        header=None)
ecl_batch_area = pd.read_csv(f'data/Hart_ecl_batch_area_{file_date}.csv', 
                        header=None)
egra_sens = pd.read_csv(f'data/Hart_egra_sens_{file_date}.csv', header=None)
egra_area = pd.read_csv(f'data/Hart_egra_area_{file_date}.csv', header=None)
clover_sens = pd.read_csv(f'data/Hart_clover_sens_{file_date}.csv', header=None)
clover_area = pd.read_csv(f'data/Hart_clover_area_{file_date}.csv', header=None)
ranjan_sens = pd.read_csv(f'data/Hart_ranjan_sens_{file_date}.csv', header=None)
ranjan_area = pd.read_csv(f'data/Hart_ranjan_area_{file_date}.csv', header=None)
sur_sens = pd.read_csv(f'data/Hart_sur_sens_{file_date}.csv', header=None)
sur_area = pd.read_csv(f'data/Hart_sur_area_{file_date}.csv', header=None)
timse_sens = pd.read_csv(f'data/Hart_timse_sens_{file_date}.csv', header=None)
timse_area = pd.read_csv(f'data/Hart_timse_area_{file_date}.csv', header=None)
tmse_sens = pd.read_csv(f'data/Hart_tmse_sens_{file_date}.csv', header=None)
tmse_area = pd.read_csv(f'data/Hart_tmse_area_{file_date}.csv', header=None)


## Change to relative error of area estimates
true_count = 11047.1
clover_err = np.abs(clover_area - true_count)/true_count
ecl_err = np.abs(ecl_area - true_count)/true_count
ecl_batch_err = np.abs(ecl_batch_area - true_count)/true_count
egra_err = np.abs(egra_area - true_count)/true_count
tmse_err = np.abs(tmse_area - true_count)/true_count
timse_err = np.abs(timse_area - true_count)/true_count
sur_err = np.abs(sur_area - true_count)/true_count
ranjan_err = np.abs(ranjan_area - true_count)/true_count


methods = ['ECL','ECL.b','CLoVER','EGRA','Ranjan','SUR','tIMSE','tMSE']
color_palette = ['royalblue','aqua', 'g', 'magenta',
                 'brown', 'orange','r','purple']
dash_styles = [(10,0), (4,3), (3, 5, 1, 1), (2,2),  
               (4, 1), (3, 1,1,1), (1, 2,2,1), (2, 3, 2, 1)]

##--------Combine results into dataframe for each subplot-------##
## GP sensitivity over function evaluations
combined_sens_mc = pd.concat([ecl_sens.iloc[:,0:n_markers].melt(),
                              ecl_batch_sens.iloc[:,0:n_markers].melt(),
                              clover_sens.iloc[:,0:n_markers].melt(),
                              egra_sens.iloc[:,0:n_markers].melt(),
                              ranjan_sens.iloc[:,0:n_markers].melt(),
                              sur_sens.iloc[:,0:n_markers].melt(),
                              timse_sens.iloc[:,0:n_markers].melt(),
                              tmse_sens.iloc[:,0:n_markers].melt()])

combined_sens_mc = combined_sens_mc.rename(columns={"value": "Sensitivity"})
combined_sens_mc = \
    combined_sens_mc.rename(columns={"variable": "Function evaluations"})
combined_sens_mc['Function evaluations'] = \
    (20*combined_sens_mc['Function evaluations']) + n_init
descript = [item for item in methods for i in range(MC_REPS*(n_markers))]
combined_sens_mc['Method'] = descript 


## Final designs sensitivity
last_sens_mc = pd.concat([ecl_sens.iloc[:,n_markers-1],
                          ecl_batch_sens.iloc[:,n_markers-1],
                          clover_sens.iloc[:,n_markers-1],
                          egra_sens.iloc[:,n_markers-1],
                          ranjan_sens.iloc[:,n_markers-1],
                          sur_sens.iloc[:,n_markers-1],
                          timse_sens.iloc[:,n_markers-1],
                          tmse_sens.iloc[:,n_markers-1]],
                          ignore_index=True)

descript = pd.DataFrame([item for item in methods for i in range(MC_REPS)])
last_sens_df = pd.concat([last_sens_mc, descript],axis=1)
last_sens_df = last_sens_df.set_axis(['Sensitivity', 'Method'], axis=1)


## Absolute relative error of contour volume over function evaluations
ecl_err_df = pd.DataFrame(ecl_err)
ecl_batch_err_df = pd.DataFrame(ecl_batch_err)
egra_err_df = pd.DataFrame(egra_err)
clover_err_df = pd.DataFrame(clover_err)
ranjan_err_df = pd.DataFrame(ranjan_err)
sur_err_df = pd.DataFrame(sur_err)
timse_err_df = pd.DataFrame(timse_err)
tmse_err_df = pd.DataFrame(tmse_err)
combined_err_mc = pd.concat([ecl_err_df.iloc[:,0:n_markers].melt(), 
                             ecl_batch_err_df.iloc[:,0:n_markers].melt(), 
                             clover_err_df.iloc[:,0:n_markers].melt(),
                             egra_err_df.iloc[:,0:n_markers].melt(),  
                             ranjan_err_df.iloc[:,0:n_markers].melt(),
                             sur_err_df.iloc[:,0:n_markers].melt(),
                             timse_err_df.iloc[:,0:n_markers].melt(),
                             tmse_err_df.iloc[:,0:n_markers].melt()])

combined_err_mc = combined_err_mc.rename(columns={"value": "Relative error"})
combined_err_mc = \
    combined_err_mc.rename(columns={"variable": "Function evaluations"})
combined_err_mc['Function evaluations'] = \
    (20*combined_err_mc['Function evaluations']) + n_init
descript = [item for item in methods for i in range(MC_REPS*(n_markers))] 
combined_err_mc['Method'] = descript 


## Final design absolute relative error of contour volume
last_re_mc = pd.concat([ecl_err_df.iloc[:,n_markers-1],
                        ecl_batch_err_df.iloc[:,n_markers-1],
                        clover_err_df.iloc[:,n_markers-1],
                        egra_err_df.iloc[:,n_markers-1],
                        ranjan_err_df.iloc[:,n_markers-1],
                        sur_err_df.iloc[:,n_markers-1],
                        timse_err_df.iloc[:,n_markers-1],
                        tmse_err_df.iloc[:,n_markers-1]],
                        ignore_index=True)

descript = pd.DataFrame([item for item in methods for i in range(MC_REPS)])

last_re_df = pd.concat([last_re_mc, descript],axis=1)
last_re_df = last_re_df.set_axis(['Relative error', 'Method'], axis=1)


###----All one figure----###
fig = plt.figure(figsize=(12, 5)) 

plt.subplots_adjust(left=0.05, bottom=0.15, right=.98, top=.98, wspace=0.15,
                    hspace=0.05)
outer = gridspec.GridSpec(1, 2, width_ratios=[4, 4]) 
gs1 = gridspec.GridSpecFromSubplotSpec(1, 2, subplot_spec = outer[0], 
                                       width_ratios=[3, 1], wspace=0.08)
gs2 = gridspec.GridSpecFromSubplotSpec(1, 2, subplot_spec = outer[1], 
                                       width_ratios=[3, 1], wspace = .21)

ax1 = plt.subplot(gs1[0])
ax1 = sns.lineplot(x="Function evaluations",
                    y="Sensitivity", hue="Method", style='Method',
                    data=combined_sens_mc, 
                    palette=color_palette,
                    markers=False, dashes=dash_styles, ci=None)
ax1.set_xticks(range(n_init,n_init+n_select+1, 60))
ax1.set(ylim=(-.05, 1.05))
ax1.get_legend().remove()
ax2 = plt.subplot(gs1[1])
ax2 = sns.boxplot( x="Method", y="Sensitivity", data=last_sens_df,
                 palette=color_palette, fliersize=4)
ax2.set_xticklabels(ax2.get_xticklabels(),rotation=90)
ax2.set(xlabel=None, ylabel=None, ylim=(-.05, 1.05))
ax2.set_yticklabels([])

ax3 = plt.subplot(gs2[0])
ax3 = sns.lineplot(x="Function evaluations",
                    y="Relative error", hue="Method", style='Method',
                    data=combined_err_mc, 
                    palette=color_palette,
                    dashes=dash_styles,
                    markers=False, ci=None)

ax3.set(yscale='log', ylim=(1e-2, 1.5), 
        ylabel='Absolute relative error of volume estimate')
ax3.set_xticks(range(n_init,n_init+n_select+1, 60))
ax3.get_legend().remove()
ax4 = plt.subplot(gs2[1])
ax4 = sns.boxplot( x="Method", y="Relative error", data=last_re_df,
                 palette=color_palette, fliersize=4)
ax4.set_xticklabels(ax4.get_xticklabels(),rotation=90)
ax4.set(yscale='log', ylim=(5e-6, 1.5))
ax4.set(xlabel=None, ylabel=None)

st = fig.suptitle("Hartmann-6", fontsize="x-large")
st.set_y(0.95)
fig.subplots_adjust(top=0.90)
# plt.tight_layout()
plt.savefig(f'Hart_results_{today.strftime("%m%d%y")}.pdf')
#plt.savefig('Hart_results.png')
