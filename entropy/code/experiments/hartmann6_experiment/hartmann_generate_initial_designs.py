"""
Generates initial designs for training GPs before adaptive design. Used in
the Hartmann6 experiment.

@author: D. Austin Cole  austin.cole8@vt.edu
"""

from datetime import date
import numpy as np
from pyDOE import lhs

import os
import sys
fileDir = os.path.dirname(os.path.abspath(__file__))
sourcePath = os.path.join(fileDir, '../../')
sys.path.append(sourcePath)
from eclGP import HartmannModel

hartmann_model = HartmannModel()
dim=6

n_init = 10*dim
MC_REPS = 30
stat_results = np.zeros((MC_REPS,dim+1))
initial_designs = np.zeros((n_init, (dim+1)*MC_REPS))
today = date.today()


for i in range(MC_REPS):
    X0 = lhs(dim, n_init)
    Y0 = hartmann_model.predict(X0)

    initial_designs[:,((dim+1)*i):((dim+1)*(i+1))] = \
        np.hstack((X0, Y0.reshape((-1,1))))


np.savetxt(f'data/Hart_initial_designs_{today.strftime("%m%d%y")}.csv',
           initial_designs, delimiter = ",")
