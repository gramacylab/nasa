"""
Calculates MFIS estimates for failure probability using ECL
adaptive designs from the Hartmann6 experiment and space-filling designs.

@author: D. Austin Cole  austin.cole8@vt.edu
"""

from datetime import date
import numpy as np
import pandas as pd
from pyDOE import lhs
import scipy.stats as ss
from sklearn.gaussian_process import GaussianProcessRegressor as GPR
from sklearn.gaussian_process.kernels import RBF

import os
import sys
fileDir = os.path.dirname(os.path.abspath(__file__))
sourcePath = os.path.join(fileDir, '../../../../../MFISPy')
sys.path.append(sourcePath)
from mfis import BiasingDistribution
from mfis import MultiFidelityIS
from mfis.mv_independent_distribution import MVIndependentDistribution

sourcePath = os.path.join(fileDir, '../../')
sys.path.append(sourcePath)
from eclGP import HartmannModel

today = date.today()

hartmann_model = HartmannModel()
bounds = ((0,1),(0,1),(0,1),(0,1),(0,1),(0,1))
threshold = 2.63
def hartmann_limit_state_function(y):
    return y - threshold

unif_dist = ss.uniform(0,1)
m1 = .5
s1 = .1
tnorm_dist = ss.truncnorm(a=(0-m1)/s1, b=(1-m1)/s1, loc=m1, scale=s1)

input_distribution = MVIndependentDistribution(
    distributions=[tnorm_dist, tnorm_dist, tnorm_dist,
                   tnorm_dist, tnorm_dist, tnorm_dist])

## Estimate failure probability (alpha)
'''
num_failures = np.zeros((100,))
for i in range(len(num_failures)):
    new_X = input_distribution.draw_samples(1000000)
    new_Y = hartmann_model.predict(new_X)
    num_failures[i] = np.sum(hartmann_limit_state_function(new_Y)>0)
    print(num_failures[i])
    print(i)
alpha = np.mean(num_failures)/len(new_X)
'''
alpha = 0.00000996

MC_REPS = 30
mfis_probs = np.ones((MC_REPS, 5))


hart_gauss_kernel = 1.0 * RBF(length_scale= np.repeat(.5, 6),
                     length_scale_bounds=(1e-4, 1e4))
N_HIGH_FIDELITY_INPUTS = 1000
dim = 6
n_init = 10*dim
n_select = 500 - n_init
file_date = "041621"

ecl_designs_df = pd.read_csv(f"data/Hart_ecl_designs_{file_date}.csv",
                             header=None)
ecl_designs = np.array(ecl_designs_df)

designs_list = [ecl_designs]

    
for j in range(MC_REPS):
    ecl_gp = None
       
    for kk in range(len(designs_list)):
            design_X = designs_list[kk][:, ((dim+1)*j):((dim+1)*(j+1)-1)]
            design_Y = designs_list[kk][:, (dim+1)*(j+1)-1]
           
            ecl_gp =  GPR(kernel=hart_gauss_kernel, alpha=1e-6)
            ecl_gp.fit(design_X, design_Y)
            
    ## N-sized Space-filling GP
    X_lhs = lhs(6, n_init + n_select)
    Y_lhs = hartmann_model.predict(X_lhs)
    lhs_gp = GPR(kernel=hart_gauss_kernel, alpha=1e-6)
    lhs_gp.fit(X_lhs, Y_lhs)
    

    # Initialize Biasing Distributions
    ecl_bd =  BiasingDistribution(trained_surrogate=ecl_gp,
                            limit_state=hartmann_limit_state_function,
                            input_distribution=input_distribution)
    ecl_bd_ucb =  BiasingDistribution(trained_surrogate=ecl_gp,
                            limit_state=hartmann_limit_state_function,
                            input_distribution=input_distribution)
    lhs_bd = BiasingDistribution(trained_surrogate=lhs_gp,
                        limit_state=hartmann_limit_state_function,
                        input_distribution=input_distribution)
    lhs_bd_ucb = BiasingDistribution(trained_surrogate=lhs_gp,
                    limit_state=hartmann_limit_state_function,
                    input_distribution=input_distribution)
    
    ## Fit Biasing Distributions
    ecl_failed_inputs = np.empty((0, dim))   
    lhs_failed_inputs = np.empty((0, dim))
    ecl_failed_inputs_ucb = np.empty((0, dim))   
    lhs_failed_inputs_ucb = np.empty((0, dim))

    # Get sample outputs from GPs and classify failures  
    for k in range(500):
        sample_inputs = input_distribution.draw_samples(100000)
        
        ecl_sample_outputs, ecl_sample_std = \
            ecl_gp.predict(sample_inputs, return_std=True)
        ecl_failed_inputs_new = sample_inputs[
            hartmann_limit_state_function(ecl_sample_outputs.flatten()) > 0,:]
        ecl_failed_inputs = np.vstack((ecl_failed_inputs,
                                       ecl_failed_inputs_new))

        ecl_failed_inputs_ucb_new = sample_inputs[
            hartmann_limit_state_function(
                ecl_sample_outputs.flatten() + 1.645*ecl_sample_std) > 0,:]
        ecl_failed_inputs_ucb = np.vstack((ecl_failed_inputs_ucb,
                                           ecl_failed_inputs_ucb_new))
        
    
        lhs_sample_outputs, lhs_sample_std = lhs_gp.predict(sample_inputs,
                                                          return_std=True)
        lhs_failed_inputs = sample_inputs[
            hartmann_limit_state_function(lhs_sample_outputs.flatten()) > 0,:]
        lhs_failed_inputs = np.vstack((lhs_failed_inputs, lhs_failed_inputs))
        
        lhs_failed_inputs_ucb_new = sample_inputs[
            hartmann_limit_state_function(
                lhs_sample_outputs.flatten() + 1.645*lhs_sample_std) > 0,:]
        lhs_failed_inputs_ucb = np.vstack((lhs_failed_inputs_ucb,
                                           lhs_failed_inputs_ucb_new))
        if (k % 100) == 0:
            print(k)

    ecl_bd.fit_from_failed_inputs(ecl_failed_inputs,
                               max_clusters=10, covariance_type='diag')
    ecl_bd_ucb.fit_from_failed_inputs(ecl_failed_inputs_ucb,
                           max_clusters=10, covariance_type='diag')
    

    
    ## Failure probability estimates
    XX_ecl = ecl_bd.draw_samples(N_HIGH_FIDELITY_INPUTS - n_init - n_select)
    hf_ecl_outputs = hartmann_model.predict(XX_ecl)
    multi_IS_ecl = \
        MultiFidelityIS(limit_state=hartmann_limit_state_function,
                        biasing_distribution=ecl_bd,
                        input_distribution=input_distribution,
                        bounds=bounds)
    ecl_mfis_stats = multi_IS_ecl.get_failure_prob_estimate(XX_ecl,
                                                          hf_ecl_outputs)
    mfis_probs[j, 0] = ecl_mfis_stats[0]
    
    
    XX_ecl_ucb = \
        ecl_bd_ucb.draw_samples(N_HIGH_FIDELITY_INPUTS - n_init - n_select)
 
    hf_ecl_ucb_outputs = hartmann_model.predict(XX_ecl_ucb)
    multi_IS_ecl_ucb = \
        MultiFidelityIS(limit_state=hartmann_limit_state_function,
                        biasing_distribution=ecl_bd_ucb,
                        input_distribution=input_distribution,
                        bounds=bounds)
    ecl_ucb_mfis_stats = multi_IS_ecl_ucb.get_failure_prob_estimate(
        XX_ecl_ucb, hf_ecl_ucb_outputs)
    mfis_probs[j, 1] = ecl_ucb_mfis_stats[0]

    ## MFIS estimate with LHS
    if len(lhs_failed_inputs) < 10:
        mfis_probs[j, 2] = 0
    else:
        lhs_bd.fit_from_failed_inputs(lhs_failed_inputs,
                           max_clusters=10, 
                           covariance_type='diag')
    
        XX_lhs = lhs_bd.draw_samples(N_HIGH_FIDELITY_INPUTS - n_init - n_select)
        hf_lhs_outputs = hartmann_model.predict(XX_lhs)
        multi_IS_lhs = \
            MultiFidelityIS(limit_state=hartmann_limit_state_function,
                            biasing_distribution=lhs_bd,
                            input_distribution=input_distribution,
                            bounds=bounds)
        lhs_mfis_stats = multi_IS_lhs.get_failure_prob_estimate(XX_lhs,
                                                              hf_lhs_outputs)
        mfis_probs[j, 2] = lhs_mfis_stats[0]

    
    if len(lhs_failed_inputs_ucb) < 10:
        mfis_probs[j, 3] = 0
    else:
        lhs_bd_ucb.fit_from_failed_inputs(lhs_failed_inputs_ucb,
                           max_clusters=10, 
                           covariance_type='diag')
    
        XX_lhs_ucb = \
            lhs_bd_ucb.draw_samples(N_HIGH_FIDELITY_INPUTS - n_init - n_select)
        hf_lhs_ucb_outputs = hartmann_model.predict(XX_lhs_ucb)
        multi_IS_lhs_ucb = \
            MultiFidelityIS(limit_state=hartmann_limit_state_function,
                            biasing_distribution=lhs_bd_ucb,
                            input_distribution=input_distribution,
                            bounds=bounds)
        lhs_ucb_mfis_stats = \
            multi_IS_lhs_ucb.get_failure_prob_estimate(XX_lhs_ucb,
                                                       hf_lhs_ucb_outputs)
        mfis_probs[j, 3] = lhs_ucb_mfis_stats[0]


    XX_mc = input_distribution.draw_samples(N_HIGH_FIDELITY_INPUTS)

    mc_outputs = -hartmann_model.predict(XX_mc)
    mc_failures = XX_mc[hartmann_limit_state_function(mc_outputs) > 0,:]
    mfis_probs[j, 4] = len(mc_failures) / N_HIGH_FIDELITY_INPUTS

    print(j)

    mfis_probs_df = pd.DataFrame(mfis_probs)
    mfis_probs_df = mfis_probs_df.rename(columns={0:'ECL', 1:'ECL(UCB)',
                                                  2:'LHS', 3:'LHS(UCB)',
                                                  4:'MC'})
    mfis_probs_df.to_csv(f'data/Hart_mfis_estimates_{today.strftime("%m%d%y")}.csv',
                         index=False)
