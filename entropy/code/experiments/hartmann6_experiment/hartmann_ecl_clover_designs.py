"""
Generates adaptive designs for the Hartmann6 experiment using ECL and CLoVER.

@author: D. Austin Cole  austin.cole8@vt.edu
"""
from datetime import date
import numpy as np
import pandas as pd
from sklearn.gaussian_process import GaussianProcessRegressor as GPR
from sklearn.gaussian_process.kernels import RBF
import time

import os
import sys
fileDir = os.path.dirname(os.path.abspath(__file__))
sourcePath = os.path.join(fileDir, '../../')
sys.path.append(sourcePath)
from eclGP import EntropyContourLocatorGP
from eclGP import HartmannModel
from extra_clover_functions import *

today = date.today()
today_string = today.strftime("%m%d%y")
hartmann_model = HartmannModel()

## Set failure threshold and classify reference set
threshold = 2.63
def hartmann_limit_state_function(y):
    return y-threshold

dim=6
bounds = ((0, 1), (0, 1) , (0, 1), (0, 1), (0, 1), (0, 1))
hartmann_gauss_kernel = 1.0 * RBF(length_scale= np.repeat(.5, dim),
                     length_scale_bounds=(1e-4, 1e4))
n_init = 10*dim
n_cand = 10*dim
n_select = 500 - n_init
MC_REPS = 30
batch_size = 10

initial_designs_df = pd.read_csv("data/Hart_initial_designs_020521.csv",
                                 header=None)
initial_designs = np.array(initial_designs_df)
ecl_designs = np.zeros((n_init+n_select, (dim+1)*MC_REPS))
ecl_times = np.zeros((MC_REPS,))
ecl_batch_designs = np.zeros((n_init+n_select, (dim+1)*MC_REPS))
ecl_batch_times = np.zeros((MC_REPS,))
clover_designs = np.zeros((n_init+n_select, (dim+1)*MC_REPS))
clover_times = np.zeros((MC_REPS,))


for i in range(MC_REPS):
    X0 = initial_designs[:,((dim+1)*i):((dim+1)*(i+1)-1)]
    Y0 = initial_designs[:,(dim+1)*(i+1)-1]
    
    ## Adaptive design with CLoVER
    clover_start_time = time.time() 
    clover_x, clover_y = \
        adaptive_design_with_clover(X0, Y0-threshold, hartmann_model, 
                                    n_select, threshold, bounds)
    clover_designs[0:len(clover_y),((dim+1)*i):((dim+1)*(i+1))] = \
        np.hstack((clover_x, clover_y.reshape((-1,1))+threshold))
    clover_times[i] = (time.time() - clover_start_time)/60
    
    ## Adaptive design with ECL
    ecl_start_time = time.time()
    init_gp = GPR(kernel=hartmann_gauss_kernel, alpha=1e-6)
    init_gp.fit(X0, Y0)
    eclgp = EntropyContourLocatorGP(init_gp, hartmann_limit_state_function)
    eclgp.fit(n_select, hartmann_model, bounds)

    ecl_designs[:,((dim+1)*i):((dim+1)*(i+1))] = \
        np.hstack((eclgp.X_, eclgp.y_.reshape((-1,1))))
    ecl_times[i] = (time.time() - ecl_start_time)/60
    
    ## Adaptive design with ECL.batch
    ecl_batch_start_time = time.time()
    eclgp_batch = EntropyContourLocatorGP(init_gp,
                                          hartmann_limit_state_function)
    
    eclgp_batch.fit(np.int(n_select/batch_size), hartmann_model, bounds, 
                           batch_size=batch_size)

    
    ecl_batch_designs[:,((dim+1)*i):((dim+1)*(i+1))] = \
        np.hstack((eclgp_batch.X_, eclgp_batch.y_.reshape((-1,1))))
    ecl_batch_times[i] = (time.time() - ecl_batch_start_time)/60


    print(i)
    np.savetxt(f'data/Hart_clover_designs_{today_string}.csv',
                clover_designs, delimiter = ",")
    np.savetxt(f'data/Hart_clover_times_{today_string}.csv', 
                clover_times, delimiter = ",")

    np.savetxt(f'data/Hart_ecl_designs_{today_string}.csv', 
                ecl_designs, delimiter = ",")
    np.savetxt(f'data/Hart_ecl_times_{today_string}.csv', 
            ecl_times, delimiter = ",")

    np.savetxt(f'data/Hart_ecl_batch10_designs_{today_string}.csv', 
                ecl_batch_designs, delimiter = ",")
    np.savetxt(f'data/Hart_ecl_batch10_times_{today_string}.csv', 
                ecl_batch_times, delimiter = ",")

