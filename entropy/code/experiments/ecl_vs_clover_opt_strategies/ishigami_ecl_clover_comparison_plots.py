"""
Creates plots comparing various optimization strategies of ECL and CLoVER.
Experiment uses the Ishigami function.

@author: D. Austin Cole  austin.cole8@vt.edu
"""

from datetime import date
import matplotlib.pyplot as plt
from matplotlib.lines import Line2D
import matplotlib.patches as mpatches
import numpy as np
import pandas as pd

today = date.today()
today_string = today.strftime("%m%d%y")

n_init = 30
n_select = 170
MC_REPS = 30


n_markers = np.int(n_select/5) + 1
file_date = '041621'

## Original strategies
orig_ecl_sens = \
    pd.read_csv(f'../ishigami_experiment/data/Ish_ecl_sens_{file_date}.csv',
                        header=None)
orig_ecl_area = \
    pd.read_csv(f'../ishigami_experiment/data/Ish_ecl_area_{file_date}.csv', 
                        header=None)
orig_clover_sens = \
    pd.read_csv(f'../ishigami_experiment/data/Ish_clover_sens_{file_date}.csv',
                          header=None)
orig_clover_area = \
    pd.read_csv(f'../ishigami_experiment/data/Ish_clover_area_{file_date}.csv',
                          header=None)

## Like each other
ecl_like_clover_sens = \
    pd.read_csv(f'data/Ish_ecl_like_clover_sens_{file_date}.csv', header=None)
ecl_like_clover_area = \
    pd.read_csv(f'data/Ish_ecl_like_clover_area_{file_date}.csv', header=None)
clover_like_ecl_sens = \
    pd.read_csv(f'data/Ish_clover_like_ecl_sens_{file_date}.csv', header=None)
clover_like_ecl_area = \
    pd.read_csv(f'data/Ish_clover_like_ecl_area_{file_date}.csv', header=None)

## Size 1000 new candidate sets for each selection
ecl_mult_cand_sens = \
    pd.read_csv(f'data/Ish_ecl_multiple_cand_sens_{file_date}.csv', 
                header=None)
ecl_mult_cand_area = \
    pd.read_csv(f'data/Ish_ecl_multiple_cand_area_{file_date}.csv', 
                header=None)
clover_mult_cand_sens = \
    pd.read_csv(f'data/Ish_clover_multiple_cand_sens_{file_date}.csv',
                header=None)
clover_mult_cand_area = \
    pd.read_csv(f'data/Ish_clover_multiple_cand_area_{file_date}.csv',
                header=None)


##----------------------
## 10000 integration knots
## Normal optimization (single candidate set)
clover_10000_knots_sens = \
    pd.read_csv(f'data/Ish_clover_10000_knots_sens_{file_date}.csv',
                header=None)
clover_10000_knots_area = \
    pd.read_csv(f'data/Ish_clover_10000_knots_area_{file_date}.csv',
                          header=None)
## Multiple candidate sets
clover_mult_cand_10000_knots_sens = \
    pd.read_csv(f'data/Ish_clover_multiple_cand_10000_knots_sens_{file_date}.csv',
                header=None)
clover_mult_cand_10000_knots_area = \
    pd.read_csv(f'data/Ish_clover_multiple_cand_10000_knots_area_{file_date}.csv',
                header=None)
## ECL optimization
clover_like_ecl_10000_knots_sens = \
    pd.read_csv(f'data/Ish_clover_like_ecl_10000_knots_sens_{file_date}.csv',
                header=None)
clover_like_ecl_10000_knots_area= \
    pd.read_csv(f'data/Ish_clover_like_ecl_10000_knots_area_{file_date}.csv',
                header=None)

n_boxes = 9

## Change to relative error of area estimates
true_count = 1008.02
orig_clover_err = np.abs(orig_clover_area - true_count)/true_count
orig_ecl_err = np.abs(orig_ecl_area - true_count)/true_count
clover_like_ecl_err = np.abs(clover_like_ecl_area - true_count)/true_count
ecl_like_clover_err = np.abs(ecl_like_clover_area - true_count)/true_count
clover_mult_cand_err = np.abs(clover_mult_cand_area - true_count)/true_count
ecl_mult_cand_err = np.abs(ecl_mult_cand_area - true_count)/true_count
clover_10000_knots_err = np.abs(clover_10000_knots_area - true_count)/true_count
clover_mult_cand_10000_knots_err = \
    np.abs(clover_mult_cand_10000_knots_area - true_count)/true_count
clover_like_ecl_10000_knots_err = \
    np.abs(clover_like_ecl_10000_knots_area - true_count)/true_count


last_sens = np.array(pd.concat([orig_ecl_sens.iloc[:,n_markers-1],
                          ecl_like_clover_sens.iloc[:,n_markers-1],
                          ecl_mult_cand_sens.iloc[:,n_markers-1],
                          clover_like_ecl_sens.iloc[:,n_markers-1],
                          orig_clover_sens.iloc[:,n_markers-1],
                          clover_mult_cand_sens.iloc[:,n_markers-1],
                          clover_like_ecl_10000_knots_sens.iloc[:,n_markers-1],
                          clover_10000_knots_sens.iloc[:,n_markers-1],
                          clover_mult_cand_10000_knots_sens.iloc[:,n_markers-1]],
                          ignore_index=False))
last_sens = last_sens.reshape((n_boxes, MC_REPS)).T


## Change box outline colors manually
box_line_col = ['k','k','k','g','g','g','crimson','crimson','crimson']
box_fill_col = ['plum','gold','lightskyblue','plum',
                'gold','lightskyblue','plum','gold','lightskyblue']
linestyles = ['-', '-', '-', '--', '--', '--', '-.', '-.','-.']
flierprops=dict(marker='d', markersize=5)
box_positions=[.3, .52, .74, 1.1, 1.32, 1.54, 1.8, 2.02, 2.24]

##---------------------------------------------------------------------------
##--Plotting sensitivity--##
fig = plt.figure(figsize=(5, 4)) 
ax = plt.boxplot(last_sens, positions=box_positions,
                 widths=.2, patch_artist=True, flierprops=flierprops)

## Boxes
for i, facecolor in enumerate(box_line_col):
    lines = ax['boxes']
    n_lines = np.int(len(lines)/n_boxes)
    for j in range(n_boxes):
        plt.setp(lines[j], facecolor = box_fill_col[j], 
                 edgecolor = box_line_col[j], linestyle=linestyles[j])

## Medians, whiskers, outliers
for item in ['whiskers', 'fliers', 'medians', 'caps']:
    lines = ax[item]
    n_lines = np.int(len(lines)/n_boxes)
    for i, facecolor in enumerate(box_line_col):
        for j in range(i*n_lines, i*n_lines+n_lines):
            if item == 'fliers':
                plt.setp(lines[j], markerfacecolor=facecolor, 
                         markeredgecolor=facecolor)
            else:
                plt.setp(lines[j], color=facecolor, 
                linestyle=linestyles[np.int(np.floor(j/n_lines))])
                
                
plt.xlim(0.1,2.5)
plt.ylim(-.05,1.05)
plt.xticks([.55, 1.7], ['ECL', 'CLoVER'])
plt.xlabel('Acquisition function')
plt.ylabel('Sensitivity')
# plt.title('Ishigami sensitivity')
plt.vlines(.93, -.05, 1.05, ls=':')

plt.savefig(f'ish_ecl_clover_sensitivity_boxplot_{today_string}.png',dpi=200)


##---------------------------------------------------------------------------
## Relative error
last_err = np.array(pd.concat([orig_ecl_err.iloc[:,n_markers-1],
                          ecl_like_clover_err.iloc[:,n_markers-1],
                          ecl_mult_cand_err.iloc[:,n_markers-1],
                          clover_like_ecl_err.iloc[:,n_markers-1],
                          orig_clover_err.iloc[:,n_markers-1],
                          clover_mult_cand_err.iloc[:,n_markers-1],
                          clover_like_ecl_10000_knots_err.iloc[:,n_markers-1],
                          clover_10000_knots_err.iloc[:,n_markers-1],
                          clover_mult_cand_10000_knots_err.iloc[:,n_markers-1]],
                          ignore_index=False))
last_err = last_err.reshape((n_boxes, MC_REPS)).T


##--Plotting absolute relative error of volume estimate--##

fig = plt.figure(figsize=(5, 4)) 
ax = plt.boxplot(last_err, positions=box_positions,
                 widths=.2, patch_artist=True, flierprops= flierprops)

## Boxes
for i, facecolor in enumerate(box_line_col):
    lines = ax['boxes']
    n_lines = np.int(len(lines)/n_boxes)
    for j in range(n_boxes):
        plt.setp(lines[j], facecolor = box_fill_col[j], 
                 edgecolor = box_line_col[j], linestyle=linestyles[j])

## Medians, whiskers, outliers
for item in ['whiskers', 'fliers', 'medians', 'caps']:
    lines = ax[item]
    n_lines = np.int(len(lines)/n_boxes)
    for i, facecolor in enumerate(box_line_col):
        for j in range(i*n_lines, i*n_lines+n_lines):
            if item == 'fliers':
                plt.setp(lines[j], markeredgecolor=facecolor,
                         markerfacecolor=facecolor)
            else:
                plt.setp(lines[j], color=facecolor, 
                linestyle=linestyles[np.int(np.floor(j/n_lines))])
                
                
plt.xlim(0.1,2.5)
plt.yscale('log')
plt.ylim(1e-5, 10)
plt.xticks([.55, 1.7], ['ECL', 'CLoVER'])
plt.xlabel('Acquisition function')
plt.ylabel('Absolute relative error')
# plt.title('Ishigami relative error')
plt.vlines(.93, 1e-5, 10, ls=':')

## Add manual legend
ecl_patch = mpatches.Patch(color='plum', label='Algorithm 2')
clover_patch = mpatches.Patch(color='gold', label='Single candidate set')
clover_patch2 = mpatches.Patch(color='lightskyblue', 
                               label='Unique candidate sets')
int103_line = Line2D([0], [0], color='g', ls='--', lw=3,
                     label='1000 integration knots')
int104_line = Line2D([0], [0], color='crimson', ls='-.', lw=3,
                     label='10000 integration knots')

plt.legend(loc='lower right', 
           handles=[ecl_patch, clover_patch, clover_patch2, int103_line, 
                    int104_line],
           handlelength=3)
plt.savefig(f'ish_ecl_clover_err_boxplot_{today_string}.png',dpi=200)
plt.show()

