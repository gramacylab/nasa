"""
Calculates sensitivity and contour volume estimates for ECL and CLoVER 
adaptive designs with various optimization strategies. Experiment uses the 
Ishigami function.

@author: D. Austin Cole  austin.cole8@vt.edu
"""
from datetime import date
import numpy as np
import pandas as pd
from sklearn.gaussian_process import GaussianProcessRegressor as GPR
from sklearn.gaussian_process.kernels import RBF

import os
import sys
fileDir = os.path.dirname(os.path.abspath(__file__))
sourcePath = os.path.join(fileDir, '../../')
sys.path.append(sourcePath)
from eclGP import IshigamiModel
from eclGP import calc_classification_stats

today = date.today()
today_string = today.strftime("%m%d%y")
ishigami_model = IshigamiModel()
bounds = ((-np.pi, np.pi), (-np.pi, np.pi), (-np.pi, np.pi))

threshold =  -10.244 #prob 1e-4
def ishigami_limit_state_function(y):
    return -y+threshold

## Get alpha estimate
'''
fail_count = np.zeros((100,))
for i in range(100):
    xx = 2*np.pi * lhs(3, samples=10000000) - np.pi
    yy = ishigami_model.predict(xx)
    fail_count[i] = np.sum(1*(ishigami_limit_state_function(yy) > 0))
    print(i)
alpha = np.sum(fail_count)/(100*len(fail_count))
'''

## Generate reference set
'''
dense_xx = 2*np.pi * lhs(3, samples=10000000) - np.pi
dense_yy = ishigami_model.predict(dense_xx)
XX = dense_xx[dense_yy < -1,:]
np.savetxt(f'ishigami_data/Ish_XX_10mil.csv', XX, delimiter = ",")
'''

## Upload reference set and find failures
XX_df = pd.read_csv(f"../ishigami_experiment/data/Ish_XX_10mil.csv",
                    header=None)
XX = np.array(XX_df)
yy = ishigami_model.predict(XX)
true_failures = XX[ishigami_limit_state_function(yy) > 0,:]
true_classifications = 1*(ishigami_limit_state_function(yy) > 0)


dim = 3
ish_gauss_kernel = 1.0 * RBF(length_scale= np.repeat(.5, dim),
                     length_scale_bounds=(1e-4, 1e4))
n_init = 10*dim
n_cand = 10*dim

n_select = 170
MC_REPS = 30
dim = 3
n_init = 10*dim
file_date = "041621"

clover_designs_like_ecl_df = \
    pd.read_csv(f"data/Ish_clover_designs_{file_date}_like_ecl.csv",
                header=None)
clover_designs_like_ecl = np.array(clover_designs_like_ecl_df)
clover_designs_mult_cand_df = \
    pd.read_csv(f"data/Ish_clover_designs_{file_date}_multiple_cand.csv",
                header=None)
clover_designs_mult_cand = np.array(clover_designs_mult_cand_df)
clover_designs_like_ecl_10000_knots_df = \
    pd.read_csv(f"data/Ish_clover_designs_{file_date}_like_ecl_10000_knots.csv",
                header=None)
clover_designs_like_ecl_10000_knots = \
    np.array(clover_designs_like_ecl_10000_knots_df)
clover_designs_mult_cand_10000_knots_df = \
    pd.read_csv(f"data/Ish_clover_designs_{file_date}_multiple_cand_10000_knots.csv",
                header=None)
clover_designs_mult_cand_10000_knots = \
    np.array(clover_designs_mult_cand_10000_knots_df)
clover_designs_10000_knots_df = \
    pd.read_csv(f"data/Ish_clover_designs_{file_date}_10000_knots.csv",
                header=None)
clover_designs_10000_knots = np.array(clover_designs_10000_knots_df)

ecl_designs_like_clover_df = \
    pd.read_csv(f"data/Ish_ecl_designs_{file_date}_like_clover.csv",
                header=None)
ecl_designs_like_clover = np.array(ecl_designs_like_clover_df)
ecl_designs_mult_cand_df = \
    pd.read_csv(f"data/Ish_ecl_designs_{file_date}_multiple_cand.csv",
                header=None)
ecl_designs_mult_cand = np.array(ecl_designs_mult_cand_df)


n_gps = np.int(n_select/5)

ecl_like_clover_area = np.zeros((MC_REPS, n_gps+1))
ecl_mult_cand_area = np.zeros((MC_REPS, n_gps+1))
clover_like_ecl_area = np.zeros((MC_REPS, n_gps+1))
clover_mult_cand_area = np.zeros((MC_REPS, n_gps+1))
clover_10000_knots_area = np.zeros((MC_REPS, n_gps+1))
clover_like_ecl_10000_knots_area = np.zeros((MC_REPS, n_gps+1))
clover_mult_cand_10000_knots_area = np.zeros((MC_REPS, n_gps+1))


ecl_like_clover_sens = np.zeros((MC_REPS, n_gps+1))
ecl_mult_cand_sens = np.zeros((MC_REPS, n_gps+1))

clover_like_ecl_sens = np.zeros((MC_REPS, n_gps+1))
clover_mult_cand_sens = np.zeros((MC_REPS, n_gps+1))
clover_10000_knots_sens = np.zeros((MC_REPS, n_gps+1))
clover_like_ecl_10000_knots_sens = np.zeros((MC_REPS, n_gps+1))
clover_mult_cand_10000_knots_sens = np.zeros((MC_REPS, n_gps+1))

designs_list = [ecl_designs_like_clover, ecl_designs_mult_cand,
                clover_designs_like_ecl, clover_designs_mult_cand,
                clover_designs_like_ecl_10000_knots,
                clover_designs_mult_cand_10000_knots,
                clover_designs_10000_knots]

area_list = [ecl_like_clover_area, ecl_mult_cand_area,
             clover_like_ecl_area, clover_mult_cand_area,
             clover_like_ecl_10000_knots_area,
             clover_mult_cand_10000_knots_area,
             clover_10000_knots_area]

sens_list = [ecl_like_clover_sens, ecl_mult_cand_sens,
             clover_like_ecl_sens, clover_mult_cand_sens,
             clover_like_ecl_10000_knots_sens,
             clover_mult_cand_10000_knots_sens,
             clover_10000_knots_sens]

for j in range(MC_REPS):
    for k in range(0, n_select+1, 5):
        k_ind = np.int(k/5)

        for kk in range(len(designs_list)):
            design_X = designs_list[kk][0:(n_init+k), 
                                        ((dim+1)*j):((dim+1)*(j+1)-1)]
            design_Y = designs_list[kk][0:(n_init+k), (dim+1)*(j+1)-1]
           
            fit_gp =  GPR(kernel=ish_gauss_kernel, alpha=1e-6)
            fit_gp.fit(design_X, design_Y)
            gp_preds = fit_gp.predict(XX)
            new_failures = XX[ishigami_limit_state_function(gp_preds) > 0,:]
            gp_classifications = 1*(ishigami_limit_state_function(gp_preds) > 0)
            
            
            sens_list[kk][j,k_ind] = calc_classification_stats(
                    true_classifications, gp_classifications)[0]
            area_list[kk][j,k_ind] = np.sum(gp_classifications)
    print(j)  
    
    np.savetxt(f'data/Ish_clover_like_ecl_area_{today_string}.csv',
                clover_like_ecl_area, delimiter = ",")
    np.savetxt(f'data/Ish_clover_like_ecl_sens_{today_string}.csv', 
                clover_like_ecl_sens, delimiter = ",")
    np.savetxt(f'data/Ish_clover_multiple_cand_area_{today_string}.csv',
                clover_mult_cand_area, delimiter = ",")
    np.savetxt(f'data/Ish_clover_multiple_cand_sens_{today_string}.csv', 
                clover_mult_cand_sens, delimiter = ",")
    np.savetxt(f'data/Ish_clover_10000_knots_area_{today_string}.csv',
                clover_10000_knots_area, delimiter = ",")
    np.savetxt(f'data/Ish_clover_10000_knots_sens_{today_string}.csv', 
                clover_10000_knots_sens, delimiter = ",")
    np.savetxt(f'data/Ish_clover_like_ecl_10000_knots_area_{today_string}.csv',
                clover_like_ecl_10000_knots_area, delimiter = ",")
    np.savetxt(f'data/Ish_clover_like_ecl_10000_knots_sens_{today_string}.csv', 
                clover_like_ecl_10000_knots_sens, delimiter = ",")
    np.savetxt(f'data/Ish_clover_multiple_cand_10000_knots_area_{today_string}.csv',
                clover_mult_cand_10000_knots_area, delimiter = ",")
    np.savetxt(f'data/Ish_clover_multiple_cand_10000_knots_sens_{today_string}.csv', 
                clover_mult_cand_10000_knots_sens, delimiter = ",")
    
    np.savetxt(f'data/Ish_ecl_like_clover_sens_{today_string}.csv', 
                ecl_like_clover_sens, delimiter = ",")
    np.savetxt(f'data/Ish_ecl_like_clover_area_{today_string}.csv', 
                ecl_like_clover_area, delimiter = ",")
    np.savetxt(f'data/Ish_ecl_multiple_cand_sens_{today_string}.csv', 
                ecl_mult_cand_sens, delimiter = ",")
    np.savetxt(f'data/Ish_ecl_multiple_cand_area_{today_string}.csv', 
                ecl_mult_cand_area, delimiter = ",")
  
