"""
Generates ECL and CLoVER adaptive designs using various optimization strategies.
Experiment uses the Ishigami function.

@author: D. Austin Cole  austin.cole8@vt.edu
"""
from datetime import date
import numpy as np
import pandas as pd
from pyDOE import lhs
from sklearn.gaussian_process import GaussianProcessRegressor as GPR
from sklearn.gaussian_process.kernels import RBF
import time

import os
import sys
fileDir = os.path.dirname(os.path.abspath(__file__))
sourcePath = os.path.join(fileDir, '../../')
sys.path.append(sourcePath)
from eclGP import EntropyContourLocatorGP
from eclGP import IshigamiModel
from extra_clover_functions import *


today = date.today()
today_string = today.strftime("%m%d%y")
ishigami_model = IshigamiModel(a_coef=5, b_coef=0.1)
bounds = ((-np.pi, np.pi), (-np.pi, np.pi), (-np.pi, np.pi))


threshold =  -10.244 #prob 1e-4
def ishigami_limit_state_function(y):
    return -y+threshold

XX_df = pd.read_csv(f"../ishigami_experiment/data/Ish_XX_10mil.csv",
                    header=None)
XX = np.array(XX_df)
yy = ishigami_model.predict(XX)
true_failures = XX[ishigami_limit_state_function(yy) > 0,:]
true_classifications = 1*(ishigami_limit_state_function(yy) > 0)


dim = 3
ish_gauss_kernel = 1.0 * RBF(length_scale= np.repeat(.5, dim),
                     length_scale_bounds=(1e-4, 1e4))
n_init = 10*dim
n_cand = 10*dim
n_select = 170
MC_REPS = 30


initial_designs_df = \
    pd.read_csv("../ishigami_experiment/data/Ish_initial_designs.csv",
                header=None)
initial_designs = np.array(initial_designs_df)
ecl_designs_like_clover = np.zeros((n_init+n_select, (dim+1)*MC_REPS))
ecl_times_like_clover = np.zeros((MC_REPS,))
ecl_designs_mult_cand = np.zeros((n_init+n_select, (dim+1)*MC_REPS))
ecl_times_mult_cand = np.zeros((MC_REPS,))

clover_designs_like_ecl = np.zeros((n_init+n_select, (dim+1)*MC_REPS))
clover_times_like_ecl = np.zeros((MC_REPS,))
clover_designs_mult_cand = np.zeros((n_init+n_select, (dim+1)*MC_REPS))
clover_times_mult_cand = np.zeros((MC_REPS,))
clover_designs_10000_knots = np.zeros((n_init+n_select, (dim+1)*MC_REPS))
clover_times_10000_knots = np.zeros((MC_REPS,))
clover_designs_like_ecl_10000_knots = np.zeros((n_init+n_select,
                                                (dim+1)*MC_REPS))
clover_times_like_ecl_10000_knots = np.zeros((MC_REPS,))
clover_designs_mult_cand_10000_knots = np.zeros((n_init+n_select,
                                                 (dim+1)*MC_REPS))
clover_times_mult_cand_10000_knots = np.zeros((MC_REPS,))

for i in range(MC_REPS):
    X0 = initial_designs[:,((dim+1)*i):((dim+1)*(i+1)-1)]
    Y0 = initial_designs[:,(dim+1)*(i+1)-1]
  
    ## Adaptive design with CLoVER
    # New small candidate set for each step, then gradient optimization
    clover_start_time1 = time.time() 
    clover_x1, clover_y1 = \
        adaptive_design_with_clover(X0, Y0-threshold, ishigami_model, 
                                    n_select, threshold, bounds, ecl_opt=True)
    clover_designs_like_ecl[0:len(clover_y1),((dim+1)*i):((dim+1)*(i+1))] = \
        np.hstack((clover_x1, clover_y1.reshape((-1,1))+threshold))
    clover_times_like_ecl[i] = (time.time() - clover_start_time1)/60
    
    # New candidate sets for each step, no gradient optimization
    clover_start_time2 = time.time() 
    clover_x2, clover_y2 = \
        adaptive_design_with_clover(X0, Y0-threshold, ishigami_model, 
                                    n_select, threshold, bounds, new_cand=True)
    clover_designs_mult_cand[0:len(clover_y2),((dim+1)*i):((dim+1)*(i+1))] = \
        np.hstack((clover_x2, clover_y2.reshape((-1,1))+threshold))
    clover_times_mult_cand[i] = (time.time() - clover_start_time2)/60
     
    ## Increase to 10000 integration knots
    # Normal: single candidate set
    clover_start_time3 = time.time() 
    clover_x3, clover_y3 = \
        adaptive_design_with_clover(X0, Y0-threshold, ishigami_model, 
                                    n_select, threshold, bounds, n_int=10000)
    clover_designs_10000_knots[0:len(clover_y3),((dim+1)*i):((dim+1)*(i+1))] = \
        np.hstack((clover_x3, clover_y3.reshape((-1,1))+threshold))
    clover_times_10000_knots[i] = (time.time() - clover_start_time3)/60
    
    # New small candidate set for each step, then gradient optimization
    clover_start_time4 = time.time() 
    clover_x4, clover_y4 = \
        adaptive_design_with_clover(X0, Y0-threshold, ishigami_model, 
                                    n_select, threshold, bounds, 
                                    n_int=10000, ecl_opt=True)
    clover_designs_like_ecl_10000_knots[0:len(clover_y4),
                                        ((dim+1)*i):((dim+1)*(i+1))] = \
        np.hstack((clover_x4, clover_y4.reshape((-1,1))+threshold))
    clover_times_like_ecl_10000_knots[i] = (time.time()-clover_start_time4)/60
    
    # New candidate sets for each step, no gradient optimization
    clover_start_time5 = time.time() 
    clover_x5, clover_y5 = \
        adaptive_design_with_clover(X0, Y0-threshold, ishigami_model, 
                                    n_select, threshold, bounds, new_cand=True)
    clover_designs_mult_cand_10000_knots[0:len(clover_y5),
                                         ((dim+1)*i):((dim+1)*(i+1))] = \
        np.hstack((clover_x5, clover_y5.reshape((-1,1))+threshold))
    clover_times_mult_cand_10000_knots[i] = (time.time()-clover_start_time5)/60
    
    ## Adaptive design with ECL
    ecl_start_time1 = time.time()
    init_gp = GPR(kernel=ish_gauss_kernel, alpha=1e-6)
    init_gp.fit(X0, Y0)
    eclgp1 = EntropyContourLocatorGP(init_gp, ishigami_limit_state_function)
    
    ## Use a single candidate set for all sample selections
    Xcand = np.copy(2*np.pi*lhs(3, 1000) - np.pi)
    eclgp1.fit(n_select, ishigami_model, bounds, X_cand=Xcand, fixed_cands=True)
    
    ecl_designs_like_clover[:,((dim+1)*i):((dim+1)*(i+1))] = \
        np.hstack((eclgp1.X_, eclgp1.y_.reshape((-1,1))))
    ecl_times_like_clover[i] = (time.time() - ecl_start_time1)/60
    
    ## New candidate sets for each step, no gradient optimization
    ecl_start_time2 = time.time()
    init_gp = GPR(kernel=ish_gauss_kernel, alpha=1e-6)
    init_gp.fit(X0, Y0)
    eclgp2 = EntropyContourLocatorGP(init_gp, ishigami_limit_state_function)
    
    eclgp2.fit(n_select, ishigami_model, bounds, n_cand=1000, local_opt=False)

    ecl_designs_mult_cand[:,((dim+1)*i):((dim+1)*(i+1))] = \
        np.hstack((eclgp2.X_, eclgp2.y_.reshape((-1,1))))
    ecl_times_mult_cand[i] = (time.time() - ecl_start_time2)/60
    
    
    print(i)
    np.savetxt(f'data/Ish_clover_designs_{today_string}_like_ecl.csv',
                clover_designs_like_ecl, delimiter = ",")
    np.savetxt(f'data/Ish_clover_times_{today_string}_like_ecl.csv', 
                clover_times_like_ecl, delimiter = ",")
    np.savetxt(f'data/Ish_clover_designs_{today_string}_multiple_cand.csv',
                clover_designs_mult_cand, delimiter = ",")
    np.savetxt(f'data/Ish_clover_times_{today_string}_multiple_cand.csv', 
                clover_times_mult_cand, delimiter = ",")
    
    np.savetxt(f'data/Ish_clover_designs_{today_string}_10000_knots.csv',
                clover_designs_10000_knots, delimiter = ",")
    np.savetxt(f'data/Ish_clover_times_{today_string}_10000_knots.csv', 
                clover_times_10000_knots, delimiter = ",")
    np.savetxt(f'data/Ish_clover_designs_{today_string}_like_ecl_10000_knots.csv',
                clover_designs_like_ecl_10000_knots, delimiter = ",")
    np.savetxt(f'data/Ish_clover_times_{today_string}_like_ecl_10000_knots.csv', 
                clover_times_like_ecl_10000_knots, delimiter = ",")
    np.savetxt(f'data/Ish_clover_designs_{today_string}_multiple_cand_10000_knots.csv',
                clover_designs_mult_cand_10000_knots, delimiter = ",")
    np.savetxt(f'data/Ish_clover_times_{today_string}_multiple_cand_10000_knots.csv', 
                clover_times_mult_cand_10000_knots, delimiter = ",")
    
    np.savetxt(f'data/Ish_ecl_times_{today_string}_like_clover.csv', 
                ecl_times_like_clover, delimiter = ",")
    np.savetxt(f'data/Ish_ecl_designs_{today_string}_like_clover.csv', 
                ecl_designs_like_clover, delimiter = ",")
    np.savetxt(f'data/Ish_ecl_times_{today_string}_multiple_cand.csv', 
                ecl_times_mult_cand, delimiter = ",")
    np.savetxt(f'data/Ish_ecl_designs_{today_string}_multiple_cand.csv', 
                ecl_designs_mult_cand, delimiter = ",")


