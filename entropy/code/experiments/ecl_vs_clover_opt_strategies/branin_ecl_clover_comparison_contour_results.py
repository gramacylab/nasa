"""
Calculates sensitivity and contour volume estimates for ECL and CLoVER 
adaptive designs with various optimization strategies. Experiment uses the 
Branin-Hoo function.

@author: D. Austin Cole  austin.cole8@vt.edu
"""
from datetime import date
import numpy as np
import pandas as pd
from sklearn.gaussian_process import GaussianProcessRegressor as GPR
from sklearn.gaussian_process.kernels import RBF

import os
import sys
fileDir = os.path.dirname(os.path.abspath(__file__))
sourcePath = os.path.join(fileDir, '../../')
sys.path.append(sourcePath)
from eclGP import BraninHooModel
from eclGP import calc_classification_stats


today = date.today()
today_string = today.strftime("%m%d%y")
branin_model = BraninHooModel()


threshold = 206
def branin_limit_state_function(y):
    return y-threshold


XX_df = \
    pd.read_csv(f"../branin_hoo_experiment/data/BH_XX_5mil.csv", header=None)
XX = np.array(XX_df)
yy = branin_model.predict(XX)
true_failures = XX[branin_limit_state_function(yy) > 0,:]
true_classifications = 1*(branin_limit_state_function(yy) > 0)


dim = 2
branin_gauss_kernel = 1.0 * RBF(length_scale= np.repeat(.5, dim),
                      length_scale_bounds=(1e-4, 1e4))
bounds = ((-5, 10), (0, 15))
n_init = 10
n_select = 20
MC_REPS = 30

file_date = '041621'

clover_designs_like_ecl_df = \
    pd.read_csv(f"data/BH_clover_designs_{file_date}_like_ecl.csv", header=None)
clover_like_ecl_designs = np.array(clover_designs_like_ecl_df)
clover_designs_100_knots_df = \
    pd.read_csv(f"data/BH_clover_designs_{file_date}_100_knots.csv", header=None)
clover_100_knots_designs = np.array(clover_designs_100_knots_df)

ecl_designs_like_clover_df = \
    pd.read_csv(f"data/BH_ecl_designs_{file_date}_like_clover.csv", header=None)
ecl_like_clover_designs = np.array(ecl_designs_like_clover_df)


ecl_like_clover_area = np.zeros((MC_REPS, n_select+1))
clover_like_ecl_area = np.zeros((MC_REPS, n_select+1))
clover_100_knots_area = np.zeros((MC_REPS, n_select+1))

ecl_like_clover_sens = np.zeros((MC_REPS, n_select+1))
clover_like_ecl_sens = np.zeros((MC_REPS, n_select+1))
clover_100_knots_sens = np.zeros((MC_REPS, n_select+1))


designs_list = [ecl_like_clover_designs, clover_like_ecl_designs,
                clover_100_knots_designs]
area_list = [ecl_like_clover_area, clover_like_ecl_area, 
                clover_100_knots_area]
sens_list = [ecl_like_clover_sens, clover_like_ecl_sens, 
                clover_100_knots_sens]


for j in range(MC_REPS):
    for kk in range(len(designs_list)):
        n_select_kk = sens_list[kk].shape[1]
        n_step = np.int(n_select/(n_select_kk-1))
        for k in range(n_select_kk):
            design_X = designs_list[kk][0:(n_init+n_step*k), 
                                        ((dim+1)*j):((dim+1)*(j+1)-1)]
            design_Y = designs_list[kk][0:(n_init+n_step*k), 
                                        (dim+1)*(j+1)-1]
            
            fit_gp =  GPR(kernel=branin_gauss_kernel, alpha=1e-6)
            fit_gp.fit(design_X, design_Y)
            gp_preds = fit_gp.predict(XX)
            gp_classifications = 1*(branin_limit_state_function(gp_preds) > 0)
    
            sens_list[kk][j,k] = calc_classification_stats(
                    true_classifications, gp_classifications)[0]
            area_list[kk][j,k] = np.sum(gp_classifications)
    print(j)   
    
    np.savetxt(f'data/BH_clover_like_ecl_sens_{today_string}.csv', 
                clover_like_ecl_sens, delimiter = ",")
    np.savetxt(f'data/BH_clover_like_ecl_area_{today_string}.csv',
                clover_like_ecl_area, delimiter = ",")
    np.savetxt(f'data/BH_clover_100_knots_sens_{today_string}.csv', 
                clover_100_knots_sens, delimiter = ",")
    np.savetxt(f'data/BH_clover_100_knots_area_{today_string}.csv',
                clover_100_knots_area, delimiter = ",")
    
    np.savetxt(f'data/BH_ecl_like_clover_sens_{today_string}.csv', 
                ecl_like_clover_sens, delimiter = ",")
    np.savetxt(f'data/BH_ecl_like_clover_area_{today_string}.csv', 
                ecl_like_clover_area, delimiter = ",")
