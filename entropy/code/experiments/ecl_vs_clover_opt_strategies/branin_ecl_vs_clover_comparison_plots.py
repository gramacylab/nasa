"""
Creates plots comparing various optimization strategies of ECL and CLoVER.
Experiment uses the Branin-Hoo function.

@author: D. Austin Cole  austin.cole8@vt.edu
"""

from datetime import date
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns


today = date.today()
today_string = today.strftime("%m%d%y")


n_init = 10
n_select = 20
MC_REPS = 30

n_markers = n_select + 1
filedate = '041621'

## Original strategies
orig_ecl_sens = \
    pd.read_csv(f'../branin_hoo_experiment/data/BH_ecl_sens_{filedate}.csv',
                header=None)
orig_ecl_area = \
    pd.read_csv(f'../branin_hoo_experiment/data/BH_ecl_area_{filedate}.csv', 
                header=None)
orig_clover_sens = \
    pd.read_csv(f'../branin_hoo_experiment/data/BH_clover_sens_{filedate}.csv',
                header=None)
orig_clover_area = \
    pd.read_csv(f'../branin_hoo_experiment/data/BH_clover_area_{filedate}.csv',
                header=None)

## Like each other
ecl_like_clover_sens = \
    pd.read_csv(f'data/BH_ecl_like_clover_sens_{filedate}.csv', header=None)
ecl_like_clover_area = \
    pd.read_csv(f'data/BH_ecl_like_clover_area_{filedate}.csv', header=None)
clover_like_ecl_sens = \
    pd.read_csv(f'data/BH_clover_like_ecl_sens_{filedate}.csv', header=None)
clover_like_ecl_area = \
    pd.read_csv(f'data/BH_clover_like_ecl_area_{filedate}.csv', header=None)

## 100 integration knots
clover_100knots_sens = \
    pd.read_csv(f'data/BH_clover_100_knots_sens_{filedate}.csv', header=None)
clover_100knots_area = \
    pd.read_csv(f'data/BH_clover_100_knots_area_{filedate}.csv', header=None)


## Change to relative error of area estimates 
true_count = 48406.86
orig_clover_err = np.abs(orig_clover_area - true_count)/true_count
orig_ecl_err = np.abs(orig_ecl_area - true_count)/true_count
clover_like_ecl_err = np.abs(clover_like_ecl_area - true_count)/true_count
ecl_like_clover_err = np.abs(ecl_like_clover_area - true_count)/true_count
clover_100knots_err = np.abs(clover_100knots_area - true_count)/true_count

methods = ['ECL','CLoVER','ECL','CLoVER','CLoVER']
color_palette = ['royalblue','g']
color_palette2 = ['purple','orange','orange']
markers = ['^','s','D']


## Sensitivity lineplot
fig = plt.figure(figsize=(5, 3.5)) 
combined_sens_mc = pd.concat([orig_ecl_sens.melt(), 
                              clover_like_ecl_sens.melt(),
                              ecl_like_clover_sens.melt(), 
                              orig_clover_sens.melt(),
                              clover_100knots_sens.melt()])
combined_sens_mc = combined_sens_mc.rename(columns={"value": "Sensitivity"})
combined_sens_mc = \
    combined_sens_mc.rename(columns={"variable": "Function Evaluations"})
combined_sens_mc['Function Evaluations'] = \
    (combined_sens_mc['Function Evaluations']) + n_init
descript = [item for item in methods for i in range(MC_REPS*(n_markers))]
combined_sens_mc['Acquisition Function'] = descript 
combined_sens_mc['Optimization'] = \
    2*MC_REPS*(n_markers)*['Algorithm 2'] +\
    2*MC_REPS*(n_markers)*['Single candidate set']  +\
        MC_REPS*(n_markers)*['100 integration knots']
        
lines = sns.lineplot(x="Function Evaluations",
                    y="Sensitivity", hue="Acquisition Function", 
                    style='Optimization',
                    data=combined_sens_mc, 
                    palette=color_palette,
                    markers=markers, ci=None)
lines.set_xticks(range(n_init,n_init+n_select+1, 2))
lines.legend().set_title('')
handles, labels = lines.get_legend_handles_labels()
new_handles=[handles[1], handles[2], handles[4], handles[5]]
new_labels=[labels[1], labels[2], labels[4], labels[5]]                                                  
# legend = lines.legend(labels=new_labels, handles=new_handles)
lines.set_title(f"Branin-Hoo sensitivity estimates over {MC_REPS} samples")
plt.savefig(f'branin_ecl_clover_sensitivity_{today_string}.png',
            dpi=200)


### Relative Error (of volume estimate) lineplot
fig = plt.figure(figsize=(5, 3.5)) 
combined_err_mc = pd.concat([orig_ecl_err.melt(), 
                              clover_like_ecl_err.melt(),
                              ecl_like_clover_err.melt(), 
                              orig_clover_err.melt(),
                              clover_100knots_err.melt()])
combined_err_mc = \
    combined_err_mc.rename(columns={"value": "Absolute relative error"})
combined_err_mc = \
    combined_err_mc.rename(columns={"variable": "Function Evaluations"})
combined_err_mc['Function Evaluations'] = \
    (combined_err_mc['Function Evaluations']) + n_init
descript = [item for item in methods for i in range(MC_REPS*(n_markers))]
combined_err_mc['Acquisition function'] = descript 
combined_err_mc['Optimization'] =   2*MC_REPS*(n_markers)*['Algorithm 2'] +\
    2*MC_REPS*(n_markers)*['One candidate set/10^3 integration knots']  +\
        MC_REPS*(n_markers)*['One candidate set/100 integration knots']
        
lines = sns.lineplot(x="Function Evaluations",
                    y="Absolute relative error", 
                    hue="Acquisition function", 
                    style='Optimization',
                    data=combined_err_mc, 
                    palette=color_palette,
                    markers=markers, ci=None)
lines.set_xticks(range(n_init,n_init+n_select+1, 2))
lines.set_title(f"Branin-Hoo error estimates over {MC_REPS} samples")
lines.set(yscale='log', ylim=(1e-3, 2.5))
lines.get_legend().remove()
plt.savefig(f'branin_ecl_clover_re_{today_string}.png',dpi=200)
plt.show()
