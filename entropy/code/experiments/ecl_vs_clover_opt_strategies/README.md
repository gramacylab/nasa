## Optimization strategy investigation performed in the appendix of the paper, "Entropy-based adaptive design for contour finding and estimating reliability"
### D. Austin Cole, Robert B. Gramacy, James E. Warner, Geoffrey F. Bomarito, Patrick E. Leser, William P. Leser

## Includes:	
- **branin_ecl_clover_comparison_contour_results.py**: script to take all ECL and CLoVER adaptive designs for the Branin-Hoo function to fit GPs and use a reference set to calculate sensitivity and area of the predicted failure region.
- **branin_ecl_clover_comparison_designs.py**: script to perform ECL and CLoVER adaptive designs with different optimization strategies on the Branin-Hoo function. Uses eclGP, the extra_clover_functions module, and the CLoVER package from Marques et al. (2018).
- **branin_ecl_vs_clover_comparison_plots.py**: script to generate sensitivity and contour volume error plots for the Branin-Hoo optimization investigation.
- **ishigami_ecl_clover_comparison_contour_results.py**: script to take all ECL and CLoVER adaptive designs for the Branin-Hoo function to fit GPs and use a reference set to calculate sensitivity and area of the predicted failure region.
- **ishigami_ecl_clover_comparison_designs.py**: script to perform ECL and CLoVER adaptive designs with different optimization strategies on the Branin-Hoo function. Uses eclGP, the extra_clover_functions module, and the CLoVER package from Marques et al. (2018).
- **ishigami_ecl_vs_clover_comparison_plots.py**: script to generate sensitivity and contour volume error plots for the Ishigami optimization investigation.