"""
Generates ECL and CLoVER adaptive designs using various optimization strategies.
Experiment uses the Branin-Hoo function.

@author: D. Austin Cole  austin.cole8@vt.edu
"""

from datetime import date
import numpy as np
from pyDOE import lhs
from sklearn.gaussian_process import GaussianProcessRegressor as GPR
from sklearn.gaussian_process.kernels import RBF
import time

import os
import sys
fileDir = os.path.dirname(os.path.abspath(__file__))
sourcePath = os.path.join(fileDir, '../../')
sys.path.append(sourcePath)
from eclGP import EntropyContourLocatorGP
from eclGP import BraninHooModel
from extra_clover_functions import *

today = date.today()
today_string = today.strftime("%m%d%y")
branin_model = BraninHooModel()

threshold = 206
def branin_limit_state_function(y):
    return y-threshold

initial_designs = \
    np.genfromtxt("../branin_hoo_experiment/data/BH_initial_designs.csv", 
                  delimiter=',')

dim = 2
branin_gauss_kernel = 1.0 * RBF(length_scale= np.repeat(.5, dim),
                     length_scale_bounds=(1e-4, 1e4))
bounds = ((-5, 10), (0, 15))
n_init = initial_designs.shape[0]
n_cand = 10*dim
n_select = 20
MC_REPS = 30

ecl_designs_single_cand = np.zeros((n_init+n_select, (dim+1)*MC_REPS))
ecl_times_single_cand  = np.zeros((MC_REPS,))

clover_designs_small_cand = np.zeros((n_init+n_select, (dim+1)*MC_REPS))
clover_times_small_cand = np.zeros((MC_REPS,))
clover_designs_small_nInt = np.zeros((n_init+n_select, (dim+1)*MC_REPS))
clover_times_small_nInt = np.zeros((MC_REPS,))

for i in range(MC_REPS):
    X0 = initial_designs[:,((dim+1)*i):((dim+1)*(i+1)-1)]
    Y0 = initial_designs[:,(dim+1)*(i+1)-1]
    
    ## Adaptive design with CLoVER
    # New small candidate set for each step, then gradient optimization
    clover_start_time1 = time.time() 
    clover_x1, clover_y1 = \
        adaptive_design_with_clover(X0, Y0-threshold, branin_model, 
                                    n_select, threshold, bounds, ecl_opt=True)
    clover_designs_small_cand[0:len(clover_y1),((dim+1)*i):((dim+1)*(i+1))] = \
        np.hstack((clover_x1, clover_y1.reshape((-1,1))+threshold))
    clover_times_small_cand[i] = (time.time() - clover_start_time1)/60
    

    # Regular candidate set, only 100 integration knots
    clover_start_time3 = time.time() 
    clover_x3, clover_y3 = \
        adaptive_design_with_clover(X0, Y0-threshold, branin_model, 
                                    n_select, threshold, bounds, n_int=100)
    clover_designs_small_nInt[0:len(clover_y1),((dim+1)*i):((dim+1)*(i+1))] = \
        np.hstack((clover_x1, clover_y1.reshape((-1,1))+threshold))
    clover_times_small_nInt[i] = (time.time() - clover_start_time3)/60
    
    ## Adaptive design with ECL
    ecl_start_time1 = time.time()
    init_gp = GPR(kernel=branin_gauss_kernel, alpha=1e-6)
    init_gp.fit(X0, Y0)
    eclgp1 = EntropyContourLocatorGP(init_gp, branin_limit_state_function)
    
    
    ## Use a single candidate set for all sample selections
    Xcand = 15*lhs(2, 1000)
    Xcand[:,0] = Xcand[:,0] - 5
    eclgp1.fit(n_select, branin_model, bounds, X_cand=Xcand, fixed_cands=True)
    
    ecl_designs_single_cand[:,((dim+1)*i):((dim+1)*(i+1))] = \
        np.hstack((eclgp1.X_, eclgp1.y_.reshape((-1,1))))
    ecl_times_single_cand[i] = (time.time() - ecl_start_time1)/60

    print(i)
    
    np.savetxt(f'data/BH_ecl_designs_{today_string}_like_clover.csv', 
                ecl_designs_single_cand, delimiter = ",")
    np.savetxt(f'data/BH_ecl_times_{today_string}_like_clover.csv', 
                ecl_times_single_cand, delimiter = ",")
    
    np.savetxt(f'data/BH_clover_designs_{today_string}_like_ecl.csv', 
                clover_designs_small_cand, delimiter = ",")
    np.savetxt(f'data/BH_clover_times_{today_string}_like_ecl.csv', 
                clover_times_small_cand, delimiter = ",")
    np.savetxt(f'data/BH_clover_designs_{today_string}_100_knots.csv', 
                clover_designs_small_nInt, delimiter = ",")
    np.savetxt(f'data/BH_clover_times_{today_string}_100_knots.csv', 
                clover_times_small_nInt, delimiter = ",")
