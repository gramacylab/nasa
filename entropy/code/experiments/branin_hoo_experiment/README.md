## Branin  Hoo Experiment performed in the paper, "Entropy-based adaptive design for contour finding and estimating reliability"
### D. Austin Cole, Robert B. Gramacy, James E. Warner, Geoffrey F. Bomarito, Patrick E. Leser, William P. Leser

## Includes:	
- **branin_contour_plots.py**: script to generate sensitivity and contour volume error plots shown in Section 4.2.
- **branin_contour_results.py**: script to take all adaptive designs to fit GPs and use a reference set to calculate sensitivity and area of the predicted failure region.
- **branin_ecl_clover_designs**: script to perform ECL and CLoVER adaptive designs. Uses the CLoVER package from Marques et al. (2018).
- **branin_generate_initial_designs.py**: script to generate Latin hypercube sample designs for initial GP fits.
- **get_branin_kriginv_adaptive_designs.R**: script to perform adaptive designs (EGRA, Ranjan, SUR, tIMSE, tMSE) using the KrigInv package