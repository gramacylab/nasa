"""
Generates adaptive designs for the Branin-Hoo experiment using ECL and CLoVER.

@author: D. Austin Cole  austin.cole8@vt.edu
"""
from datetime import date
import numpy as np
from sklearn.gaussian_process import GaussianProcessRegressor as GPR
from sklearn.gaussian_process.kernels import RBF
import time

import os
import sys
fileDir = os.path.dirname(os.path.abspath(__file__))
sourcePath = os.path.join(fileDir, '../../')
sys.path.append(sourcePath)
from extra_clover_functions import *
from eclGP import EntropyContourLocatorGP
from eclGP import BraninHooModel

today = date.today()
today_string = today.strftime("%m%d%y")
branin_model = BraninHooModel()

dense_x1_seq = np.linspace(-5, 10, 1000)
dense_x2_seq = np.linspace(0, 15, 1000)
dense_x1, dense_x2 = np.meshgrid(dense_x1_seq, dense_x2_seq)
dense_xx = np.hstack((dense_x1.reshape((-1, 1)), dense_x2.reshape((-1, 1))))
dense_y = branin_model.predict(dense_xx)

threshold = 206
def branin_limit_state_function(y):
    return y-threshold

true_failures = dense_xx[branin_limit_state_function(dense_y) > 0]
true_classifications = 1*(branin_limit_state_function(dense_y) > 0)


initial_designs = np.genfromtxt("data/BH_initial_designs.csv", delimiter=',')

dim = 2
branin_gauss_kernel = 1.0 * RBF(length_scale= np.repeat(.5, dim),
                     length_scale_bounds=(1e-4, 1e4))
bounds = ((-5, 10), (0, 15))
n_init = initial_designs.shape[0]
n_cand = 10*dim
n_select = 20
MC_REPS = 30
batch_size = 5

ecl_designs = np.zeros((n_init+n_select, (dim+1)*MC_REPS))
ecl_times = np.zeros((MC_REPS,))
ecl_batch_designs = np.zeros((n_init+n_select, (dim+1)*MC_REPS))
ecl_batch_times = np.zeros((MC_REPS,))

clover_designs = np.zeros((n_init+n_select, (dim+1)*MC_REPS))
clover_times = np.zeros((MC_REPS,))

for i in range(MC_REPS):
    X0 = initial_designs[:,((dim+1)*i):((dim+1)*(i+1)-1)]
    Y0 = initial_designs[:,(dim+1)*(i+1)-1]
    
    ## Adaptive design with CLoVER
    clover_start_time = time.time() 
    clover_x, clover_y = \
        adaptive_design_with_clover(X0, Y0-threshold, branin_model, 
                                    n_select, threshold, bounds)
    clover_designs[0:len(clover_y),((dim+1)*i):((dim+1)*(i+1))] = \
        np.hstack((clover_x, clover_y.reshape((-1,1))+threshold))
    clover_times[i] = (time.time() - clover_start_time)/60
     
    ## Adaptive design with ECL
    ecl_start_time = time.time()
    init_gp = GPR(kernel=branin_gauss_kernel, alpha=1e-6)
    init_gp.fit(X0, Y0)
    eclgp = EntropyContourLocatorGP(init_gp, branin_limit_state_function)
    
    eclgp.fit(n_select, branin_model, bounds)

    ecl_designs[:,((dim+1)*i):((dim+1)*(i+1))] = \
        np.hstack((eclgp.X_, eclgp.y_.reshape((-1,1))))
    ecl_times[i] = (time.time() - ecl_start_time)/60
    
    
    ## Adaptive design with ECL (batch)
    ecl_batch_start_time = time.time()
    eclgp_batch = EntropyContourLocatorGP(init_gp, branin_limit_state_function)
    
    eclgp_batch.fit(np.int(n_select/batch_size), branin_model, bounds, 
                           batch_size=batch_size)

    ecl_batch_designs[:,((dim+1)*i):((dim+1)*(i+1))] = \
        np.hstack((eclgp_batch.X_, eclgp_batch.y_.reshape((-1,1))))
    ecl_batch_times[i] = (time.time() - ecl_batch_start_time)/60

    print(i)
    
    np.savetxt(f'data/BH_ecl_designs_{today_string}.csv', 
                ecl_designs, delimiter = ",")
    np.savetxt(f'data/BH_ecl_times_{today_string}.csv', 
                ecl_times, delimiter = ",")
    np.savetxt(f'data/BH_ecl_batch_designs_{today_string}.csv', 
                ecl_batch_designs, delimiter = ",")
    np.savetxt(f'data/BH_ecl_batch_times_{today_string}.csv', 
                ecl_batch_times, delimiter = ",")
    np.savetxt(f'data/BH_clover_designs_{today_string}.csv', 
                clover_designs, delimiter = ",")
    np.savetxt(f'data/BH_clover_times_{today_string}.csv', 
                clover_times, delimiter = ",")



