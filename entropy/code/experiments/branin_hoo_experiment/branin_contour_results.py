"""
Calculates sensitivity and contour volume based on adaptive designs for the
Branin-Hoo experiment.

@author: D. Austin Cole  austin.cole8@vt.edu
"""
import pandas as pd
import numpy as np
from sklearn.gaussian_process import GaussianProcessRegressor as GPR
from sklearn.gaussian_process.kernels import RBF
from datetime import date

import os
import sys
fileDir = os.path.dirname(os.path.abspath(__file__))
sourcePath = os.path.join(fileDir, '../../')
sys.path.append(sourcePath)
from eclGP import BraninHooModel
from eclGP import calc_classification_stats

today = date.today()
today_string = today.strftime("%m%d%y")
branin_model = BraninHooModel()


threshold = 206
def branin_limit_state_function(y):
    return y-threshold


## Estimate failure probability
'''
fail_count = np.zeros((100,))
for i in range(100):
    xx = 15* lhs(2, samples=5000000)
    xx[:,0] = xx[:,0] - 5
    yy = branin_model.predict(xx)
    fail_count[i] = np.sum(1*(branin_limit_state_function(yy) > 0))
    print(i)
    
alpha = np.mean(len(xx) - fail_count)/len(xx)
'''


## To generate reference set
'''
dense_xx = 15 * lhs(2, samples=5000000)
dense_xx[:,0] = dense_xx[:,0] - 5 
dense_yy = branin_model.predict(dense_xx)
XX = dense_xx
np.savetxt('data/BH_XX_5mil.csv', XX, delimiter = ",")
'''


XX_df = pd.read_csv(f"data/BH_XX_5mil.csv", header=None)
XX = np.array(XX_df)
yy = branin_model.predict(XX)
true_failures = XX[branin_limit_state_function(yy) > 0,:]
true_classifications = 1*(branin_limit_state_function(yy) > 0)


dim = 2
branin_gauss_kernel = 1.0 * RBF(length_scale= np.repeat(.5, dim),
                      length_scale_bounds=(1e-4, 1e4))
bounds = ((-5, 10), (0, 15))
n_init = 10
n_select = 20
MC_REPS = 30

filedate = "041621"

clover_designs_df = \
    pd.read_csv(f"data/BH_clover_designs_{filedate}.csv", header=None)
clover_designs = np.array(clover_designs_df)

ecl_designs_df = \
    pd.read_csv(f"data/BH_ecl_designs_{filedate}.csv", header=None)
ecl_designs = np.array(ecl_designs_df)

ecl_batch_designs_df = \
    pd.read_csv(f"data/BH_ecl_batch_designs_{filedate}.csv", header=None)
ecl_batch_designs = np.array(ecl_batch_designs_df)
batch_size = 5

egra_designs_df = pd.read_csv(f"data/BH_egra_designs_{filedate}.csv")
egra_designs = np.array(egra_designs_df)
ranjan_designs_df = pd.read_csv(f"data/BH_ranjan_designs_{filedate}.csv")
ranjan_designs = np.array(ranjan_designs_df)
sur_designs_df = pd.read_csv(f"data/BH_sur_designs_{filedate}.csv")
sur_designs = np.array(sur_designs_df)
timse_designs_df = pd.read_csv(f"data/BH_timse_designs_{filedate}.csv")
timse_designs = np.array(timse_designs_df)
tmse_designs_df = pd.read_csv(f"data/BH_tmse_designs_{filedate}.csv")
tmse_designs = np.array(tmse_designs_df)


ecl_area = np.zeros((MC_REPS, n_select+1))
ecl_batch_area = np.zeros((MC_REPS, np.int(n_select/batch_size)+1))
egra_area = np.zeros((MC_REPS, n_select+1))
clover_area = np.zeros((MC_REPS, n_select+1))
tmse_area = np.zeros((MC_REPS, n_select+1))
timse_area = np.zeros((MC_REPS, n_select+1))
ranjan_area = np.zeros((MC_REPS, n_select+1))
sur_area = np.zeros((MC_REPS, n_select+1))

ecl_sens = np.zeros((MC_REPS, n_select+1))
ecl_batch_sens = np.zeros((MC_REPS, np.int(n_select/batch_size)+1))
egra_sens = np.zeros((MC_REPS, n_select+1))
clover_sens = np.zeros((MC_REPS, n_select+1))
tmse_sens = np.zeros((MC_REPS, n_select+1))
timse_sens = np.zeros((MC_REPS, n_select+1))
ranjan_sens = np.zeros((MC_REPS, n_select+1))
sur_sens = np.zeros((MC_REPS, n_select+1))

designs_list = [clover_designs, ecl_designs, ecl_batch_designs, egra_designs,
                ranjan_designs, sur_designs, timse_designs, tmse_designs]
area_list = [clover_area, ecl_area, ecl_batch_area, egra_area,
             ranjan_area, sur_area, timse_area, tmse_area]
sens_list = [clover_sens, ecl_sens, ecl_batch_sens, egra_sens, 
             ranjan_sens, sur_sens, timse_sens, tmse_sens]


for j in range(MC_REPS):
    for kk in range(len(designs_list)):
        n_select_kk = sens_list[kk].shape[1]
        n_step = np.int(n_select/(n_select_kk-1))
        for k in range(n_select_kk):
            design_X = designs_list[kk][0:(n_init+n_step*k), 
                                        ((dim+1)*j):((dim+1)*(j+1)-1)]
            design_Y = designs_list[kk][0:(n_init+n_step*k), 
                                        (dim+1)*(j+1)-1]
            
            fit_gp =  GPR(kernel=branin_gauss_kernel, alpha=1e-6)
            fit_gp.fit(design_X, design_Y)
            gp_preds = fit_gp.predict(XX)
            gp_classifications = 1*(branin_limit_state_function(gp_preds) > 0)
    
            sens_list[kk][j,k]  = calc_classification_stats(
                    true_classifications, gp_classifications)[0]
            area_list[kk][j,k] = np.sum(gp_classifications)
    print(j)   
    
    np.savetxt(f'data/BH_clover_sens_{today_string}.csv', 
                clover_sens, delimiter = ",")
    np.savetxt(f'data/BH_clover_area_{today_string}.csv',
                clover_area, delimiter = ",")
    
    np.savetxt(f'data/BH_ecl_sens_{today_string}.csv', 
                ecl_sens, delimiter = ",")
    np.savetxt(f'data/BH_ecl_area_{today_string}.csv', 
                ecl_area, delimiter = ",")
    
    np.savetxt(f'data/BH_ecl_batch_sens_{today_string}.csv', 
                ecl_batch_sens, delimiter = ",")
    np.savetxt(f'data/BH_ecl_batch_area_{today_string}.csv', 
                ecl_batch_area, delimiter = ",")

    np.savetxt(f'data/BH_egra_sens_{today_string}.csv', 
                egra_sens, delimiter = ",")
    np.savetxt(f'data/BH_egra_area_{today_string}.csv', 
                egra_area, delimiter = ",")
    
    np.savetxt(f'data/BH_ranjan_sens_{today_string}.csv', 
                ranjan_sens, delimiter = ",")
    np.savetxt(f'data/BH_ranjan_area_{today_string}.csv', 
                ranjan_area, delimiter = ",")
    
    np.savetxt(f'data/BH_sur_sens_{today_string}.csv', 
                sur_sens, delimiter = ",")
    np.savetxt(f'data/BH_sur_area_{today_string}.csv', 
                sur_area, delimiter = ",")
    
    np.savetxt(f'data/BH_timse_sens_{today_string}.csv', 
                timse_sens, delimiter = ",")
    np.savetxt(f'data/BH_timse_area_{today_string}.csv', 
                timse_area, delimiter = ",")
    
    np.savetxt(f'data/BH_tmse_sens_{today_string}.csv', 
                tmse_sens, delimiter = ",")
    np.savetxt(f'data/BH_tmse_area_{today_string}.csv', 
                tmse_area, delimiter = ",")


