"""
Generates initial designs for training GPs before adaptive design. Used in
the Branin-Hoo experiment.

@author: D. Austin Cole  austin.cole8@vt.edu
"""
from datetime import date
import numpy as np
from pyDOE import lhs

from eclGP import BraninHooModel

today = date.today()
branin_model = BraninHooModel()

n_init = 10
MC_REPS = 30
initial_designs = np.zeros((n_init, 3*MC_REPS))


for i in range(MC_REPS):
    X0 = 15*lhs(2, n_init)
    X0[:,0] = X0[:,0] - 5
    Y0 = branin_model.predict(X0)
    
    initial_designs[:,(3*i):(3*(i+1))] = np.hstack((X0, Y0.reshape((-1,1))))

np.savetxt(f'data/BH_initial_designs_{today.strftime("%m%d%y")}.csv',
           initial_designs, delimiter = ",")
