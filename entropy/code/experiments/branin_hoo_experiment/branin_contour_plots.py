"""
Creates sensitivity and contour volume error plots for Branin-Hoo experiment.

@author: D. Austin Cole  austin.cole8@vt.edu
"""

from matplotlib import gridspec
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns


n_init = 10
n_select = 20
MC_REPS = 30
batch_size = 5
n_markers = n_select + 1
file_date = "041621"

clover_sens = pd.read_csv(f'data/BH_clover_sens_{file_date}.csv', header=None)
clover_area = pd.read_csv(f'data/BH_clover_area_{file_date}.csv', header=None)
ecl_sens = pd.read_csv(f'data/BH_ecl_sens_{file_date}.csv', header=None)
ecl_area = pd.read_csv(f'data/BH_ecl_area_{file_date}.csv', header=None)
ecl_batch_sens = pd.read_csv(f'data/BH_ecl_batch_sens_{file_date}.csv',
                             header=None)
ecl_batch_area = pd.read_csv(f'data/BH_ecl_batch_area_{file_date}.csv',
                             header=None)
egra_sens = pd.read_csv(f'data/BH_egra_sens_{file_date}.csv', header=None)
egra_area = pd.read_csv(f'data/BH_egra_area_{file_date}.csv', header=None)
ranjan_sens = pd.read_csv(f'data/BH_ranjan_sens_{file_date}.csv', header=None)
ranjan_area = pd.read_csv(f'data/BH_ranjan_area_{file_date}.csv', header=None)
sur_sens = pd.read_csv(f'data/BH_sur_sens_{file_date}.csv', header=None)
sur_area = pd.read_csv(f'data/BH_sur_area_{file_date}.csv', header=None)
timse_sens = pd.read_csv(f'data/BH_timse_sens_{file_date}.csv', header=None)
timse_area = pd.read_csv(f'data/BH_timse_area_{file_date}.csv', header=None)
tmse_sens = pd.read_csv(f'data/BH_tmse_sens_{file_date}.csv', header=None)
tmse_area = pd.read_csv(f'data/BH_tmse_area_{file_date}.csv', header=None)

## Change to relative error of area estimates
true_count = 48406.86

clover_err = np.abs(clover_area - true_count)/true_count
ecl_err = np.abs(ecl_area - true_count)/true_count
ecl_batch_err = np.abs(ecl_batch_area - true_count)/true_count
egra_err = np.abs(egra_area - true_count)/true_count
tmse_err = np.abs(tmse_area - true_count)/true_count
timse_err = np.abs(timse_area - true_count)/true_count
sur_err = np.abs(sur_area - true_count)/true_count
ranjan_err = np.abs(ranjan_area - true_count)/true_count


methods = ['ECL','ECL.b','CLoVER','EGRA','Ranjan','SUR','tIMSE','tMSE']
color_palette = ['royalblue', 'aqua','g','magenta',
                 'brown', 'orange','r','purple']
dash_styles = [(10,0), (4,3), (3, 5, 1, 1),  (2,2), 
               (4, 1), (3, 1,1,1), (1, 2,2,1), (2, 3, 2, 1)]


##--------Combine results into dataframe for each subplot-------##
## GP sensitivity over function evaluations
ecl_batch_sens_melted = ecl_batch_sens.melt()
ecl_batch_sens_melted['variable'] = ecl_batch_sens_melted['variable']*batch_size
combined_sens_mc = pd.concat([ecl_sens.melt(), ecl_batch_sens_melted,
                              clover_sens.melt(), egra_sens.melt(),
                              ranjan_sens.melt(), sur_sens.melt(), 
                              timse_sens.melt(), tmse_sens.melt()])
combined_sens_mc = combined_sens_mc.rename(columns={"value": "Sensitivity"})
combined_sens_mc = \
    combined_sens_mc.rename(columns={"variable": "Function evaluations"})
combined_sens_mc['Function evaluations'] = \
    combined_sens_mc['Function evaluations'] + n_init

descript_pt1 = [methods[0]]*MC_REPS*n_markers
descript_pt2 = [methods[1]]*MC_REPS*(np.int(n_select/batch_size)+1)
descript_pt3 = \
    [item for item in methods[2:len(methods)] for i in range(MC_REPS*(n_markers))]
combined_sens_mc['Method'] = descript_pt1 + descript_pt2 + descript_pt3


## Final designs sensitivity
last_sens_mc = pd.concat([ecl_sens.iloc[:,n_markers-1],
                          ecl_batch_sens.iloc[:,np.int(n_markers/batch_size)-1],
                          clover_sens.iloc[:,n_markers-1],
                          egra_sens.iloc[:,n_markers-1],
                          ranjan_sens.iloc[:,n_markers-1],
                          sur_sens.iloc[:,n_markers-1],
                          timse_sens.iloc[:,n_markers-1],
                          tmse_sens.iloc[:,n_markers-1]],
                          ignore_index=True)
descript = pd.DataFrame([item for item in methods for i in range(MC_REPS)])

last_sens_df = pd.concat([last_sens_mc, descript],axis=1)
last_sens_df = last_sens_df.set_axis(['Sensitivity', 'Method'], axis=1)


## Absolute relative error of contour volume over function evaluations
ecl_batch_err_melted = ecl_batch_err.melt()
ecl_batch_err_melted['variable'] = ecl_batch_err_melted['variable']*batch_size
combined_err_mc = pd.concat([ecl_err.melt(), ecl_batch_err_melted,
                             clover_err.melt(), egra_err.melt(),
                             ranjan_err.melt(), sur_err.melt(), 
                             timse_err.melt(), tmse_err.melt()])
combined_err_mc = combined_err_mc.rename(columns={"value": "Relative error"})
combined_err_mc = \
    combined_err_mc.rename(columns={"variable": "Function evaluations"})
combined_err_mc['Function evaluations'] = \
    combined_err_mc['Function evaluations'] + n_init
descript_pt1 = [methods[0]]*MC_REPS*n_markers
descript_pt2 = [methods[1]]*MC_REPS*(np.int(n_select/batch_size)+1)
descript_pt3 = \
    [item for item in methods[2:len(methods)] for i in range(MC_REPS*(n_markers))]
combined_err_mc['Method'] = descript_pt1 + descript_pt2 + descript_pt3


## Final design absolute relative error of contour volume
last_err_mc = pd.concat([ecl_err.iloc[:,n_markers-1],
                         ecl_batch_err.iloc[:,np.int(n_markers/batch_size)-1],
                          clover_err.iloc[:,n_markers-1],
                          egra_err.iloc[:,n_markers-1],
                          ranjan_err.iloc[:,n_markers-1],
                          sur_err.iloc[:,n_markers-1],
                          timse_err.iloc[:,n_markers-1],
                          tmse_err.iloc[:,n_markers-1]],
                          ignore_index=True)
last_err_mc = last_err_mc+1e-10
descript = pd.DataFrame([item for item in methods for i in range(MC_REPS)])
last_re_df = pd.concat([last_err_mc, descript],axis=1)
last_re_df = last_re_df.set_axis(['Relative error', 'Method'], axis=1)




###----All plots in one figure----###
fig = plt.figure(figsize=(12, 5)) 

plt.subplots_adjust(left=0.05, bottom=0.15, right=.98, top=.98, wspace=0.15,
                    hspace=0.05)
outer = gridspec.GridSpec(1, 2, width_ratios=[4, 4]) 
gs1 = gridspec.GridSpecFromSubplotSpec(1, 2, subplot_spec = outer[0], 
                                       width_ratios=[3, 1], wspace=0.08)
gs2 = gridspec.GridSpecFromSubplotSpec(1, 2, subplot_spec = outer[1], 
                                       width_ratios=[3, 1], wspace = .21)
ax1 = plt.subplot(gs1[0])
ax1 = sns.lineplot(x="Function evaluations",
                    y="Sensitivity", hue="Method", style='Method',
                    data=combined_sens_mc, 
                    palette=color_palette,
                    markers=False, dashes=dash_styles, ci=None)
ax1.set_xticks(range(n_init,n_init+n_select+1, 2))
ax1.get_legend().remove()
ax2 = plt.subplot(gs1[1])
ax2 = sns.boxplot( x="Method", y="Sensitivity", data=last_sens_df,
                 palette=color_palette, fliersize=4)
ax2.set_xticklabels(ax2.get_xticklabels(),rotation=90)
ax2.set(xlabel=None, ylabel=None)
ax2.set_yticklabels([])

ax3 = plt.subplot(gs2[0])
ax3 = sns.lineplot(x="Function evaluations",
                    y="Relative error", hue="Method", style='Method',
                    data=combined_err_mc, 
                    palette=color_palette,
                    dashes=dash_styles,
                    markers=False, ci=None)

ax3.set(yscale='log', ylim=(3e-3, 1.1),
        ylabel='Absolute relative error of volume estimate')
ax3.set_xticks(range(n_init,n_init+n_select+1, 2))
ax3.get_legend().remove()
ax4 = plt.subplot(gs2[1])
ax4 = sns.boxplot( x="Method", y="Relative error", data=last_re_df,
                 palette=color_palette, fliersize=4)
ax4.set_xticklabels(ax4.get_xticklabels(),rotation=90)
ax4.set(yscale='log', ylim=(5e-5, 1))
ax4.set(xlabel=None, ylabel=None)

st = fig.suptitle("Branin-Hoo", fontsize="x-large")
st.set_y(0.95)
fig.subplots_adjust(top=0.90)
# plt.tight_layout()
plt.savefig(f'BH_experiment_results.pdf')
#plt.savefig('BH_experiment_results.png')
