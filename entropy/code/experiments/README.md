## Experiments performed in the paper, "Entropy-based adaptive design for contour finding and estimating reliability"
### D. Austin Cole, Robert B. Gramacy, James E. Warner. Geoffrey F. Bomarito, Patrick E. Leser, William P. Leser

## Includes Folders:	
- **branin_hoo_experiment**: simulation comparing adaptive design GPs based on different contour locating acquisition functions on the Branin-Hoo function. Described in Section 4.2.
- **ecl_surface_and_design**: showcasing the relationships between the GP and ECL surfaces for a multimodal function. Also comparing sequential and batch selections. Described in Sections 3.1-3.
- **ecl_vs_clover_opt_strategies**: simulation comparing ECL and CLoVER adaptive design GPs with different optimization strategies on the Branin-Hoo and Ishigami functions. Described in the Appendix.
- **hartmann6_experiment**: simulation comparing adaptive design GPs based on different contour locating acquisition functions on the Hartmann-6 function. Described in Section 4.2.
- **initial_lhs_vs_ecl_mc_bakeoff**: experiment showcasing Monte Carlo versus MFIS estimates with various GP designs on the Ishigami function. Described in Section 2.1.
- **ishigami_experiment**: simulation comparing adaptive design GPs based on different contour locating acquisition functions on the Ishigami function. Described in Section 4.2.