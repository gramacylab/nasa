"""
Creates an ECL heatmap and compares sequential and batch ECL design, applied
to the multimodal function.

@author: D. Austin Cole  austin.cole8@vt.edu
"""
from datetime import date
from matplotlib import gridspec
from matplotlib.lines import Line2D
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from pyDOE import lhs
from sklearn.gaussian_process import GaussianProcessRegressor as GPR
from sklearn.gaussian_process.kernels import RBF

import os
import sys
fileDir = os.path.dirname(os.path.abspath(__file__))
sourcePath = os.path.join(fileDir, '../../')
sys.path.append(sourcePath)
from eclGP import EntropyContourLocatorGP


today = date.today()

class MultiModalModel:
    def __init__(self):
        pass

    def predict(self, X):
        X0 = np.copy(X)
        
        y = (X0[:,0]**2+4)*(X0[:,1]-1)/20 - np.sin(5*X0[:,0]/2) - 2
        
        return y


mm_model = MultiModalModel()
high_fidelity_model = mm_model
bounds = ((-4, 7), (-3, 8))
gauss_kernel = 1.0 * RBF(length_scale= np.repeat(.5, 2),
                     length_scale_bounds=(1e-4, 1e4))

## Dense set of points for Multimodal failure contour
dense_x1_seq = np.linspace(bounds[0][0], bounds[0][1], 1000)
dense_x2_seq = np.linspace(bounds[1][0], bounds[1][1], 1000)
dense_x1, dense_x2 = np.meshgrid(dense_x1_seq, dense_x2_seq)
dense_xx = np.hstack((dense_x1.reshape((-1, 1)), dense_x2.reshape((-1, 1))))
dense_y = mm_model.predict(dense_xx)
densey_mat = np.array(dense_y).reshape((1000, 1000))

threshold = 0
true_failures = dense_xx[dense_y > threshold]
def limit_state_function(y):
    return -y

## failure contour
contour_ind = np.abs(dense_y - threshold) < .01
dense_contour = dense_xx[contour_ind,:]


## Grid of points for ECL surface
n_grid_1d = 200
x1_seq = np.linspace(bounds[0][0], bounds[0][1], n_grid_1d)
x2_seq = np.linspace(bounds[1][0], bounds[1][1], n_grid_1d)
xv, yv = np.meshgrid(x1_seq, x2_seq)
entropy_surface = np.zeros((len(x1_seq), len(x2_seq)))


n_cand = 20
n_start = 20

## To generate initial LHS design and candidate set
'''
X = 11*lhs(2, n_start)
X[:,0] = X[:,0] - 4
X[:,1] = X[:,1] - 3
Y = mm_model.predict(X)

Xcand = 11*lhs(2, n_cand)
Xcand[:,0] = Xcand[:,0] - 4
Xcand[:,1] = Xcand[:,1] - 3
xy_init_design = np.hstack((X,Y.reshape((-1,1)), Xcand))
xy_init_design_df = pd.DataFrame(xy_init_design)
xy_init_design_df = xy_init_design_df.rename(columns={0:'x1',1:'x2',2:'y',
                                                      3:'x1cand',4:'x2cand'})
xy_init_design_df.to_csv('multimodal_initial_design_0424.csv', index=False)
'''

## Loading saved initial design & candidate set
xy_init_design = np.array(pd.read_csv("multimodal_initial_design_0424.csv"))
X = xy_init_design[:,0:2]
Y = xy_init_design[:,2]
X_cand = xy_init_design[:,3:5]

initial_gp = GPR(kernel=gauss_kernel, alpha=1e-6)
initial_gp.fit(X, Y)
ad_gp = EntropyContourLocatorGP(initial_gp, limit_state_function)

## Initial values of ECL 
for k1 in range(len(x1_seq)):
    for k2 in range(len(x2_seq)):
            x_pt = np.array([x1_seq[k1], x2_seq[k2]]).reshape((1,-1))
            entropy_surface[k1, k2] = ad_gp.calc_entropy(x_pt)


## Returns selected sample point with original candidate (with highest ECL)
x_new_opt, x_new_cand = ad_gp.select_samples(
            bounds, X_cand=X_cand, return_cand_preopt = True)

## Performs gradient optimization on all candidate points
x_cand_opt = np.empty((n_cand, ad_gp.X_.shape[1]))
x_cand_entropy = np.empty((n_cand, ))
for i in range(n_cand):
        x_cand_i = X_cand[i,:].reshape((1,-1))
        x_cand_opt[i,:], x_cand_entropy[i] = \
            ad_gp.opt_entropy(x_cand_i, bounds, X=X, KX_inv=None)

selected_ind = (-x_cand_entropy).argsort()[:1]
x_cand_opt_best = x_cand_opt[selected_ind,:].reshape((1,-1))   
   
##---------------------------------------------------------------
## ECL surface with candidates, true contour, and selected point
fig = plt.figure(figsize=(30, 15)) 
gs1 = gridspec.GridSpec(1, 2)
gs1.update(wspace=0.05, hspace=0.05)


fig1, (ax1, ax2) = plt.subplots(1,2, sharey=True)
ax1 = plt.subplot(gs1[0])
pc = ax1.pcolormesh(x1_seq, x2_seq, entropy_surface.T, shading="auto",cmap="autumn")
ax1.set_xlim(np.min(x1_seq)-.3, np.max(x1_seq)+.3)
ax1.set_ylim(np.min(x2_seq)-.3, np.max(x2_seq)+.3)
cntr1 = ax1.contour(dense_x1_seq, dense_x2_seq, densey_mat, np.array([0]),
        alpha=.5, linewidths=2, colors='black')
p1, = ax1.plot(ad_gp.X_[:,0], ad_gp.X_[:,1], '.', markersize=8, c='k',
            label='Initial design')

p2, = ax1.plot(X_cand[:,0], X_cand[:,1], 'o', markersize=4, 
            c='cyan', fillstyle='none', label='Candidate point')
ax1.annotate('', xy=(x_new_opt[0,0],x_new_opt[0,1]-.05), 
            xytext=(x_new_cand[0,0], x_new_cand[0,1]),
            arrowprops={'arrowstyle': '->','color':'k'}, 
            va='center')
p4, = ax1.plot(x_cand_opt_best[:,0],x_cand_opt_best[:,1], 'o',
            marker='*', markersize=8, c='purple',
            label='Entropy global optimum')
p5, = ax1.plot(x_new_opt[:,0], x_new_opt[:,1], 'x', markersize=8, c='green',
         label='Sequentially selected point')


ax1.set_xlabel('X1')
ax1.set_ylabel('X2')
# ax1.set_xlim(np.min(x1_seq)-.2, np.max(x1_seq)+.2)
# ax1.set_ylim(np.min(x2_seq)-.2, np.max(x2_seq)+.2)
# plt.title('Multimodal function\'s entropy surface')
ax1.set_aspect('equal')

## Now selects 5 samples sequentially and with batch size of 5
init_preds = initial_gp.predict(dense_xx)
init_preds_mat = np.array(init_preds).reshape((1000, 1000)) ## for contour

ad_gp = EntropyContourLocatorGP(initial_gp, limit_state_function)
ad_gp_seq = EntropyContourLocatorGP(initial_gp, limit_state_function)
x_news_opt = np.zeros((5, 2))
x_news_cand = np.zeros((5, 2))

## batch selection
ad_gp_batch = EntropyContourLocatorGP(initial_gp, limit_state_function)
xb_news_opt, xb_news_cand = ad_gp_batch.select_samples(
    bounds, batch_size=5, X_cand=X_cand, return_cand_preopt=True)
y_batch = high_fidelity_model.predict(xb_news_opt)

ad_gp_batch.add_observation(xb_news_opt, y_batch)

## sequential selection
for i in range(5):
    if i > 0:
        X_cand = ad_gp_seq._generate_cand_set(n_cand, 
                                          ad_gp_seq.X_.shape[1], bounds)
    X_batch = np.empty((0, X_cand.shape[1]))
    
    x_news_opt[i,:], x_news_cand[i,:] = \
        ad_gp_seq.select_samples(
            bounds, X_cand=X_cand, return_cand_preopt = True)
   
    y_new = high_fidelity_model.predict(x_news_opt[i,:].reshape((1,-1)))
    ad_gp_seq.add_observation(x_news_opt[i,:].reshape((1,-1)), y_new)
    
   
seq_preds = ad_gp_seq.predict(dense_xx)
seq_mat = np.array(seq_preds).reshape((1000, 1000)) ## for contour
batch_preds = ad_gp_batch.predict(dense_xx)
batch_mat = np.array(batch_preds).reshape((1000, 1000)) ## for contour
    
ax2 = plt.subplot(gs1[1])
ax2.set_xlim(np.min(x1_seq)-.3, np.max(x1_seq)+.3)
ax2.set_ylim(np.min(x2_seq)-.3, np.max(x2_seq)+.3)
ax2.scatter(X[:,0], X[:,1], s=8, c='k')

ax2.contour(dense_x1_seq, dense_x2_seq, init_preds_mat, np.array([0]),
            alpha=.6, linewidths=2, colors='magenta', linestyles='dashdot')
ax2.contour(dense_x1_seq, dense_x2_seq, densey_mat, np.array([0]),
            alpha=.6, linewidths=2, colors='black')
ax2.contour(dense_x1_seq, dense_x2_seq, seq_mat, np.array([0]),
            alpha=.6, linewidths=2,colors='green', linestyles='dashed')
ax2.contour(dense_x1_seq, dense_x2_seq, batch_mat, np.array([0]),
            alpha=.6, linewidths=2, colors='blue', linestyles='dotted')

ax2.plot(x_news_opt[:,0], x_news_opt[:,1], 'ro', marker='x', c='green',
            linewidth=1, markersize=8)
p6, = ax2.plot(xb_news_opt[:,0], xb_news_opt[:,1], 'ro', marker='+', c='blue',
            linewidth=1, markersize=8, label='Batch selected point')
ax2.set_xlabel('X1')
ax2.set_yticklabels([])
# ax2.ylabel('X2')

ax2.set_aspect('equal')

## Personalized legend  
'''
custom_lines = [Line2D([0], [0], color='k', lw=2, alpha=.8),
                Line2D([0], [0], color='magenta', ls='dashdot', lw=2, alpha=.8),
                Line2D([0], [0], color='green', ls='dashed', lw=2, alpha=.8),
                Line2D([0], [0], color='blue', ls='dotted', lw=2, alpha=.8)]
handles = [p1, p2, p4, p5, p6] + custom_lines
labels = [p1.get_label(), p2.get_label(), p4.get_label(),
          p5.get_label(), p6.get_label(), 'True contour',
          'Initial design GP contour',
          'Sequential design GP contour', 'Batch design GP contour']

lgd = plt.legend(handles=handles, labels=labels, 
           bbox_to_anchor=(1.05, 1), loc='upper left', borderaxespad=0.)
plt.savefig('figures/mm_ent_surface_and_batch.pdf',
            dpi=200, bbox_extra_artists=(lgd,), bbox_inches='tight')
'''
plt.savefig('mm_ent_surface_and_batch_plots.pdf',
            dpi=200, bbox_inches='tight')
