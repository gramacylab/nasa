"""
Creates a lineplot showing the relationship between the GP predictive and ECL 
surfaces along a slice, applied to the multimodal function.

@author: D. Austin Cole  austin.cole8@vt.edu
"""


import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.gaussian_process import GaussianProcessRegressor as GPR
from sklearn.gaussian_process.kernels import RBF

import os
import sys
fileDir = os.path.dirname(os.path.abspath(__file__))
sourcePath = os.path.join(fileDir, '../../')
sys.path.append(sourcePath)
from eclGP import EntropyContourLocatorGP


class MultiModalModel:
    def __init__(self):
        pass

    def predict(self, X):
        X0 = np.copy(X)
        
        y = (X0[:,0]**2+4)*(X0[:,1]-1)/20 - np.sin(5*X0[:,0]/2) - 2
        
        return y


mm_model = MultiModalModel()
bounds = ((-4, 7), (-3, 8))

dense_x1_seq = np.linspace(bounds[0][0], bounds[0][1], 1000)
dense_x2_seq = np.linspace(bounds[1][0], bounds[1][1], 1000)
dense_x1, dense_x2 = np.meshgrid(dense_x1_seq, dense_x2_seq)
dense_xx = np.hstack((dense_x1.reshape((-1, 1)), dense_x2.reshape((-1, 1))))
dense_y = mm_model.predict(dense_xx)

threshold = 0
true_failures = dense_xx[dense_y > threshold]
def limit_state_function(y):
    return -y


n_grid_1d = 200
x1_seq = np.linspace(bounds[0][0], bounds[0][1], n_grid_1d)
x2_seq = np.linspace(bounds[1][0], bounds[1][1], n_grid_1d)

gauss_kernel = 1.0 * RBF(length_scale= np.repeat(.5, 2),
                     length_scale_bounds=(1e-4, 1e4))
entropy_surface = np.zeros((len(x1_seq), len(x2_seq)))


n_cand = 20
n_start = 20

## Loading from saved design
xy_init_design = np.array(pd.read_csv("multimodal_initial_design_0424.csv"))
X = xy_init_design[:,0:2]
Y = xy_init_design[:,2]
X_cand = xy_init_design[:,3:5]

initial_gp = GPR(kernel=gauss_kernel, alpha=1e-6)
initial_gp.fit(X, Y)

ad_gp = EntropyContourLocatorGP(initial_gp, limit_state_function)

## Select next sample with ECL
x_new_opt, x_new_cand = ad_gp.select_samples(
    bounds, X_cand=X_cand, return_cand_preopt = True)

## Gradient optimization using all candidate points
x_cand_opt = np.empty((n_cand, ad_gp.X_.shape[1]))
x_cand_entropy = np.empty((n_cand, ))
for i in range(n_cand):
        x_cand_i = X_cand[i,:].reshape((1,-1))
        x_cand_opt[i,:], x_cand_entropy[i] = \
            ad_gp.opt_entropy(x_cand_i, bounds, X=X, KX_inv=None)

selected_ind = (-x_cand_entropy).argsort()[:1]
x_cand_opt_best = x_cand_opt[selected_ind,:].reshape((1,-1))
x_cand_opt_x2 = np.round(x_cand_opt_best[0,1],3)

## slice of gp_pred and entropy
x2_slice_val = np.array(len(x1_seq)*[5.8]).reshape((-1,1))
xslice = np.hstack((x1_seq.reshape((-1,1)), x2_slice_val))
gp_preds, gp_pred_var = ad_gp.predict(xslice, return_var=True)
slice_entropy = np.zeros((len(x1_seq),))
for j in range(len(x1_seq)):
    xslice_j = xslice[j,:].reshape((1,-1))
    slice_entropy[j] = ad_gp.calc_entropy(xslice_j)

ci_95_upper = gp_preds+1.645*np.sqrt(gp_pred_var)
ci_95_lower = gp_preds-1.645*np.sqrt(gp_pred_var)

    
## ECL slice with GP mean and CI
fig, ax1 = plt.subplots()
ax1.set_xlabel('X1')
ax1.set_ylabel('Y', color='blue')
ax1.axhline(y=0, c='k', linestyle='--', label='Threshold')
ax1.plot(x1_seq, gp_preds, c='blue', linestyle='solid',
         label='GP predicted mean')
ax1.plot(x1_seq, ci_95_upper, c='r',linestyle='dotted', label='GP 95% CI')
ax1.plot(x1_seq, ci_95_lower, c='r',linestyle='dotted')
ax1.tick_params(axis='y', labelcolor='blue')

ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis

ax2.set_ylabel('ECL', color='green') 
if np.max(slice_entropy) > 0:
    plt.plot(x1_seq,slice_entropy, c='green',linestyle='solid',
             alpha=.5, label='ECL')
ax2.tick_params(axis='y', labelcolor='green')
ax2.set(ylim=(-0.02, 1.07))
plt.title(f'Multimodal function\'s GP prediction \n seq point #1;x2={x2_slice_val[0,0]}')
fig.legend(loc='upper left', bbox_to_anchor=(.13, 0.36, 0.5, 0.5))
fig.tight_layout() 
plt.savefig('mm_ecl_slice.pdf')
#plt.savefig('mm_ecl_slice1.png', dpi=200)
plt.show()
    
