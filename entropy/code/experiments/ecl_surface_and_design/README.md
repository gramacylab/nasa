## ECL surfaces on the Multimodal function performed in the paper, "Entropy-based adaptive design for contour finding and estimating reliability"
### D. Austin Cole, Robert B. Gramacy, James E. Warner, Geoffrey F. Bomarito, Patrick E. Leser, William P. Leser

## Includes:	
- **mm_ecl_slive_figure.py**: script to generate figure comparing GP predictive distribution and ECL surface. Corresponds to plot in Section 3.1.
- **mm_ent_surface_and_batch_plots.py**: script to conduct sequential and batch ECL adpative designs and produce plots of the designs, including heat maps of the ECL surface. Corresponds to plot in Section 3.2.