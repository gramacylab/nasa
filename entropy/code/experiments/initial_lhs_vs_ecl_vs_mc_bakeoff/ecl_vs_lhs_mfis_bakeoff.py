"""
A bakeoff comparing contour estimation and failure probability estimates
for the Ishigami function. Compares Monte Carlo, a space-filling GP + MFIS, 
and ECL adaptive design GP + MFIS.

@author: D. Austin Cole  austin.cole8@vt.edu
"""
from datetime import date
import numpy as np
import pandas as pd
from pyDOE import lhs
import scipy.stats as ss
from sklearn.gaussian_process import GaussianProcessRegressor as GPR
from sklearn.gaussian_process.kernels import RBF
import warnings

import os
import sys
fileDir = os.path.dirname(os.path.abspath(__file__))
sourcePath = os.path.join(fileDir, '../../../../../MFISPy')
sys.path.append(sourcePath)
from mfis import BiasingDistribution
from mfis import MultiFidelityIS
from mfis.mv_independent_distribution import MVIndependentDistribution

sourcePath = os.path.join(fileDir, '../../')
sys.path.append(sourcePath)
from eclGP import IshigamiModel
from eclGP import EntropyContourLocatorGP
from eclGP import calc_classification_stats

today = date.today()
today_string = today.strftime("%m%d%y")

unif_dist = ss.uniform(-np.pi, 2*np.pi)
input_distribution = MVIndependentDistribution(distributions=
                                               [unif_dist,unif_dist,unif_dist])

ishigami_model = IshigamiModel()
bounds = ((-np.pi, np.pi), (-np.pi, np.pi), (-np.pi, np.pi))

MC_REPS = 50
hf_samples = [500, 1000, 1500, 2000, 2500, 3000, 3500, 4000, 4500, 5000]
ecl_mfis_probs = np.ones((MC_REPS, len(hf_samples)))
lhs_mfis_probs = np.ones((MC_REPS, len(hf_samples)))
mc_probs = np.ones((MC_REPS, len(hf_samples)))

ecl_sens = np.zeros((MC_REPS))
ecl_spec = np.zeros((MC_REPS))
lhs_sens = np.zeros((MC_REPS))
lhs_spec = np.zeros((MC_REPS))

sens_stats = np.zeros((MC_REPS,2))
spec_stats = np.zeros((MC_REPS,2))


failure_threshold =  -9.2244 #prob 1e-3

def ishigami_limit_state_function(y):
    return -y+failure_threshold

'''
## Create reference set
dense_xx = 2*np.pi * lhs(3, samples=1000000) - np.pi
dense_yy = ishigami_model.predict(dense_xx)
XX = dense_xx[dense_yy < -1,:]
np.savetxt('../ishigami_experiment/data/Ish_XX_1mil.csv', XX, delimiter = ",")
'''

XX_df = pd.read_csv("../ishigami_experiment/data/Ish_XX_1mil.csv", header=None)
XX = np.array(XX_df)
yy = ishigami_model.predict(XX)
true_failures = XX[ishigami_limit_state_function(yy) > 0,:]
true_classifications = 1*(ishigami_limit_state_function(yy) > 0)

ish_gauss_kernel = 1.0 * RBF(length_scale= np.repeat(.5, 3),
                             length_scale_bounds=(1e-4, 1e4))

n_start = 30
N = 200


for i in range(MC_REPS):

    ## ECL design
    X = 2 * np.pi * lhs(3, n_start) - np.pi
    Y = ishigami_model.predict(X)

    gauss_kernel = 1.0**2 * RBF(length_scale= np.repeat(.1, X.shape[1]))
    initial_gp = GPR(kernel=gauss_kernel, alpha=1e-6)
    initial_gp.fit(X, Y)

    ecl_gp = EntropyContourLocatorGP(initial_gp, ishigami_limit_state_function)
    ecl_gp.fit(N-n_start, ishigami_model, bounds)

    ## N-sized space-filling GP
    X_lhs = 2*np.pi*lhs(3, N) - np.pi
    Y_lhs = ishigami_model.predict(X_lhs)
    lhs_gp = GPR(kernel=ish_gauss_kernel, alpha=1e-6)
    lhs_gp.fit(X_lhs, Y_lhs)
    
    
    ## Calculate sensitivity & specificity
    ecl_gp_dense_predictions = ecl_gp.predict(XX)
    ecl_failure_ind = 1*(ecl_gp_dense_predictions < failure_threshold)
    sens_stats[i,0], spec_stats[i,0] = \
        calc_classification_stats(true_classifications, ecl_failure_ind)
    
    lhs_gp_dense_predictions =  lhs_gp.predict(XX)
    lhs_failure_ind = 1*(lhs_gp_dense_predictions.flatten() < failure_threshold)
    sens_stats[i,1], spec_stats[i,1] = \
        calc_classification_stats(true_classifications, lhs_failure_ind)
  
    
    # Initialize biasing distributions
    lhs_bd = BiasingDistribution(trained_surrogate=lhs_gp,
                            limit_state=ishigami_limit_state_function,
                            input_distribution=input_distribution)
     
    ecl_bd =  BiasingDistribution(trained_surrogate=ecl_gp,
                            limit_state=ishigami_limit_state_function,
                            input_distribution=input_distribution)
    
    
    ## Fit biasing distributions
    ecl_failed_inputs = np.empty((0, 3))   
    lhs_failed_inputs = np.empty((0, 3))
        
    for k in range(10):
        sample_inputs = input_distribution.draw_samples(100000)
        with warnings.catch_warnings():
            warnings.simplefilter("ignore")
            ecl_sample_outputs, ecl_sample_var = \
                ecl_gp.predict(sample_inputs, return_var=True)
            ecl_failed_inputs1 = sample_inputs[
                (ecl_sample_outputs.flatten() - \
                 1.645*np.sqrt(ecl_sample_var)) < failure_threshold,:]
            ecl_failed_inputs = \
                np.vstack((ecl_failed_inputs, ecl_failed_inputs1))
        
        
            lhs_sample_outputs, lhs_sample_std = lhs_gp.predict(sample_inputs,
                                                              return_std=True)
            lhs_failed_inputs1 = sample_inputs[
                lhs_sample_outputs.flatten() - \
                    1.645*lhs_sample_std < failure_threshold,:]
            lhs_failed_inputs = np.vstack((lhs_failed_inputs,
                                           lhs_failed_inputs1))
       
        
    ecl_bd.fit_from_failed_inputs(ecl_failed_inputs, max_clusters=10,
                                  covariance_type='diag')
    
    lhs_bd.fit_from_failed_inputs(lhs_failed_inputs, max_clusters=10,
                                  covariance_type='diag')

    
    ## Failure probability estimates based on different sample sizes
    XX_ecl = np.empty((0,3))
    XX_lhs = np.empty((0,3))
    XX_mc = np.empty((0,3))
   
    for j, hf_sample in enumerate(hf_samples):
        N_HIGH_FIDELITY_INPUTS = hf_sample 
        
        if j == 0:
            XXnew_ecl = ecl_bd.draw_samples(N_HIGH_FIDELITY_INPUTS - N)
        else:
            XXnew_ecl = \
                ecl_bd.draw_samples(N_HIGH_FIDELITY_INPUTS - hf_samples[j-1])
        XX_ecl = np.vstack((XX_ecl, XXnew_ecl))
        
        hf_ecl_outputs = ishigami_model.predict(XX_ecl)
        multi_IS_ecl = \
            MultiFidelityIS(limit_state=ishigami_limit_state_function,
                            biasing_distribution=ecl_bd,
                            input_distribution=input_distribution,
                            bounds=bounds)
        ecl_mfis_stats = \
            multi_IS_ecl.get_failure_prob_estimate(XX_ecl, hf_ecl_outputs)
        ecl_mfis_probs[i, j] = ecl_mfis_stats[0]

        
        if j == 0:
            XXnew_lhs = lhs_bd.draw_samples(N_HIGH_FIDELITY_INPUTS - N)
        else:
            XXnew_lhs = lhs_bd.draw_samples(
                N_HIGH_FIDELITY_INPUTS-hf_samples[j-1])
        XX_lhs = np.vstack((XX_lhs, XXnew_lhs))
         
        hf_lhs_outputs = ishigami_model.predict(XX_lhs)
        multi_IS_lhs = \
            MultiFidelityIS(limit_state=ishigami_limit_state_function,
                            biasing_distribution=lhs_bd,
                            input_distribution=input_distribution,
                            bounds=bounds)
        lhs_mfis_stats = \
            multi_IS_lhs.get_failure_prob_estimate(XX_lhs, hf_lhs_outputs)
        lhs_mfis_probs[i, j] = lhs_mfis_stats[0]

        
        if j == 0:
            XXnew_mc = input_distribution.draw_samples(N_HIGH_FIDELITY_INPUTS)
        else:
            XXnew_mc = input_distribution.draw_samples(
                N_HIGH_FIDELITY_INPUTS-hf_samples[j-1])
        XX_mc = np.vstack((XX_mc, XXnew_mc))
        mc_outputs = ishigami_model.predict(XX_mc)
        mc_failures = XX_mc[mc_outputs < failure_threshold,:]
        mc_probs[i, j] = len(mc_failures) / (N + N_HIGH_FIDELITY_INPUTS)

    print(i)


spec_stats_df = pd.DataFrame(spec_stats)
spec_stats_df = spec_stats_df.rename(columns={0: "ECL", 1: "LHS"})
spec_stats_df.to_csv(f'ecl_vs_lhs_spec_stats_{today_string}.csv', index=False)

sens_stats_df = pd.DataFrame(sens_stats)
sens_stats_df = sens_stats_df.rename(columns={0: "ECL", 1: "LHS"})
sens_stats_df.to_csv(f'ecl_vs_lhs_sens_stats_{today_string}.csv', index=False)


ecl_mfis_results_df = pd.DataFrame(np.abs(ecl_mfis_probs-1e-3)).melt()
lhs_mfis_results_df = pd.DataFrame(np.abs(lhs_mfis_probs-1e-3)).melt()
mc_results_df = pd.DataFrame(np.abs(mc_probs-1e-3)).melt()
combined_mc = pd.concat(
    [ecl_mfis_results_df, lhs_mfis_results_df, mc_results_df])
combined_mc = combined_mc.rename(columns={"value": "Failure probability"})
descript = ['ECL GP + MFIS']*MC_REPS*len(hf_samples) + \
    ['LHS GP + MFIS']*MC_REPS*len(hf_samples) +\
    ['MC']*MC_REPS*len(hf_samples)
        
combined_mc['Method'] = descript    
M = [ele for ele in hf_samples for i in range(MC_REPS)]
combined_mc['High Fidelity Runs'] = M + M + M

combined_mc.to_csv(f'ecl_vs_lhs_combined_relative_error_{today_string}.csv', index=False)
