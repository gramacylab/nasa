"""
Produces plots comparing GP fits and MFIS estimates between ECL designed GP,
LHS designed GP, and Monte Carlo for the Ishigami function.

@author: D. Austin Cole  austin.cole8@vt.edu
"""
from datetime import date
from matplotlib import gridspec
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns

sns.set_style('white')
plt.rcParams['ytick.left']=True
plt.rcParams['xtick.bottom']=True

filedate = '041621'

spec_stats_df = pd.read_csv(f'ecl_vs_lhs_spec_stats_{filedate}.csv')
sens_stats_df = pd.read_csv(f'ecl_vs_lhs_sens_stats_{filedate}.csv')
spec_stats = np.array(spec_stats_df)
sens_stats = np.array(sens_stats_df)


mfis_error_df = pd.read_csv(f'ecl_vs_lhs_combined_relative_error_{filedate}.csv')


##_------------------------------------
fig = plt.figure(figsize=(10, 4)) 
plt.subplots_adjust(left=0.1, bottom=0.15, right=.92, top=.98, wspace=0,
                    hspace=0)
gs = gridspec.GridSpec(1, 2, width_ratios=[3, 1]) 
gs.update(wspace=0.19, hspace=0.2) # set the spacing between axes. 
lines = plt.subplot(gs[0])
lines = sns.lineplot(x="High Fidelity Runs",
                    y="Failure probability", hue="Method", style='Method',
                    data=mfis_error_df, palette="Set1",
                    markers=True)
lines.set(yscale='log', ylim=(1e-5, .3e-2))
lines.set(xlabel='Number of high-fidelity evaluations',
          ylabel='Absolute error of alpha estimate')

ax1 = plt.subplot(gs[1])
# fig, ax1 = plt.subplots()
gp_model = ['ECL','LHS']

ax1.set_xlabel('GP design type')
ax1.set_ylabel('Sensitivity', color='k')
flierprops = dict(marker='d', #markerfacecolor='g',
                  linestyle='none', markeredgecolor='k')
res1 = ax1.boxplot(
    sens_stats, positions = np.arange(2)-0.25, widths=0.4,
    patch_artist=True, flierprops=flierprops
)
for element in ['boxes','whiskers', 'fliers', 'means', 'medians', 'caps']:
    plt.setp(res1[element], color='k')
for patch in res1['boxes']:
    patch.set_facecolor('white')


ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis
ax2.set_ylabel('Specificity', color='purple')
flierprops2 = dict(marker='d', #markerfacecolor='g',
                  linestyle='none', markeredgecolor='purple')
boxprops2 = dict(linestyle='--', color='k')
res2 = ax2.boxplot(
    spec_stats, positions = np.arange(2)+0.25, widths=0.4,
    patch_artist=True,flierprops=flierprops2, boxprops=boxprops2
)

for element in ['boxes', 'whiskers', 'fliers', 'means', 'medians', 'caps']:
    plt.setp(res2[element], color='purple')
for patch in res2['boxes']:
    patch.set_facecolor('white')

ax1.set_xlim([-0.55, 1.55])
ax1.set_xticks(np.arange(2))
ax1.set_xticklabels(gp_model)
ax1.tick_params(axis='y', labelcolor='k')
ax2.tick_params(axis='y', labelcolor='purple')
fig.tight_layout()  # otherwise the right y-label is slightly clipped
ax1.grid(False)
ax2.grid(False)
plt.savefig('ishigami_ecl_lhs_mc_bakeoff.pdf')
#plt.savefig('ishigami_ecl_lhs_mc_bakeoff.png', dpi=200)
