## Monte Carlo vs. MFIS Experiment performed in the paper, "Entropy-based adaptive design for contour finding and estimating reliability"
### D. Austin Cole, Robert B. Gramacy, James E. Warner, Geoffrey F. Bomarito, Patrick E. Leser, William P. Leser

## Includes:	
- **ecl_vs_lhs_mfis_bakeoff.py**: script to conduct multifidelity importance sampling (MFIS) on the Ishigami function with ECL adaptive design and Latin hypercube sample designs. Compares MFIS estimates with Monte Carlo estimates for various simulation sample budgets. Uses eclGP. Described in Section 2.1.
- **ecl_vs_lhs_mc_bakeoff_plot.py**: script to produce the line plot tracking error in failure probability estimates based on simulation sample budget and the box plot comparing sensitivity and specificty of the GPs used in MFIS.