import numpy as np

class HartmannModel:
    def __init__(self):
        self.C_ =  [1, 1.2, 3, 3.2]
 
        self.a_ = np.array([[10, .05, 3, 17],[3, 10, 3.5, 8],
                            [17, 17, 1.7, .05], [3.5, .1, 10, 10], 
                            [7, 8, 17, .1], [8, 14, 8, 14]])
        
        self.p_ = np.array([[.1312, .2329, .2348, .4047], 
                            [.1696, .4135, .1451, .8828],
                            [.5569, .8307, .3522, .8732], 
                            [.0124, .3736, .2883, .5743],
                            [.8283, .1004, .3047, .1091], 
                            [.5886, .9991, .6650, .0381]])


    def predict(self, X):
        y = np.zeros((X.shape[0],))
        for i in range(4):
            expon = np.zeros((X.shape[0],))
            for j in range(6):
                expon += self.a_[j,i]*(X[:,j] - self.p_[j,i])**2
            y += self.C_[i]*np.exp(-expon)

        return y