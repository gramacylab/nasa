# -*- coding: utf-8 -*-
"""
Utility functions

@author:    D. Austin Cole <david.a.cole@nasa.gov>
"""

import numpy as np
     
def calc_classification_stats(truth, comparator):
    """
    Calculates sensitivity and specificity classification statistics
    
    Parameters
    ----------
    truth: ndarray(n,)
        vector of true indicators (0/1), where 1=category of interest 
        (i.e. inside contour)
    comparator: ndarray(n,)
        vector of predicted indicators (0/1), where 1=category of interest 
        (i.e. finside contour)
        
    Returns
    -------
    statistics: ndarray(2,1)
        sensitivity and specificity statistics
    """
    true_positives = np.sum((truth == 1) & (comparator == 1))
    true_negatives = np.sum((truth == 0) & (comparator == 0))
    false_positives = np.sum((truth == 0) & (comparator == 1))
    false_negatives = np.sum((truth == 1) & (comparator == 0))
    
    sensitivity = true_positives/(true_positives + false_negatives)
    specificity = true_negatives/(true_negatives + false_positives)
    
    return np.array([sensitivity, specificity])
