import numpy as np

class BraninHooModel:
    def __init__(self):
        pass

    def predict(self, X):
        X0 = np.copy(X)
        
        a = 1
        # b = 5.1/(4*np.pi**2)
        b = 5/(4*np.pi**2)
        c = 5/np.pi
        r = 6
        s = 10
        t = 1/(8*np.pi)
        
        # y = a*(X0[:,1]-b*X0[:,0]**2+c*X0[:,0]-r)**2 + \
        #     s*(1-t)*np.sin(X0[:,0]) + s
        y = a*(X0[:,1]-b*X0[:,0]**2+c*X0[:,0]-r)**2 + \
            s*(1-t)*np.cos(X0[:,0]) + s
        
        return y

