# BraninHooModel class:
> Creates a "high-fidelity model" for adaptive design using the Branin-Hoo function as defined [here](https://www.sfu.ca/~ssurjano/branin.html).

 **Parameters:**   *None*

## Method:
**predict**(*X*)
 
Evaluates the Branin-Hoo function as a "high-fidelity model." Inputs are expected to be in [-5,10]x[0,15]
 
 |||
 |:----|:-------------|
 |  **Parameters:**  | **X: ndarray(n_samples, 2)**|
 ||  design inputs, one per row|
 |  **Returns:**  | **y: _ndarray(n_samples,)_** |
 || output of the function |
 
# calc_classification_stats function:
> Generates sensitivity and specificity statistics for evaluating GP's ability to classify points inside/outside the contour.

 |||
 |:----|:-------------|
 |  **Parameters:**  | **truth: ndarray(n,)**|
 ||  Vector of true indicators (0/1), where 1=category of interest  (i.e. inside contour)|
||	**comparator: ndarray(n,)**| 
 ||  vector of predicted indicators (0/1), where 1=category of interest  (i.e. finside contour)|
 |  **Returns:**  | **statistics: _ndarray(2,)_** |
 || sensitivity and specificity values |


# ContourAdaptiveDesignGP class:
> An abstract base class for a Gaussian process (GP) built via an adaptive design focusing on contour location. The GP class contains the following functions: 
 >> 1) add_observation: updates the GP matrices to include new data 
 
 >> 2) predict: use the GP predictive equations to provide output estimates 


 |||
 |:----|:-------------|
 |  **Parameters:**  | **gp: _GaussianProcessRegressor class instance_**|
 ||  a trained GP|
||	**limit_state: __int or function_**| 
 ||  New outputs, one per input|
 |  **Attributes:**  | **kernel_: _kernel instance_** |
 ||**limit_state_: _int or function_**|
 ||limit state threshold or a function of the response; if a function, the contour of interest is the zero contour of the function's output|
|| **X_: _ndarray_**|
||data inputs|
|| **y: _ndarray_**|
||data outputs|

## Methods:
 |||
 |:----|:-------------|
 |**add_observation**(*x_new, y_new, update_kernel*)| Adds one or more data samples to the GP and updates matrices. Hyperparameters can be reinitialized and optimized.|
 |**predict**(*x_pred[, return_var]*) | Evaluates the GP's predictive equations and provides output predictions.|


**add_observation**(*x_new, y_new, update_kernel=True*)
 
Adds one or more data samples to the GP and updates matrices. Hyperparameters can be reinitialized and optimized.
 
 |||
 |:----|:-------------|
 |  **Parameters:**  | **x_new: _ndarray_**|
 ||  New design inputs, one per row|
||	**y_new: _ndarray_**| 
 ||  New outputs, one per input|
|| **update_kernel: _bool, default=True_**|
|| If True, hyperparameters are re-estimated|
 |  **Returns:**  | **self: _returns an instance of self_** |

 
 **predict**(*self, x_pred, return_var=False*)
 
Evaluates the GP's predictive equations and provides output predictions.
	
|||
 |:----|:-------------|
 |  **Parameters:**  | **x_new: _ndarray_ **|
 ||  New design inputs, one per row|
||	**y_new: _ndarray, default=False_**| 
 ||  New outputs, one per input|
 |  **Returns:**  | **mean: _ndarray_ ** |
  ||  Mean of predictive distribution for set of inputs|
||	**var: _ndarray, optional_**| 
 ||  Variance of predictive distribution for inputs. Only returned when `return_var` is True.|


# eclGP class:
> A subclass of ContourAdaptiveDesignGP that uses the Entropy Contour Locator (ECL) acquisition function to conduct adaptive design

|||
 |:----|:-------------|
 |**Parameters:** | **gp:** |
 || a trained GP of class GaussianProcessRegressor||
 ||**limit_state_: _int or function_**|
 ||limit state threshold or a function of the response; if a function, the contour of interest is the zero contour of the function's output|
 | **Attributes:** | **kernel_: _kernel instance_** |
 ||**limit_state_: _int or function_**|
 ||limit state threshold or a function of the response; if a function, the contour of interest is the zero contour of the function's output|
 || **X_: _ndarray_**|
 ||data inputs|
 || **y: _ndarray_**|
 ||data outputs|

## Methods:
|||
 |:----|:-------------|
| **calc_entropy**(*x[, y_var, limit_state]*)|Calculates the entropy contour locator (ECL).|
 |**fit**(*n_steps, high_fidelity_model, bounds[, batch_size, n_cand, X_cand, fixed_cands, lookahead, local_opt]*)|Performs ECL adapative design, updating the GP after each new selection|
 |**opt_entropy**(*X_cand, bounds[, X, KX_inv, limit_state]*)| Performs gradient based optimization ('L-BFGS-B') of ECL, beginning at a set of candidate points.|
| **select_samples**(*bounds[, batch_size, n_cand, X_cand, fixed_cands, lookahead, local_opt, return_cand_preopt]*)|Selects a batch of samples using ECL adaptive design.|

**calc_entropy**(*x, y_var=None, limit_state=None*)

Calculates the entropy contour locator (ECL)

|||
 |:----|:-------------|
 |**Parameters:** | **x: _ndarray_**|
 || of input locations|
 ||**y_var: _ndarray, default=None_**|
 || predictive variance estimates for each input in *x*; `len(y_var)=`number of rows in *x*|
 ||**limit_state_: _int or function, default=None_**|
 ||limit state threshold or a function of the response; if a function, the contour of interest is the zero contour of the function's output; if None, the class attribute `self.limit_state_` is used|
|**Returns**:| **ECL: _ndarray_**|
|| ECL values, one for each row in *x*|

**fit**(*n_steps, high_fidelity_model, bounds, batch_size=1, n_cand=None, X_cand=None, fixed_cands=False, lookahead=True, local_opt=True*)

Performs ECL adapative design, updating the GP after each new selection
	
|||
 |:----|:-------------|
 |**Parameters:** |  n_steps: int, number of adaptive iterations
 ||**high_fidelity_model: _class instance_**|
 || class instance with a predict call|
 ||**bounds: _seq_**|
 ||sequence of (min, max) pairs of the design space limits for each dimension|
|| **batch_size: _int, default=1_**| 
||number of samples to select in each batch|
||**n_cand: _int, default=None_**|
||number of candidate points in each set. If *None*, then 10*# of input dimensions is used. |
 ||**X_cand: _ndarray, default=None_**|
 ||a set of candidate points used for optimization; if *None*, a Latin Hypercube design is used|
 ||**fixed_cands: _bool, default=False_** ||
 ||if True, a single candidate set is used for selecting each point||
 ||**lookahead: _bool, default=True_** ||
 ||if True, the predictive variance is updated sequentially before selecting the next batch point; if False, all batch samples are selected simultaneously||
 ||**local_opt: _bool, default=True_** ||
 ||if True, gradient-based optimization is conducted after discrete optimization to select inputs; if False, only discrete optimization is performed||
| **Returns**:| **self: _returns an instance of self_**|



**opt_entropy**(*X_cand, bounds, X=None, KX_inv=None, limit_state=None*)

Performs gradient based optimization ('L-BFGS-B') of ECL, beginning at a set of candidate points.

|||
 |:----|:-------------|
 |**Parameters:** |  **X_cand: _ndarray_**|
 ||starting locations for optimization, one per row|
 ||**bounds: _seq_**|
 ||sequence of (min, max) pairs of the optimizer's search limits for each dimension|
 || **X: _ndarray, default=None_**|
 || input locations used to calculate covariance matrix (helpful for calculated look-ahead ECL); if *None*, *self.X_* is used|
 ||**KX_inv: _ndarray, default=None_**| 
 ||the inverse of the covariance matrix for X (helpful for calculated look-ahead ECL); if *None*, the appropriate hidden class attribute is used|
 ||**limit_state_: _int or function, default=None_**|
 ||limit state threshold or a function of the response; if a function, the contour of interest is the zero contour of the function's output; if None, the class attribute `self.limit_state_` is used|
|**Returns:**| **samples: _ndarray_**| 
||selected input location, based on maximizing ECL|
|| **ECL: _ndarray_ **|
|| ECL value |

**select_samples**(*bounds, batch_size=1, n_cand=None, X_cand=None, fixed_cands=False, lookahead=True, local_opt=True, return_cand_preopt=False*)

Selects a batch of samples using ECL adaptive design.

|||
 |:----|:-------------|
 |**Parameters:** |  **X_cand: _ndarray_**|
  ||starting locations for first optimization, one per row|
 ||**bounds: _seq_**|
 ||sequence of (min, max) pairs of the design space limits for each dimension|
|| **batch_size: _int, default=1_**| 
||number of batch points to select|
||**n_cand: _int, default=None_**|
||number of candidate points in each set. If *None*, then 10*# of input dimensions is used. |
 ||**X_cand: _ndarray, default=None_**|
 ||a set of candidate points used for optimization; if *None*, a Latin Hypercube design is used|
 ||**fixed_cands: _bool, default=False_** ||
 ||if True, a single candidate set is used for selecting each point||
 ||**lookahead: _bool, default=True_** ||
 ||if True, the predictive variance is updated sequentially before selecting the next batch point; if False, all batch samples areselected simultaneously||
 ||**local_opt: _bool, default=True_** ||
 ||if True, gradient-based optimization is conducted after discrete optimization to select inputs; if False, only discrete optimization is performed||
 ||**return_cand_preopt: _bool, default=False_** ||
|| if True, the candidate points selected during discrete optimization are returned |
|**Returns:**| **samples: _ndarray_**|
|| new batch of inputs, one per row.| 
|| **selected_candidates: _ndarray, optional_**|
|| selected candidate points based on discrete optimization for each new input. Only returned when 'return_cand_preopt' is True.|


# HartmannModel class:
 > Creates a "high-fidelity model" for adaptive design using the Hartmann-6 function as defined [here](https://www.sfu.ca/~ssurjano/hart6.html).
 
 **Parameters:**  *None*


## Method:
**predict**(*X*)
 
Evaluates the Hartmann-6 function. Inputs are expected to be in [0,1]^6
 
 |||
 |:----|:-------------|
 |  **Parameters:**  | **X: ndarray(n_samples, 6)**|
 ||  design inputs, one per row|
 |  **Returns:**  | **y: _ndarray(n_samples,)_** |
 || output of the function |

# IshigamiModel class:
 > Creates a "high-fidelity model" for adaptive design using the Ishigami function as defined [here](https://www.sfu.ca/~ssurjano/ishigami.html).

|||
 |:----|:-------------|
 |  **Parameters:**  | **a_coef: _int, default=5_**|
 || *a* coefficient in equation|
 | | **b_coef: _int, default=0.1_**|
 || *b* coefficient in equation|

## Method:
**predict**(*X*)
 
Evaluates the Ishigami function. Inputs are expected to be in [-pi,pi]^3
 
 |||
 |:----|:-------------|
 |  **Parameters:**  | **X: ndarray(n_samples, 3)**|
 ||  design inputs, one per row|
 |  **Returns:**  | **y: _ndarray(n_samples,)_** |
 || output of the function |
 
 
