# -*- coding: utf-8 -*-
"""
Created on Wed Dec  9 08:12:26 2020

@author: dacole2
"""
import warnings
import numpy as np
from sklearn.gaussian_process import GaussianProcessRegressor as GPR



class ContourAdaptiveDesignGP:
    def __init__(self, gp, limit_state):
        self._initial_gp = gp
        self._alpha = gp.alpha
        self.kernel_ = gp.kernel_
        self._set_hyperparameters_from_kernel(self.kernel_)
        self.limit_state_ = limit_state
        self._K_X = self._build_covariance_matrix(gp.X_train_)
        self._Ki_X = np.linalg.inv(self._K_X)
        self.X_ = gp.X_train_
        self.y_ = np.array(gp.y_train_).reshape((-1, 1))

    def add_observation(self, x_new, y_new, update_kernel=True):
        """
        Adds new observation(s) to GP, updating relevant matrices for
        prediction.

        Parameters
        ----------
        x_new : ndarray(n,dim)
            New design location(s) (one per row).
        y_new : ndarray(n,)
            New outputs.
        update_kernel : bool, optional
            If true, the kernel is updated. The default is True.

        Returns
        -------
        None.

        """
        if update_kernel:
            self._build_new_gp(x_new, y_new)
        else:
            self._update_pred_matrices_and_data(x_new, y_new)


    def predict(self, x_pred, return_var=False):
        """
        Produces predictions from the GP's predictive equations

        Parameters
        ----------
        x_pred : ndarray(n, dim)
            A set of predictive locations, one per row.
        return_var : bool, optional
            If true, the predictive variance is also returned.
            The default is False.

        Returns
        -------
        mean: ndarray(n,)
            A set of predictive means.
        variance: ndarray(n,)
            If return_var=True, a set of predictive variances.

        """
        if x_pred.ndim == 1:
            x_pred = x_pred.reshape((1, -1))
        K_xpred_X = self.kernel_(x_pred, self.X_)
        KiY = self._Ki_X.dot(self.y_.reshape((-1, 1)))

        mean = np.dot(K_xpred_X, KiY)

        if return_var:
            var = self._calc_predictive_variance(K_xpred_X)

            return mean.flatten(), var

        return mean.flatten()


    def _build_covariance_matrix(self, X):
        K_X = self.kernel_(X)
        np.fill_diagonal(K_X, K_X.diagonal() + self._initial_gp.alpha)

        return K_X


    def _build_new_gp(self, x_new, y_new):
        new_gp = GPR(kernel=self._initial_gp.kernel, alpha=self._alpha)
        with warnings.catch_warnings():
            warnings.simplefilter("ignore")
            new_gp.fit(np.vstack((self.X_, x_new)), np.append(self.y_, y_new))

        self.kernel_ = new_gp.kernel_
        self._set_hyperparameters_from_kernel(new_gp.kernel_)
        self._K_X = self._build_covariance_matrix(new_gp.X_train_)
        self._Ki_X = np.linalg.inv(self._K_X)
        self.X_ = new_gp.X_train_
        self.y_ = np.array(new_gp.y_train_).reshape((-1, 1))


    def _calc_predictive_variance(self, K_xpred_X, KX_inv=None):
        if KX_inv is None:
            KX_inv = self._Ki_X
        Ki_K_xpred = KX_inv.dot(K_xpred_X.T)
        var = self._constant_value  +  self._noise_level - \
                (K_xpred_X * Ki_K_xpred.T).sum(-1)

        negative_var = (var <= 0)
        if np.sum(negative_var) > 0:
            warnings.warn(
                UserWarning("Predictive variances smaller than 0."
                            " Setting those variances to 0."))
        var[negative_var] = 0

        return var


    def _predict_variance_with_xnew(self, X_pred, x_new, X=None, KX_inv=None):
        if X is None:
            X = self.X_

        K_Xpred_Xn1 = self.kernel_(X_pred, np.vstack((X, x_new)))
        KX_inv = self._update_precision_matrix(x_new, X, KX_inv)

        K_n1i_K_Xpred = KX_inv.dot(K_Xpred_Xn1.T)
        # Diagonal of covariance matrix
        var = self._constant_value * (1 + self._alpha) + \
            self._noise_level - \
            (K_Xpred_Xn1 * K_n1i_K_Xpred.T).sum(-1)

        return var


    def _set_hyperparameters_from_kernel(self, kernel):
        kernel_params = kernel.get_params()
        self._lengthscale = \
            [value for key, value in kernel_params.items() \
             if 'length_scale' in key][0]
        self._constant_value = \
            [value for key, value in kernel_params.items() \
             if 'constant_value' in key][0]
        if any(key.endswith('noise_level') for key in kernel_params):
            self._noise_level = \
                [value for key, value in kernel_params.items() \
                 if 'noise_level' in key][0]
        else:
            self._noise_level = 0


    def _update_covariance_matrix(self, x_new, X):
        K_X_xnew = self.kernel_(X, x_new)
        k_xnew = self._build_covariance_matrix(x_new)
        np.fill_diagonal(k_xnew, k_xnew.diagonal() + self._initial_gp.alpha)

        K_Xn1 = np.append(self._K_X, K_X_xnew, axis=1)
        K_Xn1 = np.append(K_Xn1, np.hstack((K_X_xnew.T, k_xnew)), axis=0)

        return K_Xn1

    def _update_pred_matrices_and_data(self, x_new, y_new):
        self._K_X = self._update_covariance_matrix(x_new, self.X_)
        if x_new.shape[0] == 1:
            self._Ki_X = self._update_precision_matrix(x_new)
        else:
            self._Ki_X = np.linalg.inv(self._K_X)
        self.X_ = np.vstack((self.X_, x_new))
        self.y_ = np.append(self.y_, y_new)


    def _update_precision_matrix(self, x_new, X=None, K_inv=None):

        x_new = x_new.reshape((1, -1))
        if X is None:
            X = self.X_
        if K_inv is None:
            K_inv = self._Ki_X

        K_X_xnew = self.kernel_(X, x_new)
        Ki_K_xnew = K_inv.dot(K_X_xnew)

        sig2_xnew = self._constant_value* (1+self._alpha) + \
            self._noise_level - \
            np.dot(K_X_xnew.T, Ki_K_xnew)
        g_xnew = -1/sig2_xnew * Ki_K_xnew

        Ki1 = np.hstack((K_inv + g_xnew.dot(g_xnew.T)*sig2_xnew, g_xnew))
        Ki2 = np.hstack((g_xnew.T, 1/sig2_xnew))
        Kn1i = np.vstack((Ki1, Ki2))

        return Kn1i
