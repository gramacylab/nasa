import numpy as np

class IshigamiModel:
    def __init__(self, a_coef=5, b_coef=0.1):
        self.a_coef = a_coef
        self.b_coef = b_coef


    def predict(self, X):
        outputs = np.sin(X[:, 0]) + self.a_coef*np.sin(X[:, 1])**2 + \
            self.b_coef*X[:, 2]**4 * np.sin(X[:, 0])

        return outputs
