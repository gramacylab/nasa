# Code Repository for Entropy-based Contour Locator (ECL) Adaptive Design
# Maintainer: Austin Cole

	austin.cole8@vt.edu

## Includes:	
- **eclGP**: Python package to conduct ECL adaptive design
- **experiments**: directory with scripts to run experiments in paper