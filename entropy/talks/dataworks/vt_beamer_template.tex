\documentclass[hyperref={bookmarks=false},aspectratio=169]{beamer}
\usepackage[utf8]{inputenc}
\usepackage{amsmath, subfig}

\DeclareMathOperator*{\argmaxA}{arg\,max} 
\DeclareMathOperator*{\maxA}{max} 
% ---------------  Define theme and color scheme  -----------------
\usetheme[sidebarleft]{Caltech}  % 3 options: minimal, sidebarleft, sidebarright

%\setbeamertemplate{footline}[frame number]

% ------------  Information on the title page  --------------------
\title[Optimization of Gaussian Processes]
{\bfseries{Evaluating Optimization Criteria for a Heteroskedastic Computer Experiment}}

\subtitle{A brief overview}

\author[Austin Cole]
{Austin Cole\inst{1}}

\institute[Virginia Tech]
{
  \inst{1}
  Department of Statistics\\
  Virginia Tech
}

\date[ICUP, 2014]
{Fall 2018 Gramacy Lab Presentation Series\\November 30, 2018}
%------------------------------------------------------------
\newcommand{\cs}[1]{\texttt{\symbol{`\\}#1}}




%------------------------------------------------------------
%The next block of commands puts the table of contents at the 
%beginning of each section and highlights the current section:

\AtBeginSection[]
{
  \begin{frame}
    \frametitle{Table of Contents}
    \tableofcontents[currentsection]
  \end{frame}
}
%------------------------------------------------------------


\begin{document}

\frame{\titlepage}  % Creates title page

%---------   table of contents after title page  ------------
\begin{frame}
\frametitle{Table of Contents}
\tableofcontents
\end{frame}
%---------------------------------------------------------


\section{What is Optimization?}

%---------------------------------------------------------
%Changing visibility of the text
\begin{frame}
\frametitle{Computer Experiments overview}


\begin{itemize}
  \item Experiments of processes that are often too expensive or time consuming to carry out physically
  \item Other applications exist including technological processes
  \item They are often still relatively expensive in terms of the time to conduct them and the computational resources needed
  \item Thus, methods like sequential design are used to minimize the cost of the experiment while still achieving a specific goal


\end{itemize}



\end{frame}

%---------------------------------------------------------


%---------------------------------------------------------
%Changing visibility of the text
\begin{frame}
\frametitle{Optimization in a nutshell}

\begin{itemize}
  \item The goal is to find the global maximum of a manifold $$\argmaxA_{x\in\mathcal{X}}z(x)$$
  \item A criterion is needed to determine where to evaluate the next sample in a sequential design
  \begin{itemize}
  \item This criterion itself has to be optimized for each additional sample point
   \item Functions like {\tt optim} in {\tt R} use gradients of the criterion's function to search the space for local minima
   \item Often a number of starting points are used so that we can more often find the global minima
   \end{itemize} 
   \item We end up minimizing the criterion to select design points to attain our ultimate goal of finding the minimum/maximum of the manifold


\end{itemize}



\end{frame}

%---------------------------------------------------------

%---------------------------------------------------------
%Changing visibility of the text
\begin{frame}
\frametitle{Criterion 1: Expected Improvement}
\begin{itemize}
  \item Jones et al. (1998) introduced the concept
  \item Improvement $I(x)=max\{f_{min}-Z(x),0\}$
  \item The next design point $$x_{N+1}=\argmaxA_{x\in\mathcal{X}}\mathbf{E}[I(x)]$$
  \item Analytical solution $$\mathbf{E}[I(x)]=(f_{min}-\hat{z}_N(x))\Phi\left(\frac{f_{min}-\hat{z}_N(x)}{\hat{\sigma}_N(x)}\right)+\hat{\sigma}_N(x)\phi\left(\frac{f_{min}-\hat{z}_N(x)}{\hat{\sigma}_N(x)}\right)$$
  \item This shows that $\Phi(\cdot)$ and $\phi(\cdot)$ are weights for assessing the importance of the predicted minimum and high standard error
  \item EI assumes the process is deterministic 

\end{itemize}


\end{frame}

\begin{frame}
\frametitle{Illustration of Expected Improvement}
\begin{figure}[h!]

    \includegraphics[width=100mm]{ei.png}
\end{figure}
\end{frame}

%---------------------------------------------------------

%---------------------------------------------------------
%Changing visibility of the text
\begin{frame}
\frametitle{Criterion 2: Approximate Knowledge Gradient}

\begin{itemize}
  \item The Knowledge Gradient was first introduced by Frazier et al. (2008) originally for discrete feasible sets
  \item Knowledge Gradient: $$ \mathbf{E}[\maxA_{i=1,...,N+1}\hat{z}_{N+1}(x_i)|\mathcal{F}^N,x_{N+1}=x]-\maxA_{i=0,...,N}\hat{z}_{N+1}(x_i)|_{x_{N+1}=x}$$
  \item This uses lookahead to gauge the benefit of finding a better maximum based on a new sample's location
  \item An approximation alogirthm was developed by Scott el al. (2011) and prevents the need for use of discretization of the region

\end{itemize}



\end{frame}

%---------------------------------------------------------

%---------------------------------------------------------
%Changing visibility of the text
\begin{frame}
\frametitle{Criterion 3: Integrated Expected Conditional Improvement}

\begin{itemize}
  \item Introduced by Gramacy \& Lee (2010)
  \item Conditional Improvement: $$I(y|x_{N+1}=x)=max\{f_{min}-\hat{z}(y|x_{N+1}=x),0\}$$
  \item IECI: $$ \int_\mathcal{X}(\mathbf{E}[I(y)]-\mathbf{E}[I(y|x)])w(y)dy$$
  \item This criterion provides a lookahead for expected improvement by utilizing the variance extimation relying on the design points
  \item IECI Approximation: $$-\frac{1}{MT}\sum_{m=1}^M \sum_{t=1}^T (f_{min}-\hat{z}_N(y|x))\Phi\left(\frac{f_{min}-\hat{z}_N(y|x)}{\check{\sigma}_N(y|x)}\right)+\check{\sigma}_N(y|x)\phi\left(\frac{f_{min}-\hat{z}_N(y|x)}{\check{\sigma}_N(y|x)}\right)$$

\end{itemize}


\end{frame}
%-------------------------------------------------------
\section{Optimization of BetaDist}

\begin{frame}
\frametitle{Optimization of BetaDist}
\begin{itemize}
	\item As a part of Boya's research, she proposed using a beta distribution to generate an initial experimental design
	\item This requires knowing the hyperparameters for $\alpha$ and $\beta$
	\item An optimization scheme using Expected Improvement was implemented to optimize the RMSE for certain sample sizes (n) and dimensions (dim)
	\item We then drew contours for the predicted mean surface within 2\% of the minimum to provide hyperparameter settings
\end{itemize}

\end{frame}


\begin{frame}
\frametitle{Optimization of BetaDist using EI}
\begin{figure}[h!]
\centering
\subfloat[n = 8, dim = 2]{
  \includegraphics[width=40mm]{n8dim2_2perc.png}
}
\subfloat[n = 16, dim = 3]{
  \includegraphics[width=40mm]{n16dim3_2perc.png}
}


\end{figure}

\end{frame}
%---------------------------------------------------------


\section{Comparing Optimization Criteria}

%---------------------------------------------------------

\begin{frame}
\frametitle{1-D Toy Problem}
\begin{figure}[h!]
\centering
\subfloat[Mean curves]{
  \includegraphics[width=65mm]{1d_mean.png}
}
\subfloat[Noisy curve]{
  \includegraphics[width=65mm]{1d_with_noise.png}
}


\end{figure}


\end{frame}

%---------------------------------------------------------

\begin{frame}
\frametitle{1-D Toy Problem}
\begin{itemize}
    \item A process with a mean curve that includes two local minima over $x\in[0,1]$
    \item The process has a noise element that increases quadratically as $x$ increases
    \item There is a parameter $\alpha$ that affects the peakiness of the mean curve, with a larger effect upon the second local minima
        \begin{itemize}
            \item The global minina is determined between the two local minima by the value of $\alpha$
            \item The global minima in the process shifts from the left local minima to the right local minima around $\alpha = 1.05$
        \end{itemize} 
        
\end{itemize}


\end{frame}


%---------------------------------------------------------

\begin{frame}
\frametitle{Fitting a GP with hetGP}

\begin{itemize}
    \item {\tt hetGP} developed by Binois \& Gramacy fits a Gaussian Process with heteroscedastic noise
    \item A GP is nested and fitted to model the variance of the Gaussian Process fit of the mean
    \item It includes many more terms to estimate, so model convergence can be an issue
    \item Our hypothesis was that using {\tt hetGP} to model the noise of the process will help optimize a process with non-constant noise
\end{itemize}



\end{frame}

%---------------------------------------------------------

%---------------------------------------------------------

\begin{frame}
\frametitle{1-D Toy Problem Results}

\begin{figure}[h!]

    \includegraphics[width=120mm]{results_comparison.png}
\end{figure}


\end{frame}



\begin{frame}
\frametitle{1-D Toy Problem Results}

\begin{figure}[h!]

    \includegraphics[width=120mm]{hetgp_opt_compar.png}
\end{figure}


\end{frame}



\section{Future Work}

%---------------------------------------------------------
%Changing visivility of the text

\begin{frame}
\frametitle{2-D Toy Problem}
\begin{itemize}
	\item We next plan to consider a more challenging manifold with 2 inputs
	\item There are four local minima with two of them competing for the global minima
	\item The parameter $\alpha$ is used to manipulate the depth of the upper left trough, which ultimately decides where the global minima is
	\item The noise surface is squared Gaussian, with high noise being located near the local minima in the upper left
\end{itemize}
\end{frame}
%-------------------------------------------------------
\begin{frame}
\frametitle{Future Work}
\begin{itemize}
	\item Further evaluation of the {\tt hetGP} fit of the 2-D problem
	\item Exploring real datasets and comparing the optimization results
	\item Deriving an approximation of IECI
	\item Developing a criterion for evaluating model fits outside of the usual goal of optimization where the surface within a percentage of the global minima is correctly identified
\end{itemize}

\end{frame}

%---------------------------------------------------------


\begin{frame}
\frametitle{1-D Toy Problem Results}

This is a brief introduction of \alert{Caltech pranks}.

\begin{block}{Definition}
Prank: a practical joke or mischievous act
\end{block}

\begin{alertblock}{Axiom}
Caltech pranks are a key part of the institute's history and identity.
\end{alertblock}

\begin{examples}
See the next slide for a prank example.
\end{examples}
\end{frame}
%---------------------------------------------------------

%---------------------------------------------------------
%Two columns
\begin{frame}
\frametitle{Hollywood sign}

\begin{columns}

\column{0.45\textwidth}

\begin{figure}
    \centering
    \includegraphics[width=\columnwidth]{./figures/hollywood_caltech.jpg}
    \caption{``Hollywood is still mad about that,'' says Autumn Looijen, author of \emph{Legends of Caltech III: Techer In the Dark.} \tiny{(Photo downloaded from: http://brennen.caltech.edu/autobiography/automaster2.htm)}}
    \label{fig:hollywood_prank}
\end{figure}


\column{0.55\textwidth}
In May 1987, undergraduates from Page and Ricketts houses combined forces (and several hundred dollars) to purchase enough black and white plastic, transformed the Hollywood sign to read ``Caltech''.

\small{(Reference: http://www.admissions.caltech.edu/pranks)}

\end{columns}
\end{frame}



%---------------------------------------------------------

\end{document}