\documentclass[hyperref={bookmarks=false},aspectratio=169]{beamer}
\usepackage[utf8]{inputenc}
\usepackage{natbib}
\usepackage{amsmath, subfig}
\usepackage{dsfont}
\usepackage{algpseudocode}
\usepackage[]{algorithm}
\usepackage{algorithmicx}
\renewcommand*{\bibfont}{\small}
\newcommand{\Top}{\text{T}}
\newcommand{\x}{\mathbf{x}}
\newcommand{\X}{\mathbf{X}}
\newcommand{\Y}{\mathbf{Y}}
\newcommand{\K}{\mathbf{K}}
\newcommand{\kk}{\mathbf{k}}
\newcommand{\F}{\mathbb{F}}
\newcommand{\R}{\mathbb{R}}
\newcommand{\B}{\mathcal{B}}
\newcommand{\C}{\mathcal{C}}
\newcommand{\G}{\mathcal{G}}
\newcommand{\g}{\mathbf{g}}
\newcommand{\Q}{\mathbf{Q}}
\newcommand{\W}{\mathbf{W}}
\newcommand{\cc}{\mathbf{c}}
\newcommand{\I}{\mathbb{I}}
\newcommand{\OO}{\mathcal{O}}
\newcommand{\Sig}{\mathbf{\Sigma}}

%\setbeamertemplate{caption}{\insertcaption}
\DeclareMathOperator*{\argmaxA}{arg\,max} 
\DeclareMathOperator*{\maxA}{max} 
% ---------------  Define theme and color scheme  -----------------
\usetheme[]{VT2}
%\usetheme[sidebarleft]{VT2}  % 3 options: minimal, sidebarleft, sidebarright
\setbeamertemplate{footline}[frame number]
%\setbeamertemplate{frametitle}{\color{black}\bfseries\insertframetitle\par\vskip-6pt\hrulefill}

\setbeamertemplate{caption}[numbered]

% ------------  Information on the title page  --------------------
\title[Entropy-based Adaptive Design]
{\bfseries{Entropy-based adaptive design for \\contour finding and estimating reliability}}
\subtitle{}
\author[D. Austin Cole]
	{	D.~Austin Cole\thanks{{\tt austin.cole8@vt.edu}}$^,$\footnotemark[2]
\\
Robert B.~Gramacy\thanks{Department of Statistics, Virginia Tech, Blacksburg, VA}, James E.~Warner\thanks{NASA Langley Research Center, Hampton, VA}, Geoffrey F.~Bomarito\footnotemark[3], \\
Patrick E.~Leser\footnotemark[3], William P.~Leser\footnotemark[3]}
%{D. Austin Cole}
%\institute[Virginia Tech]
%{
  %\inst{1}
%  Department of Statistics\\
%  Virginia Tech
%}
\date[ICUP, 2014]
{\vspace{-2ex} April 13, 2021}
%------------------------------------------------------------
\newcommand{\cs}[1]{\texttt{\symbol{`\\}#1}}




%------------------------------------------------------------
%The next block of commands puts the table of contents at the 
%beginning of each section and highlights the current section:

\AtBeginSection[]
{
  \begin{frame}
    \frametitle{\textbf{Table of contents}}
    \tableofcontents[currentsection]
  \end{frame}
}
%------------------------------------------------------------


\begin{document}

\frame{\titlepage}  % Creates title page

%---------   table of contents after title page  ------------
\begin{frame}
\frametitle{\textbf{Table of contents}}
\tableofcontents
\end{frame}
%---------------------------------------------------------


\section{Background}

\begin{frame}
	\frametitle{\textbf{Estimating reliability of a next-generation spacesuit}}
	\begin{itemize}
		\item {NASA needs to assess the probability of impact failure for a new spacesuit design}
		\item {There is uncertainty in the material and projectile inputs ($X$) of the damage simulator}
%	\end{itemize}
%	\begin{equation}
%		X \sim f
%	\end{equation}
%	\begin{itemize}	
		\item {\textbf{Objective}: calculate an unbiased estimate of the probability the response is above a threshold $T$}
		$$\alpha=P(Y(x) \geq T)$$
	\end{itemize}
	\begin{figure}[h!]
		\centering
		\includegraphics[ width=40mm]{../../figures/xemu_hut.jpg}
		
	\end{figure}
\end{frame}

\begin{frame}
	\frametitle{\textbf{Existing methods to assess reliability}}
	\begin{itemize}
		\item {\textbf{Monte Carlo sampling}: draws inputs from uncertainty ($X \sim \mathbb{F}$) distribution and evaluate simulation model}
		\item {\textbf{Importance sampling (IS)}: constructs a distribution $\mathbb{F}^\star$ biased toward the failure region(s) to draw inputs}
	\end{itemize}
	\begin{equation*} \label{eq:is_estimate}
	\hat{\alpha}_{\text{IS}}=\frac{1}{M^\star}\sum_{i=1}
	^{M^\star}\mathds{1}_{\{Y(\x_i^\star)>T\}}w(\x_i^\star) \quad \mbox{via weights} \quad
	w(\x^\star)=\frac{f(\x^\star)}{f_\star(\x^\star)}
\end{equation*}
	\begin{itemize}
		\item {\textbf{Multifidelity importance sampling (MFIS)}: uses a surrogate model $\mathcal{S}_N$, like a Gaussian process (GP), to construct the bias distribution $\mathbb{F}^\star$}
		\item {\textbf{Adaptive designs} for contour finding: sequentially select design points to estimate failure boundary $\{x:Y(x)=T\}$}
	\end{itemize}
\end{frame}


%\begin{frame}
%	\frametitle{Improving reliability estimation}
%	\begin{itemize}
%		\item {Provide an \textbf{unbiased} reliability estimate that incorporates knowledge of all failure regions}
%		\item{Combine an adaptive design focused on level set estimation with MFIS}
%		\item {\cite{peherstorfer2016multifidelity} and \cite{picheny2010adaptive} are working in this area, but acquisition functions often:}
%		\begin{itemize}
%			\item {Are computationally expensive, especially for high dimensions and small-volume level sets}
%			\item{Fail to explore the space to identify new failure regions}
%		\end{itemize}
%	\end{itemize}
%\end{frame}




%-------------------------------------------------------
%\begin{frame}
%	\frametitle{Importance Sampling (IS)}	
%	\begin{equation} \label{eq:monte_carlo}
%		\hat{\alpha}_{\text{MC}}= \frac{1}{M}\sum_{i=1}^M \mathds{1}_{\{g(\mathcal{T}(\x_i))>0\}}
%	\end{equation}
%	\begin{equation} \label{eq:is_estimate}
%		\hat{\alpha}_{\text{IS}}=P^{\text{IS}}_s(\x_1^\star,\dots,\x^\star_{M^\star})=\frac{1}{M^\star}\sum_{i=1}
%		^{M^\star}\mathds{1}_{\{g(\mathcal{T}(\x_i^\star))>0\}}w(\x_i^\star) \quad \mbox{via weights} \quad
%		w(\x^\star)=\frac{f(\x^\star)}{f_\star(\x^\star)}
%	\end{equation}	
%\end{frame}

%\begin{frame}
%	\frametitle{MFIS: Building a biasing distribution}
%	\begin{figure}[h!]
%		\centering
%		\includegraphics[trim={0 2.5cm 0 1.8cm}, clip, width=110mm]{failure_region_ex.pdf}
%		\includegraphics[trim={0 0 0 1.8cm}, clip, width=110mm]{densities.pdf}
%	\end{figure}
%\end{frame}

%-------------------------------------------------------

\begin{frame}
	\frametitle{\textbf{Gaussian process surrogate}}
	\begin{itemize}
		\item Assume the response $Y_N$ follows a multivariate normal distribution, $Y_N\sim \mathcal{N}_N(0, \Sigma_N)$, where $\Sigma_N=\nu K_N$
		\item Includes hyperparameters: $\psi =\{\nu (\text{scale}), \theta (\text{lengthscale})\}$
		\item The correlation matrix $K_N$ is defined as a function $k_{\theta}(x_i,x_j)$ of the squared Euclidean distance between inputs $x_i,x_j$			
		\item{Conditional predictive equations:}
$$\mu_N(x|X_N,Y_N,\psi) = \Sigma(x,X_N)\Sigma_N^{-1}Y_N$$
$$\sigma^2_N(x|X_N,\psi) = \Sigma(x,x)-\Sigma(x,X_N)\Sigma_N^{-1}\Sigma(x,X_N)^\top$$
	\end{itemize}
\end{frame}


%---------------------------------------------------------
\begin{frame}
	\frametitle{\textbf{Adaptive Design for Contour Location}}

		\begin{columns}
			\begin{column}{.6\textwidth}
	\begin{figure}
	\includegraphics[trim=10 10 10 10, clip, width=\textwidth]{../../adaptive_design_flowchart.pdf}
	
	\end{figure}
\end{column}
\begin{column}{0.4\textwidth}  %%<--- here
Existing acquisition functions based upon:
\begin{itemize}
	\item {Predictive variance \citep{picheny2010adaptive}} 
	\item {Expected Improvement \citep{ranjan2008sequential}}
	\item {Entropy \citep{marques2018contour}:}
\end{itemize}
\qquad $-\sum_{i=1}^k \mathbb{P}(w_i)\log \mathbb{P}(w_i)$,\\
\qquad with events $w_i$ and \\ \qquad probability mass $\mathbb{P}(w_i)$.
	\end{column}
\end{columns}


\end{frame}


%---------------------------------------------------------


\begin{frame}
	\frametitle{\textbf{Adaptive Design + MFIS}}
	\begin{figure}
		\includegraphics[trim=10 10 10 10, clip, width=\textwidth]{../../adaptive_design_mfis_flowchart_talk.pdf}
		%\caption{Full process combining adaptive design with MFIS.}
	\end{figure}
	
\end{frame}

%------------------------------------------------------------
\section{Entropy-based Adaptive Design}

\begin{frame}
	\frametitle{\textbf{Entropy-based Contour Location}}
	\begin{itemize}
		\item We define the \textbf{Entropy-based Contour Locator (ECL)} using the entropy equation with
		\begin{itemize}
			\item $w_1$ representing a failure (i.e. $y>T$)
			\item $w_2$ representing a lack of failure (i.e. $y\leq T$)
		\end{itemize}
		\item Using a GP $\mathcal{S}_N$'s predictive equations to calculate $\mathbb{P}(w_i)$,
	\end{itemize}
\begin{align}
	\mathrm{ECL}(\x\mid\mathcal{S}_N,T)=  \label{eq:our_entropy} 
	&-\left(1-\Phi\left(\frac{\mu_N(\x)-T}{\sigma_N(\x)}\right)\right)\log\left(1-\Phi\left(\frac{\mu_N(\x)-T}{\sigma_N(\x)}\right)\right) \nonumber \\
	&\qquad \qquad -\Phi\left(\frac{\mu_N(\x)-T}{\sigma_N(\x)}\right)\log\left(\Phi\left(\frac{\mu_N(\x)-T}{\sigma_N(\x)}\right)\right) \nonumber 
\end{align}
\begin{itemize}
	\item Design can be done in batches (ECL.b) by updating $\sigma_N(x)$ with other batch design locations
\end{itemize}

\end{frame}

%---------------------------------------------------------------
\begin{frame}
	\frametitle{\textbf{GP and ECL surfaces}}
	\begin{figure}[h!]
		\centering
		\includegraphics[trim=5 5 12 5,clip, width=.75\textwidth]{../../figures/mm_seq_ent_gp_pred_talk.pdf}
	\end{figure}
\end{frame}



\begin{frame}
	\frametitle{\textbf{Optimization of ECL}}
	%\begin{algorithm} [H]\caption{Entropy Optimization} 
	%\begin{algorithmic}[1] 
	%	\Procedure
	%	{Entropy.Opt}{$N_c, \mathcal{S}_N, \mathcal{X}$}
	%	\State $\bar{X}_{N_c} \leftarrow$ LHS of size $N_c$ in $\mathcal{X}$ \hfill \Comment{Candidate set}
	%	\State $\tilde{x} \leftarrow \mathrm{argmax}_{\bar{x}\in \bar{X}_{N_c}} \; \mathrm{ECL}(\bar{x} \mid \mathcal{S}_N)$ \hfill \Comment{Discrete search}
	%	\State $x_{N+1} \leftarrow \mathrm{argmax}_{x \in \mathcal{B}(\tilde{x})} \; \mathrm{ECL}(\x \mid \mathcal{S}_N)$ \hfill \Comment{Continuous, local, from $\tilde{x}$}
	%	\State \Return $x_{N+1}$
	%	\EndProcedure 
	%\end{algorithmic}
	%\end{algorithm}
	\begin{itemize}
		\item ECL surface contains a ridge of global maxima on the GP's predicted contour
		\item Other local maxima exist that are worth exploring through sample points
		\item \textbf{Goal}: promote exploration of global and local maxima through a small, variable set of candidate points
		\item \textbf{Steps}:
		\begin{enumerate}
			\item Evaluate ECL for a candidate set of a 10*(\# of dimensions) Latin hypercube sample
			\item Select the candidate point with the largest ECL
			\item Use gradient-based optimization (L-BFGS-B) to maximize ECL, with starting location from step 2
		\end{enumerate}
	\end{itemize}

\end{frame}


\begin{frame}
	\frametitle{\textbf{ECL 2D surface}}
	\begin{figure}[h!]
		\centering
		\includegraphics[trim=0 0 0 35,clip, width=\textwidth]{../../figures/mm_seq_ent_surface_1_nocands.pdf}
	\end{figure}
\end{frame}


\begin{frame}
	\frametitle{\textbf{ECL 2D surface: selecting the 1st point}}
	\begin{figure}[h!]
		\centering
		\includegraphics[trim=0 0 0 35,clip, width=\textwidth]{../../figures/mm_seq_ent_surface_1_withcands.pdf}
	\end{figure}
\end{frame}


\begin{frame}
	\frametitle{\textbf{ECL 2D surface: selecting the 1st point}}
	\begin{figure}[h!]
		\centering
		\includegraphics[trim=0 0 0 35,clip, width=\textwidth]{../../figures/mm_seq_ent_surface_1.pdf}
	\end{figure}
\end{frame}



\begin{frame}
	\frametitle{\textbf{ECL 2D surface: selecting the 2nd point}}
	\begin{figure}[h!]
		\centering
		\includegraphics[trim=0 0 0 35,clip, width=\textwidth]{../../figures/mm_seq_ent_surface_2.pdf}
	\end{figure}
\end{frame}

\begin{frame}
	\frametitle{\textbf{ECL 2D surface: selecting the 3rd point}}
	\begin{figure}[h!]
		\centering
		\includegraphics[trim=0 0 0 35,clip, width=\textwidth]{../../figures/mm_seq_ent_surface_3.pdf}
	\end{figure}
\end{frame}

\begin{frame}
	\frametitle{\textbf{ECL 2D surface: selecting the 7th point}}
	\begin{figure}[h!]
		\centering
		\includegraphics[trim=0 0 0 35,clip, width=\textwidth]{../../figures/mm_seq_ent_surface_7.pdf}
	\end{figure}
\end{frame}

\begin{frame}
	\frametitle{\textbf{ECL 2D surface: selecting the 10th point}}
	\begin{figure}[h!]
		\centering
		\includegraphics[trim=0 0 0 35,clip, width=\textwidth]{../../figures/mm_seq_ent_surface_10.pdf}
	\end{figure}
\end{frame}




\section{Results}

%---------------------------------------------------------------
\begin{frame}
	\frametitle{\textbf{Synthetic Function Experiments}}
%\textbf{Synthetic Functions}
\begin{table}[ht!]
	\centering
	\begin{tabular}{l|r|r} 
		%\hline
		 & \textbf{Ishigami} & \textbf{Hartmann-6} \\ 
		\hline
		Number of dimensions & 3 & 6 \\ \hline
		Failure threshold ($T$) & 10.244 & 2.63\\ \hline
		Number of failure regions & 6 & 2\\ \hline
		Quantile & 0.9999 & 0.9989\\ \hline \hline
		Initial design size & 30 & 60 \\ \hline
		Number of sequential points & 170 & 440 \\ \hline \hline
		Failure probability ($\alpha$) & $1.9\times10^{-4}$ & $1\times10^{-5}$ \\ \hline
		Number of MFIS samples & 800 & 500
	\end{tabular}
	\label{fig:timings}
\end{table}
	
\end{frame}

\begin{frame}
	\frametitle{\textbf{Ishigami sensitivity}}
	\begin{figure}[h!]
		\centering
		\includegraphics[trim=0 5 10 5, clip,width=.83\textwidth]{../../figures/Ish_sens_results.pdf}
\includegraphics[trim=260 450 15 44, clip,width=.15\textwidth]{../../figures/methods_vertical_legend.pdf}
	\end{figure}
\end{frame}

\begin{frame}
	\frametitle{\textbf{Ishigami volume estimate error}}
	\begin{figure}[h!]
		\centering
		\includegraphics[trim=0 5 10 5, clip,width=.83\textwidth]{../../figures/Ish_re_results.pdf}
		\includegraphics[trim=260 450 15 44, clip,width=.15\textwidth]{../../figures/methods_vertical_legend.pdf}
	\end{figure}
\end{frame}

\begin{frame}
	\frametitle{\textbf{Hartmann-6 sensitivity}}
	\begin{figure}[h!]
		\centering
		\includegraphics[trim=0 5 10 5, clip,width=.83\textwidth]{../../figures/Hart_sens_results.pdf}
		\includegraphics[trim=260 450 15 44, clip,width=.15\textwidth]{../../figures/methods_vertical_legend.pdf}
	\end{figure}
\end{frame}

\begin{frame}
	\frametitle{\textbf{Hartmann-6 volume estimate error}}
	\begin{figure}[h!]
		\centering
		\includegraphics[trim=0 5 10 5, clip,width=.83\textwidth]{../../figures/Hart_re_results.pdf}
		\includegraphics[trim=260 450 15 44, clip,width=.15\textwidth]{../../figures/methods_vertical_legend.pdf}
	\end{figure}
\end{frame}

\begin{frame}
	\frametitle{\textbf{Computation Time}}
	\begin{table}[ht!]
		\centering
		\begin{tabular}{l|r|r} 
			\hline
			Method & Ishigami & Hartmann-6 \\ \hline
			\textbf{ECL} & \textbf{0.10} & \textbf{4.40}  \\ \hline
			\textbf{ECL.b} & \textbf{0.08} & \textbf{0.78}  \\ \hline
			CLoVER & 17.8 & 428.4  \\ \hline
			EGRA & 3.9 & 66.0  \\ \hline
			Ranjan & 4.5 & 59.2  \\ \hline
			SUR & 3.5 & 109.1  \\ \hline
			tIMSE & 3.4 & 11.7  \\ \hline
			tMSE & 3.9 & 59.5  \\			
		\end{tabular}
		\caption{Average computation times (minutes) to select points across 30 Monte Carlo repetitions.}
		\label{fig:timings}
	\end{table}
\end{frame}



\begin{frame}
	\frametitle{\textbf{MFIS estimates for Ishigami and Hartmann-6}}
	\begin{figure}[h!]
		\centering
		\includegraphics[trim=0 0 40 13, clip,width=\textwidth]{../../figures/mfis_estimates.png}
	\end{figure}
\end{frame}


\begin{frame}
	\frametitle{\textbf{Spacesuit Simulator: samples for estimation}}
	\begin{itemize}
		\item MFIS estimates: 200 sample design used to train GP + 250 samples from bias distribution
		\item Monte Carlo: 2500 samples from input distribution
	\end{itemize}
	\textbf{Samples used to estimate failure probability:}
	\begin{figure}[h]
		\centering
		%\includegraphics[trim=0 0 0 0 , clip,width=\textwidth]{../../figures/z2_input_output_densities.png}
		\includegraphics[trim=0 0 0 0 , clip,width=\textwidth]{../../figures/z2_samples.pdf}
		\label{fig:z2_densities}
	\end{figure}
	
\end{frame}


\begin{frame}
	\frametitle{\textbf{Spacesuit Simulator: failure probability estimates}}
	\begin{figure}[h]
	\centering
	\includegraphics[trim=0 0 0 10 , clip,width=.9\textwidth]{../../figures/z2_est_errorbar.png}
	\end{figure}
	
\end{frame}

\begin{frame}
	\frametitle{\textbf{Summary}}
	\begin{itemize}
		\item Using a GP surrogate with MFIS provides a cheaper approach to failure probability estimation for expensive simulators
		\item Pairing an adaptively designed GP improves MFIS estimation by producing a more accurate bias distribution
		\item Using ECL with a simple optimization strategy balances exploration (somewhat randomly) with exploitation to drive down contour uncertainty
		\item ECL adaptive design can be 20-100 times faster than existing adaptive designs
		\item ECL with batch selection is faster than ECL, without sacrificing accuracy
		
	\end{itemize}
		
\end{frame}
%\begin{frame}
%	\frametitle{Weighted post-hoc Expected Improvement}
%	\begin{enumerate}
%		\item Create set of $m_0$ multi-start points for optimization\\
%		\item For each multi-start location $\breve{X}_i$ optimize Expected Improvement:	
%		\begin{itemize}
%		\item $\dot{X}_i \leftarrow \max_x\text{EI(x)}$
%		\item $\text{ei}_i \leftarrow \text{EI}(\dot{X}_i)$
%		\end{itemize}
%		\item Select $\dot{x}_j where \min_x |\mu(x) - T|$
%	\end{enumerate}	
%\end{frame}

%---------------------------------------------------------
\begin{frame}[allowframebreaks]
	\frametitle{Bibliography}
	\bibliographystyle{jasa}
	\bibliography{entropy_mfis_articles}
\end{frame}

\end{document}