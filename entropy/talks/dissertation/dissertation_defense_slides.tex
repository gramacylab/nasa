\documentclass[hyperref={bookmarks=false},aspectratio=169]{beamer}
\usepackage[utf8]{inputenc}
\usepackage{natbib}
\usepackage{amsmath, subfig}
\usepackage{dsfont}
\usepackage{algpseudocode}
\usepackage[]{algorithm}
\usepackage{algorithmicx}
\renewcommand*{\bibfont}{\small}
\newcommand{\Top}{\text{T}}
\newcommand{\x}{\mathbf{x}}
\newcommand{\X}{\mathbf{X}}
\newcommand{\Y}{\mathbf{Y}}
\newcommand{\K}{\mathbf{K}}
\newcommand{\kk}{\mathbf{k}}
\newcommand{\F}{\mathbb{F}}
\newcommand{\R}{\mathbb{R}}
\newcommand{\B}{\mathcal{B}}
\newcommand{\C}{\mathcal{C}}
\newcommand{\G}{\mathcal{G}}
\newcommand{\g}{\mathbf{g}}
\newcommand{\Q}{\mathbf{Q}}
\newcommand{\W}{\mathbf{W}}
\newcommand{\cc}{\mathbf{c}}
\newcommand{\I}{\mathbb{I}}
\newcommand{\OO}{\mathcal{O}}
\newcommand{\Sig}{\mathbf{\Sigma}}

\setlength{\textfloatsep}{10pt plus 1.0pt minus 2.0pt}
%\setbeamertemplate{caption}{\insertcaption}
\DeclareMathOperator*{\argmaxA}{arg\,max} 
\DeclareMathOperator*{\maxA}{max} 
% ---------------  Define theme and color scheme  -----------------
\usetheme[]{VT2}
%\usetheme[sidebarleft]{VT2}  % 3 options: minimal, sidebarleft, sidebarright
\setbeamertemplate{footline}[frame number]
%\setbeamertemplate{frametitle}{\color{black}\bfseries\insertframetitle\par\vskip-6pt\hrulefill}

\setbeamertemplate{caption}[numbered]

% ------------  Information on the title page  --------------------
\title[Efficient Computer Experiments]
{\bfseries{Efficient computer experiment designs for Gaussian process surrogates}}
\subtitle{}
\author[D. Austin Cole]
{D. Austin Cole}
\institute[Virginia Tech]
{
	%\inst{1}
	Department of Statistics\\
	Virginia Tech
}

\date[ICUP, 2014]
{Dissertation Final Defense\\May 24, 2021}
%------------------------------------------------------------
\newcommand{\cs}[1]{\texttt{\symbol{`\\}#1}}




%------------------------------------------------------------
%The next block of commands puts the table of contents at the 
%beginning of each section and highlights the current section:

\AtBeginSection[]
{
  \begin{frame}
    \frametitle{\textbf{Table of contents}}
    \tableofcontents[currentsection]
  \end{frame}
}
%------------------------------------------------------------


\begin{document}

\frame{\titlepage}  % Creates title page

%---------   table of contents after title page  ------------
\begin{frame}
\frametitle{\textbf{Table of contents}}
\tableofcontents
\end{frame}
%---------------------------------------------------------

\section{Gaussian Processes (GPs)}
%---------------------------------------------------------
%\begin{frame}
%\frametitle{Metabolism Simulation}
%\begin{figure}[h!]
%\centering
%\includegraphics[trim={0 0 0 0}, clip, width=85mm]{chem_simulation.jpg}

%\end{figure}
%\end{frame}




%---------------------------------------------------------
\begin{frame}
	\frametitle{\textbf{Computer simulation models}}
	\begin{itemize}
		\item Computer programs that mathematically models a physical process by incorporating domain knowledge
		\item Often desired over physical experiments due to cost and time savings
		\item Applications span many different fields:
	\end{itemize}
\vspace{-.3cm} \hspace{1.8cm} \textbf{Engineering} \hspace{1.6cm} \textbf{Science} \hspace{1.6cm} \textbf{Public Health} \vspace{-.5cm}
	\begin{figure}[h!]
		\centering
		\includegraphics[trim={0 0 0 0}, clip, width=35mm]{extra_figures/hubble.jpg}
		\includegraphics[trim={0 0 0 0}, clip, width=35mm]{extra_figures/chemistry.png}
		\includegraphics[trim={0 0 0 0}, clip, width=35mm]{extra_figures/disease_spread.jpg}
		
	\end{figure}
\end{frame}

%---------------------------------------------------------
\begin{frame}
	\frametitle{\textbf{Surrogate modeling--a worthy substitute}}
	\begin{itemize}
		\item Includes enough flexibility to incorporate the underlying science from the simulator
		\item Provides accurate predictions with uncertainty quantification
		\item Generates predictions \textbf{much quicker than the simulator} produces data
	\end{itemize}
	\begin{figure}[h!]
		\centering
		\includegraphics[trim={0 0 0 0}, clip, width=65mm]{extra_figures/waiting.jpg}
		
	\end{figure}
\end{frame}

%---------------------------------------------------------
\begin{frame}
	\frametitle{\textbf{Gaussian processes (GPs)}}
	\begin{itemize}
		\item Assume the response $Y_N$ follows a multivariate Normal distribution:\\
		 \hspace{2cm} $Y_N\sim \mathcal{N}_N(\mu, \Sigma_N)$, where $\Sigma_N=\tau^2(K_N+g\mathbb{I}_N)$
		\item A common simplification assumes $\mu=0$, offloading the relationship in the data to $\Sigma_N$
		\item Includes hyperparameters: $\psi =\{\tau^2\text{ (scale)}, g \text{ (nugget)}, \text{and } \theta \text{ (lengthscale)}\}$
		\item The correlation matrix $K_N$ is defined as a function of the squared Euclidean distance between pairs of inputs, e.g.
		$$k(\x_i,\x_j)=\text{exp}\Bigg\{-\frac{||\x_i-\x_j||^2}{\theta} \Bigg\}$$	
	\end{itemize}
\end{frame}

%---------------------------------------------------------
\begin{frame}
	\frametitle{\textbf{GP predictions}}
	\begin{itemize}
		\item{Conditional predictive equations:}
		$$\mu(\x'|\X_N,Y_N,\psi) = \Sigma(\x',\X_N)\Sigma_N^{-1}Y_N$$
		$$\sigma^2(\x'|\X_N,\psi) = \Sigma(\x',\x')-\Sigma(\x',\X_N)\Sigma_N^{-1}\Sigma(\x',\X_N)^\top$$
		\item{Interpolates well, providing accurate out-of-sample predictions}
		\item{Conditioning on hyperparameters, $\sigma^2(\x')$ doesn't depend on $Y_N$}
	\end{itemize}
\end{frame}


%---------------------------------------------------------
\begin{frame}
	\frametitle{\textbf{GP fit to sine curve}}
	\begin{figure}[h!]
		\centering
		\includegraphics[trim={0 0 0 2cm}, clip, width=95mm]{extra_figures/gp_fit_sine_curve.pdf}
	\end{figure}
\end{frame}





\begin{frame}
	\frametitle{\textbf{GP adaptive design for computer experiments}}
	\begin{figure}
	\includegraphics[trim=10 10 10 10, clip, width=.8\textwidth]{../../figures/adaptive_design_flowchart.pdf}
	
\end{figure}
\end{frame}

\begin{frame}
	\frametitle{\textbf{Adaptive design acquisition functions}}
		\begin{itemize}
			\item {\textbf{Integrated mean-squared error (IMSE)}: reduces global variance} 
		\end{itemize}
		$$\text{IMSE}(X_N)=\int_{\x\in\mathcal{X}}\frac{{\sigma}^2_{N}(\x)}{\tau^2}d\x$$
		\begin{itemize}
			\item {\textbf{Entropy}: maximizes information gain about events}
		\end{itemize}
		$$\text{Entropy}=-\sum_{i=1}^k \mathbb{P}(w_i|\X_N)\log \mathbb{P}(w_i|\X_N),$$\\
		\qquad with events $w_i$ and probability mass $\mathbb{P}(w_i|\X_N)$.

\end{frame}

%-------------------------------------------------------
\begin{frame}
	\frametitle{\textbf{Limitations of GPs}}
	
	
	\begin{itemize}
		\item The standard GP framework computationally relies heavily on the inversion of the covariance, $\Sigma_N$, an $N\times N$ matrix
		\item Without special structure in $\Sigma_N$, the computation is $O(N^3)$, which limits modern computers to data sets in the 1000s
	\end{itemize}
	
	\begin{figure}[h!]
		
		\includegraphics[trim={0 0 0 0}, width=70mm]{extra_figures/too_heavy_lifting2.jpg}
	\end{figure}
	
\end{frame}

%---------------------------------------------------------



%-------------------------------------------------------
\section{Locally Induced GPs (LIGP)}
%\subsection{Inducing points}
\begin{frame}
	\frametitle{\textbf{Inducing points formulation}}
	\begin{itemize}
		\item Let $\bar{X}_M$ be \textbf{inducing points}, a set of $M\times d$ ($M \ll N$) latent variables that represent design locations
		\item Using inducing points, we can creates a low rank covariance matrix with
		%$$K_N \approx \bar{K} = k_{NM}K_M^{-1}k_{MN}$$
		$$K_N \approx\bar{K} = k_{NM}K_M^{-1}k_{MN}+\text{Diag}(K_N -k_{NM}K_M^{-1}k_{MN})$$
		\item Woodbury identities allow for more computationally efficient calculations of $\bar{K}^{-1}$ and $|\bar{K}|$
		\item Reduces the computational burden to $O(NM^2)$
		%\item We use this approximation, introduced by \cite{Snelson2006}: 
	\end{itemize}
	
	
\end{frame}
%-------------------------------------------------------



%---------------------------------------------------------
\begin{frame}
	\frametitle{\textbf{Local approximate GPs (LAGP)}}

			\begin{columns}

		\begin{column}{0.5\textwidth}  %%<--- here
			\begin{itemize}
		\item Introduced by \cite{Gramacy2015}
\item Fits a distinct GP for each predictive location
\item Uses a \textbf{subset of the data} for each local neighborhood around a predictive location
%\begin{itemize}
%	\item Nearest Neighbor (NN)
%	\item Active Learning Cohn (ALC)
%\end{itemize}
\item Produces accurate mean predictions
\item Local scope limits uncertainty approximation
			\end{itemize}
		\end{column}
			\begin{column}{.5\textwidth}
		\begin{figure}
			\includegraphics[trim=0 10 15 15, clip, width=\textwidth]{extra_figures/lagp_neighborhood.pdf}
			
		\end{figure}
	\end{column}
	\end{columns}
	
\end{frame}
%--------------------------------------------------------
%\section{Locally Induced GPs (LIGP)}


\begin{frame}
	\frametitle{\textbf{LIGP framework}}
	
	\begin{enumerate}
		\item \textbf{Initialize}: neighborhood size (n), number of inducing points (m), and hyperparameters ($\theta$, $g$)\\
		For each predictive location $\x'$:	
		\item Build a \textbf{local neighborhood} of $\mathbf{n}$ points nearest to $\x'$
		\item Create an \textbf{inducing point design} centered at $\x'$
		\begin{itemize}
			\item Select points with weighted integrated mean-squared error
			\item Scale a space-filling design
		\end{itemize}
		\item Optimize hyperparameters
	\end{enumerate}
	
\end{frame}



\begin{frame}
	\frametitle{\textbf{Weighted integrated mean-squared error (wIMSE)}}
	\begin{itemize}
		\item For a given predictive location $\x'$, integrates the weighted predictive variance given a new proposed inducing point over the design space:
		$$\int_{\tilde{\x}\in \mathcal{X}}k_\theta(\tilde{\x},\x')\frac{\sigma_{m+1,n}^2(\tilde{\x})}{\tau^2} \; d\tilde{\x} $$
		
		\item By using a Gaussian kernel for the weight (centered at $\x'$), we can express wIMSE in closed form \citep{hetGP2}:
		$$\frac{\sqrt{\theta\pi}}{2}\prod_{k=1}^d\left(\text{erf}\left\{\frac{x'_k-a_k}{\sqrt{\theta}}\right\}-\text{erf}\left\{\frac{x'_k-b_k}{\sqrt{\theta}}\right\}\right)-\text{tr}\Big\{\left(K^{-1}_{m+1}-Q^{-1(n)}_{m+1}\right)W'_{m+1}\Big\}$$
		
	\end{itemize}
	
\end{frame}
%---------------------------------------------------------
\begin{frame}
	\frametitle{\textbf{wIMSE-based inducing point design}}
 \hspace{27mm}	wIMSE surface \hspace{12mm} Order of inducing points selection \vspace{-0.5cm}
	\begin{figure}[h!]
		\centering

		\includegraphics[trim={0 0 0.5cm 1.7cm}, clip, height=55mm]{extra_figures/wimse_seq_ip10.pdf}
		\includegraphics[trim={1.8cm 0 0 1.7cm}, clip, height=55mm]{extra_figures/ip_wimse_order.pdf}
	\end{figure}
	
\end{frame}




%---------------------------------------------------------

\begin{frame}
	\frametitle{\textbf{Borehole (8d) experiment}}
	
	\begin{minipage}{70mm}
		\centering
		\includegraphics[trim=0 0 0 0, clip, width=68mm]{extra_figures/borehole_mc_boxplot.png}
	\end{minipage}
	\begin{minipage}{30mm}
		\begin{tabular}{|c | c| c |} 
			\hline
			GP Method & n & Minutes\footnotemark \\ 
			\hline
			\textcolor{red}{LAGP(ALC)} & \textcolor{red}{50} & \textcolor{red}{0.95} \\
			\textcolor{red}{LAGP(NN)} & \textcolor{red}{50} & \textcolor{red}{0.08}  \\ 
			LAGP(NN) & 150 & 1.25  \\ 
			LIGP(wIMSE) & 150 & 3.04   \\
			LIGP(cHR) & 150 & 0.71 \\
			LIGP(qNorm) & 150 & 0.72 \\
			\hline
		\end{tabular}
		
	\end{minipage}
	\footnotetext[1]{LAGP is coded in {\tt C}, while LIGP is coded in {\tt R}}
\end{frame}

%---------------------------------------------------------



\begin{frame}
	\frametitle{\textbf{Building a local neighborhood with replicates}}
		\begin{figure}[h!]
		\centering
		\includegraphics[trim=0 0 0 50, clip, height=70mm]{extra_figures/lagp_vs_ligp_neighborhoods_talk.pdf}
		
	\end{figure}
\end{frame}


\begin{frame}
	\frametitle{\textbf{Ocean oxygen concentration (4d) experiment}}
	\begin{figure}[h!]
		\centering
		\includegraphics[trim=0 0 10 50, clip, height=58mm]{extra_figures/fksim_rmse_vs_time.pdf}
		\includegraphics[trim=0 0 10 50, clip, height=58mm]{extra_figures/fksim_logscore_vs_time.pdf}
		
	\end{figure}
\end{frame}
%-------------------------------------------------------
\begin{frame}
	\frametitle{\textbf{LIGP Summary}}
	\begin{itemize}
		\item LIGP marrying local GP approximations with inducing points, opening the door for \textbf{larger local neighborhoods and faster computation}
		\item When the data includes replicates, building neighborhoods based on the number of \textbf{unique points} allows more data to be included
		\item Efficient inducing point designs space-fill around the predictive location $\x'$ (i.e. the neighborhood's center), allowing us to use \textbf{template schemes}
		\item LIGP can be \textbf{faster} than LAGP/HetGP and often provide \textbf{more accurate mean predictions and noise estimates}
	\end{itemize}
\end{frame}


\section{Entropy-based Adaptive Design}


%\subsection[]{Background}

\begin{frame}
	\frametitle{\textbf{Estimating reliability of a next-generation spacesuit}}
	\begin{itemize}
		\item {NASA needs to assess the probability of impact failure for a new spacesuit design}
		\item {There is uncertainty in the material and projectile inputs ($X$) of the damage simulator}
%	\end{itemize}
%	\begin{equation}
%		X \sim f
%	\end{equation}
%	\begin{itemize}	
		\item {\textbf{Objective}: calculate an unbiased estimate of the probability the response is above a threshold $T$}
		$$\alpha=P(Y(\x) > T)$$
	\end{itemize}
	\begin{figure}[h!]
		\centering
		\includegraphics[ width=40mm]{../../figures/xemu_hut.jpg}
		
	\end{figure}
\end{frame}

\begin{frame}
	\frametitle{\textbf{Motivations when estimating reliability}}
	\begin{itemize}
		\item {Handle a high number of dimensions}
		\item {Identify multiple failure regions (if they exist)}
		\item {Estimate contours corresponding to extremely high/low quantiles}
		\item {Be conservative in the prediction of failure locations}
		\item {Tap into parallel-processing through batch selection}
	\end{itemize}
\vspace{-.3cm}
\begin{figure}[h!]
	\centering
	\includegraphics[trim={0 0 0 1}, clip, height=40mm]{../../figures/assembly_model.png}
\end{figure}
\end{frame}


\begin{frame}
	\frametitle{\textbf{Existing methods to assess reliability}}
	\begin{itemize}
		\item {\textbf{Monte Carlo sampling}: draws inputs from uncertainty ($X \sim \mathbb{F}$) distribution and evaluate simulation model}
		\item {\textbf{Importance sampling (IS)}: constructs a distribution $\mathbb{F}^\star$ biased toward the failure region(s) to draw inputs}
	\end{itemize}
	\begin{equation*} \label{eq:is_estimate}
	\hat{\alpha}_{\text{IS}}=\frac{1}{\grave{N}}\sum_{i=1}
	^{\grave{N}}\mathds{1}_{\{Y(\x_i^\star)>T\}}w(\x_i^\star) \quad \mbox{via weights} \quad
	w(x^\star)=\frac{f(\x^\star)}{f_\star(\x^\star)}
\end{equation*}
	\begin{itemize}
		\item {\textbf{Multifidelity importance sampling (MFIS)}: uses a surrogate model $\mathcal{S}_N$, like a Gaussian process (GP), to construct the bias distribution $\mathbb{F}^\star$}
		\item {\textbf{Adaptive designs} for contour finding: sequentially select design points to estimate failure boundary $\{\x:Y(\x)=T\}$}
	\end{itemize}
\end{frame}


%---------------------------------------------------------
\begin{frame}
	\frametitle{\textbf{Adaptive design for contour location}}

		\begin{columns}
			\begin{column}{.6\textwidth}
	\begin{figure}
	\includegraphics[trim=10 10 10 10, clip, width=\textwidth]{../../figures/adaptive_design_flowchart.pdf}
	
	\end{figure}
\end{column}
\begin{column}{0.4\textwidth}  %%<--- here
Existing acquisition functions based upon:
\begin{itemize}
	\item {Predictive variance \citep{picheny2010adaptive}} 
	\item {Expected Improvement \citep{ranjan2008sequential}}
	\item {Entropy \\ \citep{marques2018contour}}
\end{itemize}
	\end{column}
\end{columns}


\end{frame}


%---------------------------------------------------------


\begin{frame}
	\frametitle{\textbf{Adaptive design + MFIS}}
	\begin{figure}
		\includegraphics[trim=10 10 10 10, clip, width=\textwidth]{../../figures/adaptive_design_mfis_flowchart_dissertation.pdf}
		%\caption{Full process combining adaptive design with MFIS.}
	\end{figure}
	
\end{frame}

%------------------------------------------------------------
%\subsection{Entropy-based Adaptive Design}

\begin{frame}
	\frametitle{\textbf{Entropy-based Contour Location}}
	\begin{itemize}
		\item We define the \textbf{Entropy-based Contour Locator (ECL)} using \\ $\text{Entropy}=-\sum_{i=1}^k \mathbb{P}(w_i)\log \mathbb{P}(w_i)$, with
		\begin{itemize}
			\item $w_1$ representing a failure (i.e. $y>T$)
			\item $w_2$ representing a lack of failure (i.e. $y\leq T$)
		\end{itemize}
		\item Using a GP $\mathcal{S}_N$'s predictive equations to calculate $\mathbb{P}(w_i)$,
	\end{itemize}
\vspace{-0.5cm}
\begin{align}
	\mathrm{ECL}(\x\mid\mathcal{S}_N,T)=  \label{eq:our_entropy} 
	&-\left(1-\Phi\left(\frac{\mu_N(\x)-T}{\sigma_N(\x)}\right)\right)\log\left(1-\Phi\left(\frac{\mu_N(\x)-T}{\sigma_N(\x)}\right)\right) \nonumber \\
	&\qquad \qquad -\Phi\left(\frac{\mu_N(\x)-T}{\sigma_N(\x)}\right)\log\left(\Phi\left(\frac{\mu_N(\x)-T}{\sigma_N(\x)}\right)\right) \nonumber 
\end{align}
\vspace{-0.5cm}
\begin{itemize}
	\item Design can be done in batches (ECL.b) by updating $\sigma_N(\x)$ with other batch design locations
\end{itemize}

\end{frame}

%---------------------------------------------------------------
\begin{frame}
	\frametitle{\textbf{GP and ECL surfaces}}
	\begin{figure}[h!]
		\centering
		\includegraphics[trim=5 5 12 5,clip, width=.75\textwidth]{../../figures/mm_seq_ent_gp_pred_talk.pdf}
	\end{figure}
\end{frame}



\begin{frame}
	\frametitle{\textbf{Optimization of ECL}}
	%\begin{algorithm} [H]\caption{Entropy Optimization} 
	%\begin{algorithmic}[1] 
	%	\Procedure
	%	{Entropy.Opt}{$N_c, \mathcal{S}_N, \mathcal{X}$}
	%	\State $\bar{X}_{N_c} \leftarrow$ LHS of size $N_c$ in $\mathcal{X}$ \hfill \Comment{Candidate set}
	%	\State $\tilde{x} \leftarrow \mathrm{argmax}_{\bar{x}\in \bar{X}_{N_c}} \; \mathrm{ECL}(\bar{x} \mid \mathcal{S}_N)$ \hfill \Comment{Discrete search}
	%	\State $x_{N+1} \leftarrow \mathrm{argmax}_{x \in \mathcal{B}(\tilde{x})} \; \mathrm{ECL}(\x \mid \mathcal{S}_N)$ \hfill \Comment{Continuous, local, from $\tilde{x}$}
	%	\State \Return $x_{N+1}$
	%	\EndProcedure 
	%\end{algorithmic}
	%\end{algorithm}
	\begin{itemize}
		\item ECL surface contains a ridge of global maxima on the GP's predicted contour
		\item Other local maxima exist that are worth exploring through sample points
		\item \textbf{Goal}: promote exploration of global and local maxima through a small, variable set of candidate points
		\item \textbf{Steps}:
		\begin{enumerate}
			\item Evaluate ECL for a candidate set of a $10\times d$ Latin hypercube sample
			\item Select the candidate point with the largest ECL
			\item Use local optimization to maximize ECL, with starting location from step 2
		\end{enumerate}
	\end{itemize}

\end{frame}


\begin{frame}
	\frametitle{\textbf{ECL 2d surface}}
	\begin{figure}[h!]
		\centering
		\includegraphics[trim=0 0 0 35,clip, width=\textwidth]{../../figures/mm_seq_ent_surface_1_nocands.pdf}
	\end{figure}
\end{frame}


\begin{frame}
	\frametitle{\textbf{ECL 2d surface: selecting the 1st point}}
	\begin{figure}[h!]
		\centering
		\includegraphics[trim=0 0 0 35,clip, width=\textwidth]{../../figures/mm_seq_ent_surface_1_withcands.pdf}
	\end{figure}
\end{frame}


\begin{frame}
	\frametitle{\textbf{ECL 2d surface: selecting the 1st point}}
	\begin{figure}[h!]
		\centering
		\includegraphics[trim=0 0 0 35,clip, width=\textwidth]{../../figures/mm_seq_ent_surface_1.pdf}
	\end{figure}
\end{frame}



\begin{frame}
	\frametitle{\textbf{ECL 2d surface: selecting the 2nd point}}
	\begin{figure}[h!]
		\centering
		\includegraphics[trim=0 0 0 35,clip, width=\textwidth]{../../figures/mm_seq_ent_surface_2.pdf}
	\end{figure}
\end{frame}

\begin{frame}
	\frametitle{\textbf{ECL 2d surface: selecting the 3rd point}}
	\begin{figure}[h!]
		\centering
		\includegraphics[trim=0 0 0 35,clip, width=\textwidth]{../../figures/mm_seq_ent_surface_3.pdf}
	\end{figure}
\end{frame}

\begin{frame}
	\frametitle{\textbf{ECL 2d surface: selecting the 7th point}}
	\begin{figure}[h!]
		\centering
		\includegraphics[trim=0 0 0 35,clip, width=\textwidth]{../../figures/mm_seq_ent_surface_7.pdf}
	\end{figure}
\end{frame}

\begin{frame}
	\frametitle{\textbf{ECL 2d surface: selecting the 10th point}}
	\begin{figure}[h!]
		\centering
		\includegraphics[trim=0 0 0 35,clip, width=\textwidth]{../../figures/mm_seq_ent_surface_10.pdf}
	\end{figure}
\end{frame}




%\subsection{Experiment Results}

%---------------------------------------------------------------
\begin{frame}
	\frametitle{\textbf{Synthetic function experiments}}
%\textbf{Synthetic Functions}
\begin{table}[ht!]
	\centering
	\begin{tabular}{l|r|r} 
		%\hline
		 & \textbf{Ishigami} & \textbf{Hartmann-6} \\ 
		\hline
		Number of dimensions & 3 & 6 \\ \hline
		Failure threshold ($T$) & 10.244 & 2.63\\ \hline
		Number of failure regions & 6 & 2\\ \hline
		Quantile & 0.9999 & 0.9989\\ \hline \hline
		Initial design size & 30 & 60 \\ \hline
		Number of sequential points & 170 & 440 \\ \hline \hline
		Failure probability ($\alpha$) & $1.9\times10^{-4}$ & $1\times10^{-5}$ \\ \hline
		Number of MFIS samples & 800 & 500
	\end{tabular}
	\label{fig:timings}
\end{table}
	
\end{frame}

\begin{frame}
	\frametitle{\textbf{Ishigami sensitivity}}
	\begin{figure}[h!]
		\centering
		\includegraphics[trim=0 5 10 5, clip,width=.83\textwidth]{../../figures/Ish_sens_results.pdf}
\includegraphics[trim=260 450 15 44, clip,width=.15\textwidth]{../../figures/methods_vertical_legend.pdf}
	\end{figure}
\end{frame}

\begin{frame}
	\frametitle{\textbf{Ishigami volume estimate error}}
	\begin{figure}[h!]
		\centering
		\includegraphics[trim=0 5 10 5, clip,width=.83\textwidth]{../../figures/Ish_re_results.pdf}
		\includegraphics[trim=260 450 15 44, clip,width=.15\textwidth]{../../figures/methods_vertical_legend.pdf}
	\end{figure}
\end{frame}

\begin{frame}
	\frametitle{\textbf{Hartmann-6 sensitivity}}
	\begin{figure}[h!]
		\centering
		\includegraphics[trim=0 5 10 5, clip,width=.83\textwidth]{../../figures/Hart_sens_results.pdf}
		\includegraphics[trim=260 450 15 44, clip,width=.15\textwidth]{../../figures/methods_vertical_legend.pdf}
	\end{figure}
\end{frame}

\begin{frame}
	\frametitle{\textbf{Hartmann-6 volume estimate error}}
	\begin{figure}[h!]
		\centering
		\includegraphics[trim=0 5 10 5, clip,width=.83\textwidth]{../../figures/Hart_re_results.pdf}
		\includegraphics[trim=260 450 15 44, clip,width=.15\textwidth]{../../figures/methods_vertical_legend.pdf}
	\end{figure}
\end{frame}

\begin{frame}
	\frametitle{\textbf{Average Computation Time}}
	\begin{table}[ht!]
		\centering
		\begin{tabular}{l|r|r} 
			\hline
			Method\footnotemark & Ishigami & Hartmann-6 \\ \hline
			\textbf{ECL} & \textbf{0.10} & \textbf{4.40}  \\ \hline
			\textbf{ECL.b} & \textbf{0.08} & \textbf{0.78}  \\ \hline
			CLoVER & 17.8 & 428.4  \\ \hline
			EGRA & 3.9 & 66.0  \\ \hline
			Ranjan & 4.5 & 59.2  \\ \hline
			SUR & 3.5 & 109.1  \\ \hline
			tIMSE & 3.4 & 11.7  \\ \hline
			tMSE & 3.9 & 59.5  \\			
		\end{tabular}
		\label{fig:timings}
	\end{table}
\footnotetext[2]{ECL and CLoVER are coded in {\tt Python}, the rest is coded in {\tt R}}
\end{frame}


\begin{frame}
	\frametitle{\textbf{MFIS estimates for Ishigami and Hartmann-6}}
	\begin{figure}[h!]
		\centering
		\includegraphics[trim=0 0 40 13, clip,width=\textwidth]{../../figures/mfis_estimates.png}
	\end{figure}
\end{frame}


\begin{frame}
	\frametitle{\textbf{Spacesuit Simulator: samples for estimation}}
	\begin{itemize}
		\item MFIS estimates: 200 sample design used to train GP + 250 samples from bias distribution
		\item Monte Carlo: 2500 samples from input distribution
	\end{itemize}
	\textbf{Samples used to estimate failure probability:}
	\begin{figure}[h]
		\centering
		\includegraphics[trim=0 0 0 0 , clip,width=\textwidth]{../../figures/z2_samples.pdf}
		\label{fig:z2_densities}
	\end{figure}
	
\end{frame}


\begin{frame}
	\frametitle{\textbf{Spacesuit Simulator: failure probability estimates}}
	\begin{figure}[h]
	\centering
	\includegraphics[trim=0 0 0 10 , clip,width=.9\textwidth]{../../figures/z2_est_errorbar.png}
	\end{figure}
	
\end{frame}

\begin{frame}
	\frametitle{\textbf{Summary}}
	\begin{itemize}
		\item Using a \textbf{GP surrogate with MFIS provides a cheaper approach} to failure probability estimation for expensive simulators
		\item An \textbf{adaptive design} GP improves MFIS estimation by producing a \textbf{more accurate bias distribution}
		\item Using ECL with a simple optimization strategy \textbf{balances exploration (somewhat randomly) with exploitation} to drive down contour uncertainty
		\item ECL adaptive design can be \textbf{20-100 times faster} than existing adaptive designs
		\item ECL batch selection is \textbf{faster than and nearly as accurate} as ECL
		
	\end{itemize}
		
\end{frame}
%\begin{frame}
%	\frametitle{Weighted post-hoc Expected Improvement}
%	\begin{enumerate}
%		\item Create set of $m_0$ multi-start points for optimization\\
%		\item For each multi-start location $\breve{X}_i$ optimize Expected Improvement:	
%		\begin{itemize}
%		\item $\dot{X}_i \leftarrow \max_x\text{EI(x)}$
%		\item $\text{ei}_i \leftarrow \text{EI}(\dot{X}_i)$
%		\end{itemize}
%		\item Select $\dot{x}_j where \min_x |\mu(x) - T|$
%	\end{enumerate}	
%\end{frame}

%---------------------------------------------------------
\begin{frame}[allowframebreaks]
	\frametitle{Bibliography}
	\bibliographystyle{jasa}
	\bibliography{articles}
\end{frame}

\end{document}