\documentclass[hyperref={bookmarks=false},aspectratio=169]{beamer}
\usepackage[utf8]{inputenc}
\usepackage{natbib}
\usepackage{amsmath, subfig}
\usepackage{dsfont}
\usepackage{algpseudocode}
\usepackage[]{algorithm}
\usepackage{algorithmicx}
\renewcommand*{\bibfont}{\small}
\newcommand{\Top}{\text{T}}
\newcommand{\x}{\mathbf{x}}
\newcommand{\X}{\mathbf{X}}
\newcommand{\Y}{\mathbf{Y}}
\newcommand{\K}{\mathbf{K}}
\newcommand{\kk}{\mathbf{k}}
\newcommand{\F}{\mathbb{F}}
\newcommand{\R}{\mathbb{R}}
\newcommand{\B}{\mathcal{B}}
\newcommand{\C}{\mathcal{C}}
\newcommand{\G}{\mathcal{G}}
\newcommand{\g}{\mathbf{g}}
\newcommand{\Q}{\mathbf{Q}}
\newcommand{\W}{\mathbf{W}}
\newcommand{\cc}{\mathbf{c}}
\newcommand{\I}{\mathbb{I}}
\newcommand{\OO}{\mathcal{O}}
\newcommand{\Sig}{\mathbf{\Sigma}}

\setlength{\textfloatsep}{10pt plus 1.0pt minus 2.0pt}
%\setbeamertemplate{caption}{\insertcaption}
\DeclareMathOperator*{\argmaxA}{arg\,max} 
\DeclareMathOperator*{\maxA}{max} 
% ---------------  Define theme and color scheme  -----------------
\usetheme[]{VT2}
%\usetheme[sidebarleft]{VT2}  % 3 options: minimal, sidebarleft, sidebarright
\setbeamertemplate{footline}[frame number]
%\setbeamertemplate{frametitle}{\color{black}\bfseries\insertframetitle\par\vskip-6pt\hrulefill}

\setbeamertemplate{caption}[numbered]

% ------------  Information on the title page  --------------------
\title[Entropy Active Learning]
{\bfseries{Entropy-Based Adaptive Design for Contour-Finding and Estimating Reliability}}
\subtitle{}
\author[Cole, Gramacy, et al.]
{D.~Austin Cole, {\bf Robert B.~Gramacy} from VT\\
J.E.~Warner, G.F.~Bomarito, P.E.~Leser, W.P.~Leser from NASA}
\institute[Virginia Tech]
{
	%\inst{1}
	Department of Statistics\\
	Virginia Tech
}

\date[]{University of Calgary Stat/Biostat, Mar 2022}
%------------------------------------------------------------
\newcommand{\cs}[1]{\texttt{\symbol{`\\}#1}}




%------------------------------------------------------------
%The next block of commands puts the table of contents at the 
%beginning of each section and highlights the current section:

\AtBeginSection[]
{
  \begin{frame}
    \frametitle{\textbf{Table of contents}}
    \tableofcontents[currentsection]
  \end{frame}
}
%------------------------------------------------------------


\begin{document}

\frame{\titlepage}  % Creates title page

%---------   table of contents after title page  ------------
% \begin{frame}
% \frametitle{\textbf{Table of contents}}
% \tableofcontents
% \end{frame}
%---------------------------------------------------------

\section*{Simulation \& Surrogates}
%---------------------------------------------------------
%\begin{frame}
%\frametitle{Metabolism Simulation}
%\begin{figure}[h!]
%\centering
%\includegraphics[trim={0 0 0 0}, clip, width=85mm]{chem_simulation.jpg}

%\end{figure}
%\end{frame}




%---------------------------------------------------------
\begin{frame}
	\frametitle{\textbf{Computer simulation models}}
	\begin{itemize}
		\item Computationally demanding programs that mathematically model a physical process by incorporating domain knowledge
		\item Often desired over physical experiments due to cost and time savings
		\item Applications span many different fields:
	\end{itemize}
 \hspace{1.8cm} \textbf{Engineering} \hspace{1.6cm} \textbf{Science} \hspace{1.6cm} \textbf{Public Health} \vspace{-.5cm}
	\begin{figure}[h!]
		\centering
		\includegraphics[trim={0 0 0 0}, clip, width=35mm]{extra_figures/hubble.jpg}
		\includegraphics[trim={0 0 0 0}, clip, width=35mm]{extra_figures/chemistry.png}
		\includegraphics[trim={0 0 0 0}, clip, width=35mm]{extra_figures/disease_spread.jpg}
		
	\end{figure}
\end{frame}

%---------------------------------------------------------
\begin{frame}
	\frametitle{\textbf{Surrogate modeling--a worthy substitute}}
	\begin{itemize}
		\item Trained on data from a simulation campaign to imitate the simulator ...
		\item providing accurate predictions with uncertainty quantification (UQ) ...
		\item \textbf{much faster than the simulator} could.
	\end{itemize}
	\begin{figure}[h!]
		\centering
		\includegraphics[trim={0 0 0 0}, clip, width=65mm]{extra_figures/waiting.jpg}
	\end{figure}
\end{frame}

\section*{Gaussian Processes}


%---------------------------------------------------------
\begin{frame}
	\frametitle{\textbf{Gaussian process (GP) surrogate}}

	\begin{itemize}
		\item Assume simulation outputs $Y_N$ follow a multivariate Normal distribution:
		$$Y_N\sim \mathcal{N}_N(0, \Sigma_N), \quad \mbox{where} \quad \Sigma_N=\tau^2(K_N+g\mathbb{I}_N)$$
		\item Correlation matrix $K_N$ via Euclidean distance between pairs of inputs, e.g.
		$$k(\x_i,\x_j)=\text{exp}\Bigg\{-\frac{||\x_i-\x_j||^2}{\theta} \Bigg\}$$	
		\item{Conditional predictive equations:}
\begin{align*}
		\mu(\x'|\X_N,Y_N) &= \Sigma(\x',\X_N)\Sigma_N^{-1}Y_N \\
		\sigma^2(\x'|\X_N) &= \Sigma(\x',\x')-\Sigma(\x',\X_N)\Sigma_N^{-1}\Sigma(\x',\X_N)^\top
\end{align*}
	\end{itemize}
\end{frame}


%---------------------------------------------------------
\begin{frame}
	\frametitle{\textbf{GP fit to sine curve}}
	\begin{figure}[h!]
		\centering
		\includegraphics[trim={0 0 0 2cm}, clip, width=95mm]{extra_figures/gp_fit_sine_curve.pdf}
	\end{figure}
\end{frame}


\subsection*{Active learning}


\begin{frame}
	\frametitle{\textbf{GP adaptive design for computer experiments}}
	\begin{figure}
	\includegraphics[trim=10 10 10 10, clip, width=.8\textwidth]{../../figures/adaptive_design_flowchart.pdf}
	
\end{figure}
\end{frame}

\begin{frame}
	\frametitle{\textbf{Acquisition functions}}
		\begin{itemize}
			\item {\textbf{Integrated mean-squared error (IMSE)}: reduces global variance} 
		$$\text{IMSE}(\x_{N+1})=\int_{\x\in\mathcal{X}}\frac{{\sigma}^2_{N+1}(\x)}{\tau^2}d\x$$
		\item  {\textbf{Expected improvement (EI)}: for Bayesian optimization}
		$$
		\mathrm{EI}(\x_{N+1}) = (y_{\min} - \mu_N(\x_{N+1}))\cdot \Phi + \sigma_N(\x_{N+1})\cdot \phi
		$$
			\item {\textbf{Entropy}: maximizes information gain about events $w_i(\x_{N+1})$}
		$$\text{Entropy}(\x_{N+1}) =-\sum_{i=1}^k \mathbb{P}(w_i(\x_{N+1})) \log \mathbb{P}(w_i(\x_{N+1}))$$
		\end{itemize}
\end{frame}


\section*{NASA Spacesuit Reliability}


%\subsection[]{Background}

\begin{frame}
	\frametitle{\textbf{Estimating reliability of a next-generation spacesuit}}
	\begin{itemize}
		\item {NASA must assess simulated impact failure for a new spacesuit}
		\item {under uncertainties in material and projectile inputs ($\X\sim \mathbb{F}$) }
%	\end{itemize}
%	\begin{equation}
%		X \sim f
%	\end{equation}
%	\begin{itemize}	
		\item {\textbf{Objective}: estimate the probability the response exceeds threshold $T$}
		$$\alpha=P(Y(\x) > T)$$
	\end{itemize}
	\vspace{-0.25cm}
	\begin{figure}[h!]
		\centering
		\includegraphics[ width=40mm]{../../figures/xemu_hut.jpg}
		
	\end{figure}
\end{frame}

\begin{frame}
	\frametitle{\textbf{Characteristics and goals}}
	\begin{minipage}{8.5cm}
	\begin{itemize}
		\item {Moderate input ($\X$) dimension, $d=2,\dots,10$.}
		\item {Identify {\bf multiple failure regions} (if they exist)}
		\item {Estimate contours corresponding to high/low quantiles: extreme $T$ and/or $\alpha$}
		\item {Be {\bf conservative} in estimates of the location of failure regions}
		\item {Tap into parallel-processing: {\bf batch selection}}
	\end{itemize}

	\vspace{1cm}
	\hfill Computer schematic of one design $\rightarrow$
	\end{minipage}
	\hspace{0.5cm}
	\begin{minipage}{3cm}
	\includegraphics[trim={0 0 0 1}, angle=-90, clip, width=30mm]{../../figures/assembly_model.png}
	\end{minipage}
\end{frame}


\begin{frame}
	\frametitle{\textbf{Existing methods to assess reliability}}
	\begin{itemize}
		\item {\textbf{Monte Carlo sampling}: draws inputs from uncertainty ($\X \sim \mathbb{F}$) distribution and evaluate simulation model}
		\item {\textbf{Importance sampling (IS)}: constructs a distribution $\mathbb{F}^\star$ biased toward the failure region(s) to draw inputs}
	\end{itemize}
	\begin{equation*} \label{eq:is_estimate}
	\hat{\alpha}_{\text{IS}}=\frac{1}{\grave{N}}\sum_{i=1}
	^{\grave{N}}\mathds{1}_{\{Y(\x_i^\star)>T\}}w(\x_i^\star) \quad \mbox{via weights} \quad
	w(x^\star)=\frac{f(\x^\star)}{f_\star(\x^\star)}
\end{equation*}
	\begin{itemize}
		\item {\textbf{Multifidelity importance sampling (MFIS)}: uses a surrogate (GP), to construct the bias distribution $\mathbb{F}^\star$}
		\item {\textbf{Adaptive designs} for contour finding: sequentially select design points to estimate failure boundary $\{\x:Y(\x)=T\}$}
	\end{itemize}
\end{frame}

%---------------------------------------------------------


\begin{frame}
	\frametitle{\textbf{Adaptive design + MFIS}}
	\begin{figure}
		\includegraphics[trim=10 10 10 10, clip, width=\textwidth]{../../figures/adaptive_design_mfis_flowchart_dissertation.pdf}
		%\caption{Full process combining adaptive design with MFIS.}
	\end{figure}

	Acquisition via variations on IMSE \citep{picheny2010adaptive}, EI \citep{ranjan2008sequential}, {\bf entropy} \citep{marques2018contour}.
	
\end{frame}

%------------------------------------------------------------
%\subsection{Entropy-based Adaptive Design}

\section*{Entropy contour location}


\begin{frame}
	\frametitle{\textbf{Entropy-based acquisition}}
	\begin{itemize}
		\item \textbf{Entropy-based Contour Locator (ECL)} via $-\sum_{i=1}^2 \mathbb{P}(w_i)\log \mathbb{P}(w_i)$
	$$\mbox{with } \quad w_1 = \{y(\x)>T\} \quad \mbox{ and } \quad w_2 = \{y(\x)\leq T\}  $$
		\item GP surrogate provides $\mathbb{P}(w_i)$ in closed form:
	\end{itemize}
\vspace{-0.5cm}
\begin{align}
	\mathrm{ECL}(\x\mid \Y_N,T)=  \label{eq:our_entropy} 
	&-\left(1-\Phi\left(\frac{\mu_N(\x)-T}{\sigma_N(\x)}\right)\right)\log\left(1-\Phi\left(\frac{\mu_N(\x)-T}{\sigma_N(\x)}\right)\right) \nonumber \\
	&\qquad \qquad -\Phi\left(\frac{\mu_N(\x)-T}{\sigma_N(\x)}\right)\log\left(\Phi\left(\frac{\mu_N(\x)-T}{\sigma_N(\x)}\right)\right) \nonumber 
\end{align}
\vspace{-0.5cm}
\begin{itemize}
	\item Batch extensions (ECL.b) by updating $\sigma_N^2(\x)$ with earlier acquisitions
\end{itemize}

\end{frame}

%---------------------------------------------------------------
\begin{frame}
	\frametitle{\textbf{GP and ECL surfaces}}
	\begin{figure}[h!]
		\centering
		\includegraphics[trim=5 5 12 5,clip, width=.75\textwidth]{../../figures/mm_seq_ent_gp_pred_talk.pdf}
	\end{figure}
\end{frame}



\begin{frame}
	\frametitle{\textbf{Optimization of ECL}}
	%\begin{algorithm} [H]\caption{Entropy Optimization} 
	%\begin{algorithmic}[1] 
	%	\Procedure
	%	{Entropy.Opt}{$N_c, \mathcal{S}_N, \mathcal{X}$}
	%	\State $\bar{X}_{N_c} \leftarrow$ LHS of size $N_c$ in $\mathcal{X}$ \hfill \Comment{Candidate set}
	%	\State $\tilde{x} \leftarrow \mathrm{argmax}_{\bar{x}\in \bar{X}_{N_c}} \; \mathrm{ECL}(\bar{x} \mid \mathcal{S}_N)$ \hfill \Comment{Discrete search}
	%	\State $x_{N+1} \leftarrow \mathrm{argmax}_{x \in \mathcal{B}(\tilde{x})} \; \mathrm{ECL}(\x \mid \mathcal{S}_N)$ \hfill \Comment{Continuous, local, from $\tilde{x}$}
	%	\State \Return $x_{N+1}$
	%	\EndProcedure 
	%\end{algorithmic}
	%\end{algorithm}
	ECL surface may have disconnected ridges of (global) maxima depending on the
	nature of the GP's predicted contour for $T$.
	\begin{itemize}
		\item Exploration nearby local/local maxima may be more valuable than exploitation.  (Previous methods preferred intractable integration.)

	\end{itemize}

	\textbf{Goal}: promote exploration without numerical integration.

	\textbf{Steps}:
		\begin{enumerate}
			\item Evaluate ECL for a candidate set of a $10\times d$ Latin hypercube sample
			\item Select the candidate point with the largest ECL
			\item Use local optimization to maximize ECL, with starting location from step 2
		\end{enumerate}

\end{frame}


\begin{frame}
	\frametitle{\textbf{ECL 2d surface}}
	\begin{figure}[h!]
		\centering
		\includegraphics[trim=0 0 0 35,clip, width=\textwidth]{../../figures/mm_seq_ent_surface_1_nocands.pdf}
	\end{figure}
\end{frame}


\begin{frame}
	\frametitle{\textbf{ECL 2d surface: selecting the 1st point}}
	\begin{figure}[h!]
		\centering
		\includegraphics[trim=0 0 0 35,clip, width=\textwidth]{../../figures/mm_seq_ent_surface_1_withcands.pdf}
	\end{figure}
\end{frame}


\begin{frame}
	\frametitle{\textbf{ECL 2d surface: selecting the 1st point}}
	\begin{figure}[h!]
		\centering
		\includegraphics[trim=0 0 0 35,clip, width=\textwidth]{../../figures/mm_seq_ent_surface_1.pdf}
	\end{figure}
\end{frame}



\begin{frame}
	\frametitle{\textbf{ECL 2d surface: selecting the 2nd point}}
	\begin{figure}[h!]
		\centering
		\includegraphics[trim=0 0 0 35,clip, width=\textwidth]{../../figures/mm_seq_ent_surface_2.pdf}
	\end{figure}
\end{frame}

\begin{frame}
	\frametitle{\textbf{ECL 2d surface: selecting the 3rd point}}
	\begin{figure}[h!]
		\centering
		\includegraphics[trim=0 0 0 35,clip, width=\textwidth]{../../figures/mm_seq_ent_surface_3.pdf}
	\end{figure}
\end{frame}

\begin{frame}
	\frametitle{\textbf{ECL 2d surface: selecting the 7th point}}
	\begin{figure}[h!]
		\centering
		\includegraphics[trim=0 0 0 35,clip, width=\textwidth]{../../figures/mm_seq_ent_surface_7.pdf}
	\end{figure}
\end{frame}

\begin{frame}
	\frametitle{\textbf{ECL 2d surface: selecting the 10th point}}
	\begin{figure}[h!]
		\centering
		\includegraphics[trim=0 0 0 35,clip, width=\textwidth]{../../figures/mm_seq_ent_surface_10.pdf}
	\end{figure}
\end{frame}


\subsection*{Synthetic Results}

%---------------------------------------------------------------
\begin{frame}
	\frametitle{\textbf{Two synthetic examples}}
%\textbf{Synthetic Functions}
	\begin{center}
	\begin{tabular}{l|r|r} 
		%\hline
		 & \textbf{Ishigami} & \textbf{Hartmann-6} \\ 
		\hline
		Number of dimensions & 3 & 6 \\ \hline
		Failure threshold ($T$) & 10.244 & 2.63\\ \hline
		Number of failure regions & 6 & 2\\ \hline
		Quantile & 0.9999 & 0.9989\\ \hline \hline
		Design $N_0$/$N_{\mathrm{total}}$ & 30/170 & 60/440 \\ \hline\hline
		Failure probability ($\alpha$) & $1.9\times10^{-4}$ & $1\times10^{-5}$ \\ \hline
		Number of MFIS samples & 800 & 500
	\end{tabular}
	\end{center}	

First measuring out-of-sample failure region estimation:
\begin{itemize}
	\item {\bf Sensitivity}: how often correctly predict failure
	\item {\bf Volume error} of the failure region
\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{\textbf{Ishigami sensitivity}}
	\begin{figure}[h!]
		\centering
		\includegraphics[trim=0 5 10 5, clip,width=.83\textwidth]{../../figures/Ish_sens_results.pdf}
\includegraphics[trim=260 450 15 44, clip,width=.15\textwidth]{../../figures/methods_vertical_legend.pdf}
	\end{figure}
\end{frame}

\begin{frame}
	\frametitle{\textbf{Ishigami volume estimate error}}
	\begin{figure}[h!]
		\centering
		\includegraphics[trim=0 5 10 5, clip,width=.83\textwidth]{../../figures/Ish_re_results.pdf}
		\includegraphics[trim=260 450 15 44, clip,width=.15\textwidth]{../../figures/methods_vertical_legend.pdf}
	\end{figure}
\end{frame}

\begin{frame}
	\frametitle{\textbf{Hartmann-6 sensitivity}}
	\begin{figure}[h!]
		\centering
		\includegraphics[trim=0 5 10 5, clip,width=.83\textwidth]{../../figures/Hart_sens_results.pdf}
		\includegraphics[trim=260 450 15 44, clip,width=.15\textwidth]{../../figures/methods_vertical_legend.pdf}
	\end{figure}
\end{frame}

\begin{frame}
	\frametitle{\textbf{Hartmann-6 volume estimate error}}
	\begin{figure}[h!]
		\centering
		\includegraphics[trim=0 5 10 5, clip,width=.83\textwidth]{../../figures/Hart_re_results.pdf}
		\includegraphics[trim=260 450 15 44, clip,width=.15\textwidth]{../../figures/methods_vertical_legend.pdf}
	\end{figure}
\end{frame}

\begin{frame}
	\frametitle{\textbf{Average Computation Time}}
	\begin{table}[ht!]
		\centering
		\begin{tabular}{l|r|r} 
			\hline
			Method\footnotemark & Ishigami & Hartmann-6 \\ \hline
			\textbf{ECL} & \textbf{0.10} & \textbf{4.40}  \\ \hline
			\textbf{ECL.b} & \textbf{0.08} & \textbf{0.78}  \\ \hline
			CLoVER & 17.8 & 428.4  \\ \hline
			EGRA & 3.9 & 66.0  \\ \hline
			Ranjan & 4.5 & 59.2  \\ \hline
			SUR & 3.5 & 109.1  \\ \hline
			tIMSE & 3.4 & 11.7  \\ \hline
			tMSE & 3.9 & 59.5  \\			
		\end{tabular}
		\label{fig:timings}
	\end{table}
\footnotetext[2]{ECL and CLoVER are coded in {\tt Python}; the rest in {\sf R}}
\end{frame}


\begin{frame}
	\frametitle{\textbf{MFIS estimates for Ishigami and Hartmann-6}}
	\begin{figure}[h!]
		\centering
		\includegraphics[trim=0 0 40 13, clip,width=\textwidth]{../../figures/mfis_estimates.png}
	\end{figure}
\end{frame}

\subsection*{NASA Results}



\begin{frame}
	\frametitle{\textbf{Spacesuit Simulator: samples for estimation}}
	\begin{itemize}
		\item MFIS estimates: 200 design sites used to train GP + 250 samples from $\mathbb{F}^\star$
		\item Monte Carlo: 2500 samples from $\mathbb{F}$
	\end{itemize}
	\textbf{Samples used to estimate failure probability:}
	\begin{figure}[h]
		\centering
		\includegraphics[trim=0 0 0 20, width=\textwidth]{../../figures/z2_samples.pdf}
		\label{fig:z2_densities}
	\end{figure}
	
\end{frame}


\begin{frame}
	\frametitle{\textbf{Spacesuit Simulator: failure probability estimates}}
	More confident with active learning to focus on failure contours.
	\begin{figure}[h]
	\centering
	\includegraphics[trim=0 0 0 10, clip,width=.75\textwidth]{../../figures/z2_est_errorbar.png}
	\end{figure}
	
\end{frame}

\section*{Summary}

\begin{frame}
	\frametitle{\textbf{Summary}}
	\begin{itemize}
		\item Using a \textbf{GP surrogate with MFIS provides a cheaper approach} to failure probability estimation for expensive simulators
		\item An \textbf{adaptive design} GP improves MFIS estimation by producing a \textbf{more accurate bias distribution}
		\item Carefully optimized ECL \textbf{balances exploration with exploitation} to drive down contour uncertainty
		\item ECL adaptive design can be \textbf{20-100 times faster} than other strategies
		\item Batch ECL is \textbf{faster than and nearly as accurate} as ECL
	\end{itemize}
		
	Reproducible {\sf Python} and {\sf R} at \url{https://bitbucket.org/gramacylab/nasa/}	
\end{frame}
%\begin{frame}
%	\frametitle{Weighted post-hoc Expected Improvement}
%	\begin{enumerate}
%		\item Create set of $m_0$ multi-start points for optimization\\
%		\item For each multi-start location $\breve{X}_i$ optimize Expected Improvement:	
%		\begin{itemize}
%		\item $\dot{X}_i \leftarrow \max_x\text{EI(x)}$
%		\item $\text{ei}_i \leftarrow \text{EI}(\dot{X}_i)$
%		\end{itemize}
%		\item Select $\dot{x}_j where \min_x |\mu(x) - T|$
%	\end{enumerate}	
%\end{frame}

%---------------------------------------------------------
\begin{frame}[allowframebreaks]
	\frametitle{Bibliography}
	\bibliographystyle{jasa}
	\bibliography{articles}
\end{frame}

\end{document}
